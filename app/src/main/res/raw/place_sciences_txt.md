– Quel bruit assourdissant
– Cet échassier immense…
– Quelle vue splendide doit avoir le grutier
– J’adore cette couleur orange sur le ciel !
– Une équerre…
– Ou un perchoir pour les hirondelles
– Le chant du chantier n’est pas anodin
– Le sol a tremblé
– Elle ne va pas lâcher son chargement au moins…
– Et si ça tombe ?
– Et si ça tombe il a le vertige !
– Et si ça tombe ça va rester vide !
– Et si ça tombe ça va être moche
– Ça tombe bien je voulais traverser
– Et c’est pour qui ces nouveaux logements ? pas pour les étudiants !
– Mais il n’y a pas QUE des étudiants à LLN !
– ben non, c’est pour les vieux ! et les vieux riches !
– Mes grands-parents viennent d’acheter un appart ici ! j’irai chez papy faire un spa !
– Mais y aura aussi un hôtel !
– En même temps, y avait pas d’hôtel avant
– C’est toujours mieux qu’un parking
– Au moins il y a plus de courant d’air quand on passe
– Quel dynamisme époustouflant dans cette ville
– LLN ne dort jamais
– Oui, mais on n’a plus vue sur le lac
– Alors ouvre les yeux, vois plus loin…