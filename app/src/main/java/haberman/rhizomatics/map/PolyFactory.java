package haberman.rhizomatics.map;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import haberman.rhizomatics.utils.GeoUtils;
import timber.log.Timber;

/**
 * This class contains static method to compute parametric polygons' coordinates.
 *
 * @author haberman
 */

public class PolyFactory
{
    
    /**
     * Translates the JSON fragment of a shape to an array of {@code LatLng}.
     *
     * @param shape The shape could be either "cyclic" or "polygon"
     *
     * @return A list of {@code LatLng}.
     * @throws JSONException from either the {@link #decode_polygon(JSONArray)} or
     *                       {@link #decode_cyclic_(JSONObject, float)} method.
     */
    @SuppressWarnings("ConstantConditions")
    @NonNull
    public static ArrayList<LatLng> decode_shape( final JSONObject shape ) throws JSONException {
        ArrayList<LatLng> polygon = new ArrayList<>();
    
        if (shape.has( "polygon" )) {
            return decode_polygon( shape.getJSONArray( "polygon" ) );
        } else if (shape.has( "cyclic" )) {
            return decode_cyclic_( shape.getJSONObject( "cyclic" ), 1 );
        }
    
        return polygon;
    }
    
    /**
     * Returns an array of {@code LatLng} describing a polygon from its json-encoded representation.
     *
     * @param poly_json The {@code JSONArray} to decode
     *
     * @return A list of {@code LatLng} or null if the JSONArray wasn't decoded.
     */
    @Nullable
    public static ArrayList<LatLng> decode_polygon( final JSONArray poly_json ) {
        ArrayList<LatLng> polygon = new ArrayList<>();
    
        try {
            for (int i = 0; i < poly_json.length(); i++) {
                final JSONArray latlng = poly_json.getJSONArray( i );
                polygon.add( new LatLng( latlng.getDouble( 0 ), latlng.getDouble( 1 ) ) );
            }
        } catch (JSONException e) {
            Timber.w( e );
            return null;
        }
    
        return polygon;
    }
    
    /**
     * Decodes the JSON content of a `cyclic` shape.
     *
     * @param cyclic_json {@code JSONObject} describing the cyclic shape
     * @param factor      Growth / shrink factor that should be applied
     *
     * @return The {@code ArrayList} coming from {@link #cycle(LatLng, double, int, double)}.
     */
    @Nullable
    static ArrayList<LatLng> decode_cyclic_( final JSONObject cyclic_json,
                                             final float factor ) {
        try {
            final JSONArray c = cyclic_json.getJSONArray( "center" );
            
            final LatLng center = new LatLng( c.getDouble( 0 ), c.getDouble( 1 ) );
            final float  radius = cyclic_json.getInt( "radius" ) * factor;
            final int    sides  = cyclic_json.getInt( "sides" );
            
            return cycle( center, radius, sides, 0 ); // TODO check if a 0º starting angle is OK.
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Return an array of LatLng describing a geographic cycle of Earth.
     *
     * @param center         Center of the cycle
     * @param radius         Radius of the cycle in meters
     * @param sides          Amount of points used to compute the cycle points, eg: resolution
     * @param starting_angle The angle_to of the first point, others will be added from this angle_to in
     *                       clockwise increment.
     *
     * @return A polygon made of CW points.
     */
    @NonNull
    private static ArrayList<LatLng> cycle( final LatLng center,
                                            final double radius,
                                            final int sides,
                                            final double starting_angle ) {
        final double angle_inc = Math.PI * 2 / sides;
        final double distance  = radius / GeoUtils.earth_radius( center.getLatitude() );
    
        ArrayList<LatLng> polygon = new ArrayList<>();
    
        for (int i = 0; i < sides; i++) {
            polygon.add( GeoUtils.project_on_circle( center,
                                                     (float) distance,
                                                     starting_angle + i * angle_inc ) );
        }
    
        return polygon;
    }
}
