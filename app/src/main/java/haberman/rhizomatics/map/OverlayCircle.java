package haberman.rhizomatics.map;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.fsm.entities.PlaceEntity;

/**
 * {@link Overlay}'s child class used for parametric polygon.
 * For now, by `parametric`, we really (& only) mean circle geometry.
 *
 * @author haberman
 */

public class OverlayCircle extends Overlay
{
    
    public OverlayCircle( final PlaceEntity place ) {
        super( place );
    }
    
    /**
     * Computes rings given the {@link #place_}'s shape {@code JSONObject} (`center`, `radius` & `sides`).
     */
    @Override
    protected void compute_rings() {
        super.compute_rings();
        
        try {
            final JSONObject cyclic_json = place_.shape().getJSONObject( "cyclic" );
    
            final float growth_inc = 1f / (float) Constants.RINGS_AMOUNT;
            float       growth     = 1f;
            final int   s          = place_.points().size();
    
            for (short i = Constants.RINGS_AMOUNT; i > 0; --i) {
                ArrayList<LatLng> ring_points = new ArrayList<>( s );
                
                ring_points.addAll( PolyFactory.decode_cyclic_( cyclic_json, growth ) );
                
                PolyDrawer ring = new PolyDrawer( ring_points, null,
                                                  fill_gradient_.get_color_at( growth ),
                                                  stroke_gradient_.get_color_at( growth ),
                                                  STROKE_WIDTH, ALPHAS );
        
                rings_.add( ring );
                growth -= growth_inc;
            }
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
    }
}
