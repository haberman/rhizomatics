package haberman.rhizomatics.ui.pager;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

/**
 * Extends a {@code ViewPager} by overwriting its measurements method so its final height wraps its
 * entire content.
 *
 * @see <a href="https://stackoverflow.com/questions/8394681/android-i-am-unable-to-have-viewpager-wrap-content#20784791">stac</a>
 */

public class PagerWrapper extends ViewPager
{
    
    public PagerWrapper( Context context ) {
        super( context );
    }
    
    public PagerWrapper( Context context, AttributeSet attrs ) {
        super( context, attrs );
    }
    
    @Override
    protected void onMeasure( int widthMeasureSpec, int heightMeasureSpec ) {
        
        int mode = MeasureSpec.getMode( heightMeasureSpec );
        // Unspecified means that the ViewPager is in a ScrollView WRAP_CONTENT.
        // At Most means that the ViewPager is not in a ScrollView WRAP_CONTENT.
        if (mode == MeasureSpec.UNSPECIFIED || mode == MeasureSpec.AT_MOST) {
            // super has to be called in the beginning so the child views can be initialized.
            super.onMeasure( widthMeasureSpec, heightMeasureSpec );
            int height = 0;
            for (int i = 0; i < getChildCount(); i++) {
                View child = getChildAt( i );
                child.measure( widthMeasureSpec,
                               MeasureSpec.makeMeasureSpec( 0, MeasureSpec.UNSPECIFIED ) );
                int childMeasuredHeight = child.getMeasuredHeight();
                if (childMeasuredHeight > height) {
                    height = childMeasuredHeight;
                }
            }
            heightMeasureSpec = MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY );
        }
        // super has to be called again so the new specs are treated as exact measurements
        super.onMeasure( widthMeasureSpec, heightMeasureSpec );
    }
    
}
