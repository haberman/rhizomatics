package haberman.rhizomatics.events;

import android.support.annotation.NonNull;

/**
 * {@link IEvent} implementation for {@link haberman.rhizomatics.fsm.GPS} related events.
 *
 * @author haberman
 */

public class GPSEvent implements IEvent
{
    public static final String SEARCH  = "event_gps_search";
    public static final String START   = "event_gps_start";
    public static final String STOP    = "event_gps_stop";
    public static final String TIMEOUT = "event_gps_timeout";
    public static final String FAILURE = "event_gps_failure";
    
    /**
     * The type constant of this event.
     */
    @NonNull
    private final String event_type_;
    
    /**
     * Constructor.
     *
     * @param event_type The event type
     */
    public GPSEvent( @NonNull final String event_type ) {
        event_type_ = event_type;
    }
    
    @NonNull
    @Override
    public final String event_type() {
        return event_type_;
    }
}
