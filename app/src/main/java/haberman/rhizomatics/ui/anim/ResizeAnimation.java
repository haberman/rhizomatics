package haberman.rhizomatics.ui.anim;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * 2 states animator that will enlarge or shrink a given view on a specified axis.
 *
 * @author haberman
 */

public class ResizeAnimation extends Animation
{
    
    /** Reference to the view that receives transformations. */
    private final View view_;
    
    /** Axis along which view resizing occurs. */
    private final Axis axis_;
    
    /** Resize animation boundary by state. */
    private final float collapsed_;
    
    /** Current state of the animation: either {@link State#EXPANDED} or {@link State#COLLAPSED}. */
    private State state_;
    
    /** Holds the target value of animation for the {@link State#EXPANDED}. */
    private float expanded_;
    
    /** Start / end values of the animation, regardless of the state. */
    private float from_;
    private float to_;
    
    /** Whether the view is being transformed by the animation or not. */
    private boolean animating_ = false;
    
    /**
     * Constructor.
     *
     * @param view      The view on which animation occurs
     * @param collapsed The upper limit of the resize animation
     * @param axis      The axis on which animation applies
     */
    public ResizeAnimation( final View view, float collapsed, @Nullable final Axis axis ) {
        view_ = view;
        collapsed_ = collapsed;
        axis_ = axis != null ? axis : Axis.Y;
        state_ = State.COLLAPSED;
    
        setInterpolator( new AccelerateDecelerateInterpolator() );
        setDuration( view.getResources().getInteger( android.R.integer.config_shortAnimTime ) );
    }
    
    /** We need to inform that this animation transforms the view boundary. */
    @Override
    public boolean willChangeBounds() {
        return true;
    }
    
    /**
     * Method called during animation progress.
     *
     * @param interpolated_time The current 0/1 progress value
     * @param transformation    A {@code Transformation} instance
     */
    @Override
    protected void applyTransformation( float interpolated_time, Transformation transformation ) {
        float transform = (to_ - from_) * interpolated_time + from_;
    
        if (interpolated_time == 1 && animating_) {
            animating_ = false;
            state_ = state_.equals( State.EXPANDED ) ? State.COLLAPSED : State.EXPANDED;
        }
    
        resize_view( transform );
    }
    
    /**
     * Resize the view on the right axis with the given unity transform.
     *
     * @param transform A value between o and 1 indicating the animation progress.
     */
    private void resize_view( final float transform ) {
        final ViewGroup.LayoutParams layout_params = view_.getLayoutParams();
        
        if (axis_ == Axis.Y) {
            layout_params.height = (int) transform;
        } else if (axis_ == Axis.X) {
            layout_params.width = (int) transform;
        }
        
        view_.requestLayout();
    }
    
    /** Animates to {@link State#EXPANDED}. */
    public void expand() {
        if (state_.equals( State.EXPANDED ) || animating_) { return; }
    
        resize_view( collapsed_ );
    
        from_ = collapsed_;
        to_ = expanded_;
        animating_ = true;
    
        view_.startAnimation( this );
    }
    
    /** Animates to {@link State#COLLAPSED}. */
    public void collapse() {
        if (state_.equals( State.COLLAPSED ) || animating_) { return; }
    
        resize_view( expanded_ );
    
        from_ = expanded_;
        to_ = collapsed_;
        animating_ = true;
    
        view_.startAnimation( this );
    }
    
    public State state() { return state_; }
    
    /**
     * Sets {@link #view_} dimensions to the corresponding input state value without animating.
     *
     * @param state The state on which the animation should be driven.
     */
    public void set_state( final State state ) {
        state_ = state;
    
        if (state_.equals( State.COLLAPSED )) {
            resize_view( collapsed_ );
        } else if (state_.equals( State.EXPANDED )) {
            resize_view( expanded_ );
        }
    
        animating_ = false;
    }
    
    public void set_expanded( final int expanded ) {
        expanded_ = expanded;
    }
    
    /** Enum the possible transformation's axis. */
    public enum Axis
    {
        X, Y
    }
    
    /** Enum the possible animations' states. */
    public enum State
    {
        EXPANDED, COLLAPSED
    }
    
}
