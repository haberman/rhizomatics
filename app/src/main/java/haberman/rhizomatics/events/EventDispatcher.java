package haberman.rhizomatics.events;

import java.util.concurrent.CopyOnWriteArrayList;

/**
 * {@link IEventDispatcher} class implementation used to keep track of event dispatchers.
 *
 * @author haberman
 */
public class EventDispatcher implements IEventDispatcher
{
    /**
     * Thread-safe array of every handled listeners_.
     */
    private final CopyOnWriteArrayList<EventListener> listeners_ = new CopyOnWriteArrayList<>();
    
    /**
     * Adds a new listener.
     *
     * @param et The event type
     * @param h  An {@link IEventHandler} implementation
     */
    @Override
    public void add_listener( final String et, final IEventHandler h ) {
        final EventListener listener = new EventListener( et, h );
    
        remove_listener( et, h );
        listeners_.add( listener );
    }
    
    /**
     * Removes all listeners for a given event type.
     *
     * @param et The event type
     */
    @Override
    public void remove_listeners_for_event( final String et ) {
        for (final EventListener listener : listeners_) {
            if (listener.type().equals( et )) {
                listeners_.remove( listener );
            }
        }
    }
    
    /**
     * Remove all listeners for a given handler.
     *
     * @param h An handler's object implementation
     */
    @Override
    public void remove_listeners_for_handler( final IEventHandler h ) {
        for (final EventListener listener : listeners_) {
            if (listener.handler() == h) {
                listeners_.remove( listener );
            }
        }
    }
    
    /**
     * Removes a listener of a specific type from a given handler.
     *
     * @param et The event type
     * @param h  An handler's object implementation
     */
    @Override
    public void remove_listener( final String et, final IEventHandler h ) {
        for (final EventListener listener : listeners_) {
            if (listener.type().equals( et ) && listener.handler() == h) {
                listeners_.remove( listener );
            }
        }
    }
    
    /**
     * Removes every listeners from the {@link #listeners_} list.
     */
    @Override
    public void remove_listeners() {
        listeners_.clear();
    }
    
    /**
     * Dispatches an event.
     *
     * @param e An event's object implementation.
     */
    @Override
    public void dispatch_event( final IEvent e ) {
        for (final EventListener listener : listeners_) {
            if (e.event_type().equals( listener.type() )) {
                listener.handler().on_event( e );
            }
        }
    }
}