package haberman.rhizomatics.fsm;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.StateMachineConfig;
import com.github.oxo42.stateless4j.delegates.Action;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;

import java.util.ArrayList;
import java.util.HashMap;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.SoundEvent;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import haberman.rhizomatics.fsm.entities.SoundEntity;
import haberman.rhizomatics.map.Overlay;
import haberman.rhizomatics.map.OverlayCircle;
import haberman.rhizomatics.map.OverlayPolygon;
import haberman.rhizomatics.utils.GeoUtils;
import haberman.rhizomatics.utils.MathUtils;
import timber.log.Timber;

/**
 * The Place state machine that drives the map related updates for a {@link AudioPlayer.AudioType#READING} overlay.
 *
 * @author haberman
 */
public class Place
{
    
    @DrawableRes
    private final int MARKER_NORMAL = R.drawable.bm_marker;
    
    @DrawableRes
    private final int MARKER_SELECT = R.drawable.bm_marker_selected;
    
    /** {@link haberman.rhizomatics.MainActivity}'s {@code Context} backref. */
    private final Context context_;
    
    /** The {@link PlaceEntity} handled by this state machine. */
    private final PlaceEntity place_entity_;
    
    /** A list of {@link Overlay} indexed by {@link SoundEntity.Where}. */
    private final HashMap<SoundEntity.Where, Overlay> overlays_;
    
    /** The minimum distance between this place centroid and its outer polygon. */
    private final short distance_from_center_;
    
    /** The state machine. */
    private final StateMachine<AppState, AppTrigger> fsm_;
    
    /** A marker that could be displayed in any state but {@link AppState#STOPPED}. */
    @Nullable
    private Marker marker_;
    
    /** The marker {@code DrawableRes} which is used to guess a {@code selected} state boolean. */
    @DrawableRes
    private int marker_res_ = MARKER_NORMAL;
    
    /** The distance between the user {@link SharedBuffer#location()} and the centroid of this place. */
    private short distance_from_user_;
    
    /**
     * Constructor
     *
     * @param context      Application {@code Context}
     * @param place_entity The entity handled by the state machine
     */
    @SuppressWarnings("ConstantConditions")
    public Place( @NonNull final Context context, @NonNull final PlaceEntity place_entity ) {
        context_ = context;
        place_entity_ = place_entity;
        
        overlays_ = new HashMap<>();
        
        final ArrayList<SoundEntity> sounds = SharedBuffer.manager()
                                                          .all_sounds( place_entity_.id() );
        
        for (final SoundEntity sound_entity : sounds) {
            final Overlay overlay;
            
            if (place_entity_.shape().has( "polygon" )) {
                overlay = new OverlayPolygon( place_entity_ );
            } else if (place_entity_.shape().has( "cyclic" )) {
                overlay = new OverlayCircle( place_entity_ );
            } else {
                overlay = null;
            }
            
            overlays_.put( sound_entity.where(), overlay );
        }
        
        distance_from_center_ = (short) GeoUtils.min_distance( place_entity_.center(),
                                                               place_entity_.points() );
        
        final StateMachineConfig<AppState, AppTrigger> fsm_config = new StateMachineConfig<>();
        
        fsm_config.configure( AppState.CREATED )
                  .permit( AppTrigger.enter, AppState.INSIDE )
                  .permit( AppTrigger.exit, AppState.OUTSIDE );
        
        fsm_config.configure( AppState.INSIDE )
                  .onEntry( new EnterEntry() )
                  .permit( AppTrigger.exit, AppState.OUTSIDE )
                  .permit( AppTrigger.stop, AppState.STOPPED )
                  .permitReentry( AppTrigger.enter );
        
        fsm_config.configure( AppState.OUTSIDE )
                  .onEntry( new ExitEntry() )
                  .permit( AppTrigger.enter, AppState.INSIDE )
                  .permit( AppTrigger.stop, AppState.STOPPED )
                  .permitReentry( AppTrigger.exit );
        
        fsm_config.configure( AppState.STOPPED )
                  .onEntry( new StopEntry() )
                  .permit( AppTrigger.enter, AppState.INSIDE )
                  .permit( AppTrigger.exit, AppState.OUTSIDE )
                  .permit( AppTrigger.destroy, AppState.IDLE );
        
        fsm_ = new StateMachine<>( AppState.CREATED, fsm_config );
        fsm_.onUnhandledTrigger( ( arg1, arg2 ) -> Timber.i( "unhandled  trigger %s from state %s ",
                                                             arg2,
                                                             fsm_.getState() ) );
    }
    
    /** {@link AppTrigger#stop} trigger. */
    void stop_() {
        fsm_.fire( AppTrigger.stop );
    }
    
    /**
     * Conditional trigger that will set the machine's state to:
     * <ul>
     * <li>{@link AppTrigger#enter} if the user is within;</li>
     * <li>{@link AppTrigger#exit} if the user is at least {@link Constants#DISTANCE_PLACE_VISIBILITY} meters away;</li>
     * <li>{@link AppTrigger#stop} otherwise.</li>
     * </ul>
     */
    void update_() {
        distance_from_user_ = (short) SharedBuffer.location().distanceTo( place_entity_.center() );
        
        AppTrigger next_trigger = AppTrigger.stop;
        
        if (GeoUtils.in_polygon( SharedBuffer.location(), place_entity_.points() )) {
            next_trigger = AppTrigger.enter;
        } else if (distance_from_user_ < Constants.DISTANCE_PLACE_VISIBILITY) {
            next_trigger = AppTrigger.exit;
        }
        
        if (fsm_.canFire( next_trigger )) { fsm_.fire( next_trigger ); }
    }
    
    /**
     * Shows / hides the marker.
     *
     * @param value Whether to show the marker or not
     */
    public void mark( final boolean value ) {
        if (value) {
            final MarkerOptions marker_options = new MarkerOptions()
                    .position( place_entity_.center() )
                    .icon( IconFactory.getInstance( context_ ).fromResource( marker_res_ ) );
            
            if (marker_ == null) {
                marker_ = SharedBuffer.map().addMarker( marker_options );
            }
        } else {
            marker_res_ = MARKER_NORMAL;
            
            assert marker_ != null;
            SharedBuffer.map().removeMarker( marker_ );
            marker_ = null;
        }
    }
    
    @NonNull
    public final PlaceEntity entity() { return place_entity_; }
    
    @Nullable
    public final Marker marker() { return marker_; }
    
    /**
     * Set the {@link #marker_} to be {@link #MARKER_SELECT}ed (as the current active place).
     *
     * @param select Whether to mark this place as selected or not
     */
    public void marker_select( final boolean select ) {
        if (marker_ == null) { return; }
        
        marker_res_ = select ? MARKER_SELECT : MARKER_NORMAL;
        marker_.setIcon( IconFactory.getInstance( context_ ).fromResource( marker_res_ ) );
    }
    
    public void in() { overlays_.get( SoundEntity.Where.INSIDE ).in(); }
    
    /**
     * Returns the current state of the {@link #fsm_}.
     *
     * @return An {@link AppState} variable.
     */
    public AppState state() { return fsm_.getState(); }
    
    public void apply_sound_event( SoundEvent event ) {
        overlays_.get( SoundEntity.Where.INSIDE ).set_spectrum( event.spectrum() );
    }
    
    /** States enum. */
    public enum AppState
    {
        CREATED, INSIDE, OUTSIDE, STOPPED, IDLE
    }
    
    /** Trigger enum. */
    private enum AppTrigger
    {
        enter, exit, stop, destroy
    }
    
    /**
     * Action taken when the state machine enters {@link AppState#INSIDE}
     * -> calls {@link Overlay#in()} * for every {@link SoundEntity} being {@link SoundEntity.Where#INSIDE}
     */
    private class EnterEntry implements Action
    {
        @Override
        public void doIt() { overlays_.get( SoundEntity.Where.INSIDE ).in(); }
    }
    
    /**
     * Action taken when the state machine exits {@link AppState#INSIDE}
     * -> computes the opacity given the actual {@link #distance_from_user_}, calls {@link Overlay#out(float)}
     * for every {@link SoundEntity} being {@link SoundEntity.Where#INSIDE}.
     */
    private class ExitEntry implements Action
    {
        @Override
        public void doIt() {
            final float opacity = MathUtils.map_number( distance_from_user_,
                                                        Constants.DISTANCE_PLACE_VISIBILITY,
                                                        distance_from_center_, 0f, 1f );
            
            overlays_.get( SoundEntity.Where.INSIDE ).out( opacity );
        }
    }
    
    /**
     * Action taken when the state machine enters {@link AppState#STOPPED}
     * -> calls {@link Overlay#off()} for every {@link SoundEntity} being {@link SoundEntity.Where#INSIDE}.
     */
    private class StopEntry implements Action
    {
        @Override
        public void doIt() {
            overlays_.get( SoundEntity.Where.INSIDE ).off();
        }
    }
    
}
