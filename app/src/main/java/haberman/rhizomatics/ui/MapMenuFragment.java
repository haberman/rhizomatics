package haberman.rhizomatics.ui;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Space;

import haberman.rhizomatics.R;
import haberman.rhizomatics.ui.toggles.ToggleButton;

/**
 * Fragment that simply inflates the {@link R.layout#mapmenu_fragment} layout.
 *
 * @author haberman
 */

public class MapMenuFragment extends Fragment
{
    private ToggleButton location_toggle, marker_toggle;
    private Space toggle_space, trailing_space;
    
    public View onCreateView( @NonNull LayoutInflater inflater,
                              ViewGroup container,
                              Bundle bundle ) {
        final FrameLayout wrapper_ = (FrameLayout) inflater.inflate( R.layout.mapmenu_fragment,
                                                                     container,
                                                                     false );
        
        location_toggle = wrapper_.findViewById( R.id.location_toggle );
        marker_toggle = wrapper_.findViewById( R.id.marker_toggle );
        
        toggle_space = wrapper_.findViewById( R.id.toggle_space );
        trailing_space = wrapper_.findViewById( R.id.trailing_space );
        
        return wrapper_;
    }
    
    public void credits_mode( final boolean value ) {
        final int visibility = value ? View.GONE : View.VISIBLE;
        
        location_toggle.setVisibility( visibility );
        marker_toggle.setVisibility( visibility );
        
        toggle_space.setVisibility( visibility );
        trailing_space.setVisibility( visibility );
    }
}
