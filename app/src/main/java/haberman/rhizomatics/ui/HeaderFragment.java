package haberman.rhizomatics.ui;

import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.drawable.Animatable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import android.support.annotation.StringRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.UXEvent;
import haberman.rhizomatics.fsm.AudioPlayer;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import haberman.rhizomatics.ui.anim.ResizeAnimation;
import haberman.rhizomatics.ui.anim.SlideAnimation;
import haberman.rhizomatics.ui.pager.PagerAdapter;
import haberman.rhizomatics.ui.pager.PagerFragment;
import haberman.rhizomatics.ui.pager.PagerWrapper;
import haberman.rhizomatics.utils.ResourceUtils;
import timber.log.Timber;

import static haberman.rhizomatics.fsm.AudioPlayer.AppState.IDLE;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PLAYING;

/**
 * Fragment that inflates the {@link R.layout#header_fragment} layout with a bunch of methods
 * to access & control UX.
 *
 * @author haberman
 */

public class HeaderFragment extends Fragment implements View.OnClickListener,
                                                        Animation.AnimationListener
{
    private LinearLayout wrapper_;
    /**
     * Header views
     */
    private Button       cheveron_button_;
    private Button       player_button_;
    private ProgressBar  player_progress_;
    
    /** Info views */
    private TextView info_title_;
    private TextView info_snippet_;
    private String reading_id_ = null;
    
    /** Reading views */
    private LinearLayout reading_wrapper_;
    private PagerWrapper reading_pager_;
    private TabLayout    reading_dots_;
    
    @Nullable
    private PlaceEntity place_entity_;
    /** A possible place entity to consume when playing a new reading over an existing one. */
    @Nullable
    private PlaceEntity queue_entity_;
    
    /** View animations */
    private ResizeAnimation resize_animation_;
    private SlideAnimation  slide_animation_;
    
    /** Auto close threads */
    private Handler  autoclose_handler_;
    private Runnable autoclose_runnable_;
    
    /** Header constant height (given by Resources) */
    private int header_dim_;
    
    /**
     * It may happen that an animation is requested before one is finished, in that case: we store
     * the future state to transit to, once the running animation has ended.
     */
    @Nullable
    private SlideAnimation.State next_state_ = null;
    
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container, Bundle bundle ) {
        header_dim_ = (int) getResources().getDimension( R.dimen.header_height );
        
        wrapper_ = (LinearLayout) inflater.inflate( R.layout.header_fragment, container, false );
        
        reading_wrapper_ = wrapper_.findViewById( R.id.reading_wrapper );
        reading_pager_ = reading_wrapper_.findViewById( R.id.reading_pager );
        reading_dots_ = reading_wrapper_.findViewById( R.id.reading_dots );
        reading_dots_.setupWithViewPager( reading_pager_, true );
        
        info_title_ = wrapper_.findViewById( R.id.info_title );
        info_snippet_ = wrapper_.findViewById( R.id.info_snippet );
        
        player_button_ = wrapper_.findViewById( R.id.player_button );
        player_button_.setOnClickListener( this );
        player_progress_ = wrapper_.findViewById( R.id.player_progress );
        
        cheveron_button_ = wrapper_.findViewById( R.id.cheveron_button );
        cheveron_button_.setOnClickListener( this );
        
        slide_animation_ = new SlideAnimation( wrapper_, header_dim_, SlideAnimation.Side.TOP );
        slide_animation_.setAnimationListener( this );
        
        resize_animation_ = new ResizeAnimation( wrapper_, header_dim_, ResizeAnimation.Axis.Y );
        resize_animation_.setAnimationListener( this );
        
        autoclose_handler_ = new Handler();
        autoclose_runnable_ = () -> hide();
        
        wrapper_.setAlpha( 0 );
        wrapper_.setY( slide_animation_.hidden() );
        
        return wrapper_;
    }
    
    @Override
    public void onDestroyView() {
        player_button_.setOnClickListener( null );
        cheveron_button_.setOnClickListener( null );
        
        slide_animation_.setAnimationListener( null );
        slide_animation_ = null;
        resize_animation_.setAnimationListener( null );
        resize_animation_ = null;
        
        autoclose_handler_.removeCallbacks( autoclose_runnable_ );
        autoclose_runnable_ = null;
        autoclose_handler_ = null;
        
        reading_pager_.setAdapter( null );
        reading_pager_ = null;
        
        reading_dots_.setupWithViewPager( null );
        reading_dots_ = null;
        
        info_title_ = null;
        info_snippet_ = null;
        player_button_ = null;
        cheveron_button_ = null;
        reading_wrapper_ = null;
        wrapper_ = null;
        
        super.onDestroyView();
    }
    
    
    /**
     * Switches {@link #player_progress_} visibility.
     *
     * @param value Whether to display the progress icon or not.
     */
    public void ui_busy( final boolean value ) {
        if (value) {
            player_button_.setVisibility( View.GONE );
            player_progress_.setVisibility( View.VISIBLE );
        } else {
            player_button_.setVisibility( View.VISIBLE );
            player_progress_.setVisibility( View.GONE );
        }
    }
    
    /** Makes the {@link #wrapper_} visible if the  {@link #slide_animation_} has been started. */
    @Override
    public void onAnimationStart( Animation animation ) {
        if (animation.equals( slide_animation_ )) {
            wrapper_.setVisibility( View.VISIBLE );
        }
    }
    
    @Override
    public void onAnimationRepeat( Animation animation ) { /* pass */ }
    
    /**
     * Tweaks some bits of ui when animations end, meaning:
     * <ul>
     * <li>when ending from {@link #resize_animation_}: consumes an eventual {@link #reading_id_} into
     * a new adapter and hides the {@link #reading_pager_} if ending into {@link ResizeAnimation.State#COLLAPSED} state;</li>
     * <li>when ending from {@link #slide_animation_} into {@link SlideAnimation.State#HIDDEN} state:
     * ensures that the {@link #reading_pager_} is in {@link ResizeAnimation.State#COLLAPSED} state
     * and takes the {@link #wrapper_} off the screen.</li>
     * </ul>
     */
    @Override
    public void onAnimationEnd( Animation animation ) {
        if (animation.equals( resize_animation_ )) {
            
            if (reading_id_ != null) {
                set_lecture_adapter( reading_id_ );
                reading_id_ = null;
            }
            
            if (resize_animation_.state().equals( ResizeAnimation.State.COLLAPSED )) {
                reading_wrapper_.setVisibility( View.GONE );
            }
        } else if (slide_animation_.state().equals( SlideAnimation.State.HIDDEN )) {
            if (resize_animation_.state().equals( ResizeAnimation.State.EXPANDED )) {
                cheveron_button_.setBackground( getActivity().getDrawable( R.drawable.avd_chevron_down_up ) );
                resize_animation_.set_state( ResizeAnimation.State.COLLAPSED );
                reading_wrapper_.setVisibility( View.GONE );
            }
            
            wrapper_.setVisibility( View.GONE );
        }
        
        if (next_state_ != null) {
            switch (next_state_) {
                case VISIBLE:
                    if (queue_entity_ != null) {
                        show_reading( queue_entity_ );
                        ui_busy( true );
                        queue_entity_ = null;
                    }
                    slide_animation_.show();
                    break;
                case HIDDEN:
                    slide_animation_.hide();
                    break;
            }
            next_state_ = null;
        }
    }
    
    /** Simply slides the header in, with no interest in the content it may hold (or not). */
    public void show() {
        if (slide_animation_.show()) {
            slide_animation_.start();
        } else {
            next_state_ = SlideAnimation.State.VISIBLE;
        }
    }
    
    /** Hides the fragment. */
    public void hide() {
        if (slide_animation_.hide()) {
            slide_animation_.start();
        } else {
            next_state_ = SlideAnimation.State.HIDDEN;
        }
    }
    
    /**
     * Global click listeners that respond to both play_/pause interactions expand/collapse interactions.
     *
     * @param view The clicked view.
     */
    @Override
    public void onClick( View view ) {
        if (view.equals( cheveron_button_ )) {
            if (resize_animation_.state().equals( ResizeAnimation.State.EXPANDED )) {
                ui_collapse_();
            } else {
                ui_expand_();
            }
        } else if (view.equals( player_button_ )) {
            String action = null;
            switch (SharedBuffer.audio_state()) {
                case PLAYING:
                    switch (SharedBuffer.audio_type()) {
                        case READING:
                            ui_play();
                            action = PlayerAction.PAUSE.toString();
                            break;
                        case SOUNDSCAPE:
                            action = PlayerAction.NEXT.toString();
                            break;
                    }
                    break;
                case COMPLETED:
                    ui_pause();
                    action = PlayerAction.REPLAY.toString();
                    break;
                default:
                    ui_busy( true );
                    action = PlayerAction.PLAY.toString();
            }
            
            if (action != null) {
                SharedBuffer.dispatcher()
                            .dispatch_event( new UXEvent( UXEvent.PLAYER_ACTION, action ) );
            }
        }
    }
    
    /** Collapses the {@link #reading_pager_} below header. */
    private void ui_collapse_() {
        resize_animation_.collapse();
        
        cheveron_button_.setBackground( getActivity().getDrawable( R.drawable.avd_chevron_up_down ) );
        ((Animatable) cheveron_button_.getBackground()).start();
    }
    
    /**
     * Expands the header so it fits the biggest {@link #reading_pager_} {@link PagerFragment} height.
     * <p>
     * A this point, we don't know the size of the pager (its visibility is {@code View.GONE}), so
     * we first take it out of the screen (to avoid a one-frame glitch that would sadly resize the
     * entire header) and sets its visibility to {@code View.VISIBLE}. In a `once` manner, we catch a
     * layout measurement where we finally replace the pager at the right position before shooting
     * the {@link ResizeAnimation#expand()} animation with proper value.
     * </p>
     */
    private void ui_expand_() {
        /* Takes lecture_pager out of the screen. */
        final LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) reading_wrapper_
                .getLayoutParams();
        params.topMargin = -SharedBuffer.screen_height();
        reading_wrapper_.setLayoutParams( params );
        
        /* Adds a new layout listener with rightfully measured dimensions to launch animation. */
        final ViewTreeObserver vt = reading_wrapper_.getViewTreeObserver();
        vt.addOnGlobalLayoutListener( new ViewTreeObserver.OnGlobalLayoutListener()
        {
            @Override
            public void onGlobalLayout() {
                if (reading_wrapper_.getVisibility() == View.VISIBLE) {
                    /* Set new expanded value and starts animation. */
                    final float expanded = reading_wrapper_.getMeasuredHeight() + header_dim_;
                    resize_animation_.set_expanded( (int) expanded );
                    resize_animation_.expand();
                    
                    /* Reset {@code topMargin} pager to 0. */
                    params.topMargin = 0;
                    reading_wrapper_.getViewTreeObserver().removeOnGlobalLayoutListener( this );
                    reading_wrapper_.setLayoutParams( params );
                    
                    cheveron_button_.setBackground( getActivity().getDrawable( R.drawable.avd_chevron_down_up ) );
                    ((Animatable) cheveron_button_.getBackground()).start();
                }
                
            }
        } );
        
        // Triggers a layout measurement.
        reading_wrapper_.setVisibility( View.VISIBLE );
    }
    
    /** Sets {@link #player_button_} ui state to play_. */
    public void ui_play() {
        player_button_.setBackground( getActivity().getDrawable( R.drawable.vd_play ) );
        player_button_.setVisibility( View.VISIBLE );
    }
    
    /** Sets {@link #player_button_} ui state to pause. */
    public void ui_pause() {
        player_button_.setBackground( getActivity().getDrawable( R.drawable.vd_pause ) );
        player_button_.setVisibility( View.VISIBLE );
    }
    
    
    /**
     * Passes a new adapter to the {@link #reading_pager_} by splitting an content of an input md
     * file into distinct paragraphs.
     *
     * @param lecture_id The id / name of the lecture.
     *
     * @see ResourceUtils#text_to_paragraphs(Resources, int)
     */
    private void set_lecture_adapter( @NonNull final String lecture_id ) {
        @RawRes final int md_file = getResources().getIdentifier( lecture_id + "_txt",
                                                                  "raw",
                                                                  getActivity().getPackageName() );
        
        final ArrayList<String> paragraphs = ResourceUtils.text_to_paragraphs( getResources(),
                                                                               md_file );
        final FragmentManager fm      = ((FragmentActivity) getActivity()).getSupportFragmentManager();
        final PagerAdapter    adapter = new PagerAdapter( fm, paragraphs );
        
        assert paragraphs != null;
        reading_pager_.setOffscreenPageLimit( paragraphs.size() ); // we can afford that
        reading_pager_.setAdapter( adapter );
    }
    
    /**
     * Displays the fragment with content coming from a {@link PlaceEntity}.
     *
     * @param place_entity Entity to read values from
     */
    public void show_reading( @Nullable final PlaceEntity place_entity ) {
        if (place_entity != null) {
            place_entity_ = place_entity;
        }
        
        if (place_entity_ == null) {
            return;
        }
        
        show_( place_entity_.name(),
               getResources().getString( R.string.by ) + " " + place_entity_.author(),
               place_entity_.id() );
    }
    
    /**
     * Shows the header display.
     *
     * @param title   One line text of title
     * @param snippet One line text of info
     * @param id      An eventual id to be associated with raw markdown files
     */
    private void show_( @NonNull final String title,
                        @NonNull final String snippet,
                        @Nullable final String id ) {
        info_title_.setText( title );
        info_snippet_.setText( snippet );
        
        final AudioPlayer.AppState audio_state = SharedBuffer.audio_state();
        
        if (audio_state.equals( PLAYING )) {
            switch (SharedBuffer.audio_type()) {
                case READING:
                    ui_pause();
                    break;
                case SOUNDSCAPE:
                    ui_next();
                    break;
                
            }
        } else if (audio_state.equals( IDLE )) { ui_play(); }
        
        if (id != null) {
            cheveron_button_.setVisibility( View.VISIBLE );
            
            if (resize_animation_.state().equals( ResizeAnimation.State.EXPANDED )) {
                reading_id_ = id;
                ui_collapse_();
            } else {
                reading_id_ = null;
                set_lecture_adapter( id );
            }
        } else { cheveron_button_.setVisibility( View.GONE ); }
        
        slide_animation_.show();
    }
    
    /** Sets {@link #player_button_} ui state to skip. */
    public void ui_next() {
        player_button_.setBackground( getActivity().getDrawable( R.drawable.vd_next ) );
        player_button_.setVisibility( View.VISIBLE );
    }
    
    
    /**
     * Displays a given {@code String} xml resource as an error.
     *
     * @param snippet A {@code StringRes} integer.
     *
     * @see #error_(String)
     */
    public void error( @StringRes final int snippet ) {
        ui_error_();
        error_( getResources().getString( snippet ) );
    }
    
    /** Sets {@link #player_button_} ui state to error. */
    private void ui_error_() {
        player_button_.setBackground( getActivity().getDrawable( R.drawable.vd_error ) );
        player_button_.setVisibility( View.VISIBLE );
    }
    
    /**
     * Displays an error.
     *
     * @param snippet A mandatory error's description
     */
    private void error_( @NonNull final String snippet ) {
        player_button_.setVisibility( View.INVISIBLE );
        player_progress_.setVisibility( View.INVISIBLE );
        
        show_( getResources().getString( R.string.error ), snippet, null );
        
        autoclose_handler_.postDelayed( autoclose_runnable_, 10000 );
    }
    
    /**
     * Sets header information.
     *
     * @param title   Text of the title
     * @param snippet Text of the snippet
     */
    public void show( final int title, @StringRes final int snippet ) {
        show_( getResources().getString( title ), getResources().getString( snippet ), null );
    }
    
    /**
     * Indicates that the player holds queued audio (set when entering a place).
     * -> hides the header and stores the place entity to be consumed when header becomes visible again.
     *
     * @param place_entity The entity to keep in the {@link #queue_entity_}
     */
    public void queue_reading( PlaceEntity place_entity ) {
        if (visible()) { hide(); }
        
        next_state_ = SlideAnimation.State.VISIBLE;
        queue_entity_ = place_entity;
        
    }
    
    public boolean visible() {
        return wrapper_.getVisibility() == View.VISIBLE;
    }
    
    /** Sets player UI to holds a `soundscape` track. */
    public void show_soundscape() {
        place_entity_ = null;
        
        ui_next();
        show_( getResources().getString( R.string.soundscape ),
               getResources().getString( R.string.by ) + " Philippe Franck", null );
    }
    
    /** Sets {@link #player_button_} ui state to replay. */
    public void ui_replay() {
        player_button_.setBackground( getActivity().getDrawable( R.drawable.vd_replay ) );
        player_button_.setVisibility( View.VISIBLE );
    }
    
    @Nullable
    public PlaceEntity place_entity() {
        return place_entity_;
    }
    
    /** Enum to list possible UI state. */
    public enum PlayerAction
    {
        PLAY, PAUSE, NEXT, REPLAY
    }
}
