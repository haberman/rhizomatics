Aaah la Ferme du Biéreau. 
Une ferme, à Louvain-la-Neuve ? 
Oui, une ferme. 
Non, en fait, sept fermes à Louvain-la-Neuve. 

Reliques d’un temps où les prairies constituaient l’unique paysage de la future cité, elles ont été préservées par les pères du projet. Ils souhaitaient que survivent les traces d’un passé agricole, avant que l’activité intellectuelle ne s’installe et ne domine, avant que l’architecture moderne ne s’impose. Il fallait que subsiste le terreau sur lequel la ville naîtrait et grandirait ensuite, sans discontinuer.

Nous sommes ici face à l’une des plus anciennes fermes. Construite au XIIème siècle selon la volonté de l’abbaye de Florival, elle en demeure la propriété durant plus de six cents ans. On aperçoit d’ailleurs encore, sur la façade intérieure du corps de logis, le blason de l’une des abbesses orné de quatre coquilles Saint-Jacques. L’exploitation est ensuite vendue comme « bien national » et plusieurs familles se succèderont pour y perpétuer le travail de la terre.

Néanmoins, contrairement à la ferme de Lauzelle, aujourd’hui encore en activité, celle du Biéreau est revendue à l’Université Catholique de Louvain en 1969 et l’on y cesse de cultiver en 1972. Des habitants l’occuperont jusqu’en 1977, s’appropriant les lieux désormais vides pour y organiser des événements culturels et festifs. Comme quatre des sept exploitations de la localité, elle est ainsi vouée aux arts. Des arts vivants, amenés par les résidents autant que par les étudiants récemment installés. C’est donc ici, notamment, que démarre l’activité artistique de Louvain « la naissante ».

Toutefois, des signes continuaient à attester des deux sens du mot « culture » : c’est en bottes que l’on allait aux concerts ou aux cours, c’est dans les anciennes écuries que le folklore battait son plein et c’est dans un vieux four à pain, non loin de là, que l’on discutait et se sustentait.

Si, aujourd’hui, les étudiants préfèrent les baskets aux bottes et les sandwichs garnis aux miches d’antan, ils n’ont pas fini de fréquenter la ferme (les habitants non plus, d’ailleurs). Comme la fosse à purin qui se dessine au centre de la cour et offre un terrain fertile pour les fleurs et l’herbe désormais domestiquées, l’exploitation a changé de vocation : à côté des mangeoires, sous les poutres de la grange, sur le plancher des vaches, sont organisés festivals, concerts et événements en tous genres. On s’y rencontre, on s’y mêle et on apprécie le cadre traditionnel dont l’odeur des écuries et, tous les mardis, les étals d’un fermier-maraicher nous rappellent qu’il s’agit d’un passé agricole… pas si lointain. 

Un bâtiment riche d’un passé fondateur, d’un présent enthousiasmant mais aussi d’un futur prometteur ! On y envisage en effet de continuer les rénovations afin d’augmenter et d’améliorer les surfaces destinées au théâtre, à la musique, aux expositions… La Ferme du Biéreau n’a pas fini de produire, de nourrir, d’accueillir.

Notons encore qu’au-delà du parking pavé longeant la ferme, tel un écho des anciennes pratiques agricoles, les serres de la faculté des sciences démontrent que l’on cultive toujours au Biéreau. Se parant, le soir venu, de couleurs semblables à celles des projecteurs de la grange en face, elles assurent ainsi la « photo »-synthèse des cultures.