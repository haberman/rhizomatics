package haberman.rhizomatics.utils;

/**
 * Created by haberman on 17/10/17.
 */

public class MathUtils
{
    public static float random_range( final float min, final float max ) {
        return (float) (min + Math.random() * (max - min));
    }
    
    /**
     * Maps a given input number from a range to another one.
     *
     * @param value   The input number to map
     * @param in_min  The input's minimum value
     * @param in_max  The input's maximum value
     * @param out_min The outputs's minimum value
     * @param out_max The outputs's maximum value
     *
     * @return A float.
     */
    public static float map_number( float value,
                                    float in_min,
                                    float in_max,
                                    float out_min,
                                    float out_max ) {
        return (value - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
}