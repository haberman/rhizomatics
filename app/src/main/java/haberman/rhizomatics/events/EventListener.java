package haberman.rhizomatics.events;

import android.support.annotation.NonNull;

/**
 * Minimal event listener.
 *
 * @author haberman
 */
class EventListener
{
    /** String type to identify the type of event being fired. */
    @NonNull
    private final String event_type_;
    
    /** An implementation of the event handler interface. */
    @NonNull
    private final IEventHandler handler_;
    
    /**
     * Constructor.
     *
     * @param event_type The event type
     * @param handler    The handler
     */
    public EventListener( @NonNull String event_type, @NonNull IEventHandler handler ) {
        event_type_ = event_type;
        handler_ = handler;
    }
    
    @NonNull
    public String type() {
        return event_type_;
    }
    
    @NonNull
    public IEventHandler handler() {
        return handler_;
    }
}