package haberman.rhizomatics.ui.pager;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.Objects;

import haberman.rhizomatics.R;

/**
 * Pager layout setup that simply sets the content of the {@code TextView} from the bundled arguments.
 *
 * @author haberman
 */

public class PagerFragment extends Fragment
{
    public View onCreateView( @NonNull LayoutInflater inflater, ViewGroup container, Bundle bundle ) {
        final Bundle args = getArguments();
        final FrameLayout wrapper = (FrameLayout) inflater.inflate( R.layout.pager_fragment,
                                                                    container,
                                                                    false );
    
        final TextView lecture_content = wrapper.findViewById( R.id.lecture_content );
        lecture_content.setText( Objects.requireNonNull( args ).getString( "content" ) );
    
        return wrapper;
    }
}