package haberman.rhizomatics.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import haberman.rhizomatics.R;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import haberman.rhizomatics.fsm.entities.SoundEntity;
import haberman.rhizomatics.utils.ResourceUtils;

/**
 * This class extends {@code SQLiteOpenHelper} and allows data-driven map features creation. It acts
 * as a intermediate between a live data  API (currently hard-coded under {@code R.raw.}) and the
 * end-user interactions.
 *
 * @author haberman
 */

public class DataManager extends SQLiteOpenHelper
{
    
    @RawRes
//    private static final int JSON_BACKEND = R.raw.st_gilles;
    private static final int JSON_BACKEND = R.raw.rhizomatics;
    
    /**
     * Database constants
     */
    private static final int    DATABASE_VERSION = 5;
    private static final String DATABASE_NAME    = "rhizomatics.db";
    
    /**
     * Places tables's fields.
     */
    private static final String PLACE_ID     = "place_id";
    private static final String PLACE_NAME   = "place_name";
    private static final String PLACE_AUTHOR = "place_author";
    private static final String PLACE_THEME  = "place_theme";
    private static final String PLACE_SHAPE  = "place_shape";
    
    private static final String   PLACE_TABLE   = "places";
    private static final String[] PLACE_COLUMNS = { PLACE_ID,
                                                    PLACE_NAME,
                                                    PLACE_AUTHOR,
                                                    PLACE_THEME,
                                                    PLACE_SHAPE };
    private static final String[] PLACE_TYPES   = { "TEXT PRIMARY KEY",
                                                    "TEXT",
                                                    "TEXT",
                                                    "TEXT",
                                                    "TEXT" };
    
    /**
     * Sounds tables's fields.
     */
    private static final String SOUND_ID    = "sound_id";
    private static final String SOUND_TRACK = "sound_track";
    private static final String SOUND_WHERE = "sound_where";
    private static final String SOUND_PLACE = "sound_place";
    
    private static final String   SOUND_TABLE   = "sounds";
    private static final String[] SOUND_COLUMNS = { SOUND_ID,
                                                    SOUND_TRACK,
                                                    SOUND_WHERE,
                                                    SOUND_PLACE };
    private static final String[] SOUND_TYPES   = { "INTEGER PRIMARY KEY AUTOINCREMENT",
                                                    "TEXT",
                                                    "TEXT",
                                                    "TEXT" };
    
    /**
     * Application's context background.
     */
    private final Context          context_;
    /**
     * Readable & writable persistent databases' connections.
     */
    private final SQLiteDatabase[] dbs_;
    /**
     * Keeps an in-memory tracks of all places.
     */
    @Nullable
    private ArrayList<PlaceEntity> places_ = null;
    
    /**
     * Constructor.
     * Creates a new child instance of {@code SQLiteOpenHelper} and a {@code SoundPool}.
     *
     * @param context The base {@code Context} of this app.
     */
    public DataManager( @NonNull Context context ) {
        super( context, DATABASE_NAME, null, DATABASE_VERSION );
    
        context_ = context;
        dbs_ = new SQLiteDatabase[ 2 ];
    
        dbs_[ 0 ] = getReadableDatabase();
        dbs_[ 1 ] = getWritableDatabase();
    }
    
    /**
     * Given a list of {@link PlaceEntity}s and a reference point, returns the same list without its
     * farthest location.
     *
     * @param reference The {@code LatLng} reference's point
     * @param places    A list of {@code LatLng} to compare
     *
     * @return The filtered list.
     */
    public static ArrayList<PlaceEntity> remove_farthest_place( PlaceEntity reference,
                                                                ArrayList<PlaceEntity> places ) {
        // clones the input list
        ArrayList<PlaceEntity> new_places = new ArrayList<>( places.size() );
        new_places.addAll( places );
    
        int   idx      = 0, farthest_idx = 0;
        float distance = 0;
    
        for (final PlaceEntity place : places) {
            final double distance_to = reference.center().distanceTo( place.center() );
        
            if (distance_to > distance) { // we have a new max
                distance = (float) distance_to;
                farthest_idx = idx;
            }
        
            ++idx;
        }
    
        new_places.remove( farthest_idx );
        return new_places;
    }
    
    /**
     * Returns the minimum distance between a reference point and a list of {@link PlaceEntity}s.
     *
     * @param reference The reference point to measure the distance from
     * @param places    A list of {@link PlaceEntity}
     *
     * @return The maximum distance in meters.
     */
    public static float min_distance_place( LatLng reference, ArrayList<PlaceEntity> places ) {
        float distance = Constants.DISTANCE_MIN_NEAREST;
    
        for (final PlaceEntity place : places) {
            distance = (float) Math.min( distance, place.center().distanceTo( reference ) );
        }
    
        return distance;
    }
    
    /**
     * Returns a given amount list of {@link PlaceEntity}s being the nearest of a given reference.
     *
     * @param reference The location of the reference point
     * @param places    A list of {@link PlaceEntity} to compare
     * @param amount    The amount of locations to retrieve
     *
     * @return The list of the closest locations found.
     */
    public static ArrayList<PlaceEntity> nearest_place_list( @NonNull LatLng reference,
                                                             @NonNull ArrayList<PlaceEntity> places,
                                                             short amount ) {
        if (places.size() <= amount) {
            return places;
        }

//        float distance = Constants.DISTANCE_MIN_NEAREST;
        ArrayList<PlaceEntity> nearest_list = new ArrayList<>( amount );
    
        for (short i = 0; i < amount; ++i) {
            final PlaceEntity nearest_entity = nearest_place( reference, places );
            nearest_list.add( nearest_entity );
            places.remove( nearest_entity );
        }
    
        return nearest_list;
    }
    
    /**
     * Given a list of locations, returns the nearest geographical coordinates of a reference point.
     *
     * @param reference The location of the reference point
     * @param places    A list of {@code LatLng} to compare
     *
     * @return The nearest location in the list or null if places is empty.
     */
    @Nullable
    private static PlaceEntity nearest_place( LatLng reference,
                                              ArrayList<PlaceEntity> places ) {
        if (places.isEmpty()) {
            return null;
        }
        
        PlaceEntity nearest  = places.get( 0 );
        float       distance = Constants.DISTANCE_MIN_NEAREST;
        
        for (final PlaceEntity place : places) {
            final double distance_to = reference.distanceTo( place.center() );
            
            if (distance_to < distance) { // we have a new min
                distance = (float) distance_to;
                nearest = place;
            }
        }
        
        return nearest;
    }
    
    /**
     * Checks that we have actual data it the database; fills it with the (currently hard-coded)
     * {@link #JSON_BACKEND}.
     *
     * @return The root {@link Area}.
     * @throws JSONException in case of badly written JSON.
     */
    @SuppressWarnings("ConstantConditions")
    public final Area init() throws JSONException {
        final JSONObject json = ResourceUtils.read_json( context_.getResources(),
                                                         JSON_BACKEND );
        
        final Area area = new Area( json.getJSONObject( "area" ) );
        
        if (count_places() > 0) {
            return area; // JSON is already loaded.
        }
        
        try {
            final JSONArray places = json.getJSONArray( "places" );
            for (int i = 0; i < places.length(); i++) {
                insert_place( places.getJSONObject( i ) );
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return area;
    }
    
    /**
     * Returns the total amount of places.
     *
     * @return The number of places or -1 if there is no result.
     */
    private int count_places() {
        final Cursor cursor = dbs_[ 0 ].rawQuery( "SELECT COUNT(*) FROM " + PLACE_TABLE, null );
        
        if (!cursor.moveToFirst()) {
            cursor.close();
            return -1;
        }
        
        final int count = cursor.getInt( 0 );
        
        cursor.close();
        return count;
    }
    
    /**
     * Mapping function that takes care of JSON -> `place` SQLITE injection.
     *
     * @param place The {@code JSONObject} to map
     */
    private void insert_place( final JSONObject place ) {
        try {
            final JSONArray sounds   = place.getJSONArray( "sounds" );
            final String    place_id = place.getString( "id" );
            
            for (int i = 0; i < sounds.length(); i++) {
                insert_sound( place_id, sounds.getJSONObject( i ) );
            }
            
            ContentValues values = new ContentValues();
            
            values.put( PLACE_ID, place_id );
            values.put( PLACE_NAME, place.getString( "name" ) );
            values.put( PLACE_AUTHOR, place.getString( "author" ) );
            values.put( PLACE_THEME, place.getJSONObject( "theme" ).toString() );
            values.put( PLACE_SHAPE, place.getJSONObject( "shape" ).toString() );
            
            dbs_[ 1 ].insert( PLACE_TABLE, null, values );
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Mapping function that takes care of JSON -> `sound` SQLITE injection.
     *
     * @param place_id The id of the place this sound belongs to
     * @param sound    The {@code JSONObject} to map
     */
    private void insert_sound( final String place_id, final JSONObject sound ) {
        try {
            final ContentValues values = new ContentValues();
            
            values.put( SOUND_TRACK, sound.getString( "track" ) );
            values.put( SOUND_WHERE, sound.getJSONObject( "where" ).toString() );
            values.put( SOUND_PLACE, place_id );
            
            dbs_[ 1 ].insert( SOUND_TABLE, null, values );
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Closes the connection on both readable and writable databases.
     *
     * @see #dbs_
     */
    public void close() {
        dbs_[ 0 ].close();
        dbs_[ 1 ].close();
    }
    
    /**
     * Creates table structure after database creation.
     *
     * @param db The database
     */
    @Override
    public void onCreate( SQLiteDatabase db ) {
        final String query_create_places = create_table_query( PLACE_TABLE,
                                                               PLACE_COLUMNS,
                                                               PLACE_TYPES );
        db.execSQL( query_create_places );
    
        final String query_create_sounds = create_table_query( SOUND_TABLE,
                                                               SOUND_COLUMNS,
                                                               SOUND_TYPES );
        db.execSQL( query_create_sounds );
    }
    
    /**
     * Called on database version upgrade.
     *
     * @param db         The database
     * @param oldVersion The old database version
     * @param newVersion The new database version
     *
     * @see #DATABASE_VERSION
     */
    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVersion, int newVersion ) {
        drop( db );
    }
    
    /**
     * Called on database version downgrade.
     *
     * @param db         The database
     * @param oldVersion The old database version
     * @param newVersion The new database version
     *
     * @see #DATABASE_VERSION
     */
    @Override
    public void onDowngrade( SQLiteDatabase db, int oldVersion, int newVersion ) {
        drop( db );
    }
    
    /**
     * Deletes table and recreates.
     *
     * @param db A writable SQLite database
     */
    private void drop( SQLiteDatabase db ) {
        db.execSQL( "DROP TABLE IF EXISTS " + PLACE_TABLE );
        db.execSQL( "DROP TABLE IF EXISTS " + SOUND_TABLE );
    
        onCreate( db );
    }
    
    /**
     * Utility function to generate a `CREATE TABLE` query where every column is of type `TEXT`, the
     * first one being a primary key.
     *
     * @param table   The table name
     * @param columns A list of columns that describes the table
     * @param types   The corresponding sql types of columns
     *
     * @return The sql query.
     */
    private String create_table_query( final String table,
                                       final String[] columns,
                                       final String[] types ) {
        StringBuilder query = new StringBuilder( "CREATE TABLE " + table + " (" );
        
        for (int i = 0; i < columns.length; i++) {
            query.append( columns[ i ] )
                 .append( " " )
                 .append( types[ i ] )
                 .append( ", " );
        }
        
        return query.substring( 0, query.length() - 2 ) + ")";
    }
    
    /**
     * Fetches all places stored in the {@link #PLACE_TABLE} table.
     * The result of a first retrieval is cached inside {@link #places_}.
     *
     * @return An array of {@link PlaceEntity} or {@code null} if there is no result.
     */
    @Nullable
    public ArrayList<PlaceEntity> all_places() {
        if (places_ != null) {
            return places_;
        }
    
        final Cursor cursor = dbs_[ 0 ].rawQuery( "SELECT * FROM " + PLACE_TABLE, null );
    
        if (!cursor.moveToFirst()) {
            return null;
        }
    
        places_ = new ArrayList<>( cursor.getCount() );
    
        do {
            places_.add( PlaceEntity.from_cursor( cursor ) );
        } while (cursor.moveToNext());
    
        cursor.close();
        return places_;
    }
    
    /**
     * Gets all sounds belonging to a specific {@link PlaceEntity}.
     *
     * @param place_id The id of the place
     *
     * @return An array of {@link SoundEntity} or {@code null} if there is no result.
     */
    @Nullable
    public ArrayList<SoundEntity> all_sounds( final String place_id ) {
        final Cursor cursor = dbs_[ 0 ].query( SOUND_TABLE,
                                               SOUND_COLUMNS,
                                               String.format( "%s =?", SOUND_PLACE ),
                                               new String[]{ place_id },
                                               null,
                                               null,
                                               null,
                                               null );
    
        if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
    
        final ArrayList<SoundEntity> sounds = new ArrayList<>();
    
        do {
            sounds.add( SoundEntity.from_cursor( cursor ) );
        } while (cursor.moveToNext());
    
        cursor.close();
        return sounds;
    }
    
    /**
     * Returns all sounds of "inside" type.
     *
     * @param place_id The place from where we're searching inside sounds
     *
     * @return A {@link SoundEntity} or {@code null} if there is no result.
     */
    @Nullable
    public final SoundEntity inside_sound( @NonNull final String place_id ) {
        Cursor cursor = dbs_[ 0 ].query( SOUND_TABLE,
                                         SOUND_COLUMNS,
                                         String.format( "%s =? AND %s LIKE ?",
                                                        SOUND_PLACE,
                                                        SOUND_WHERE ),
                                         new String[]{ place_id, "%inside%" },
                                         null,
                                         null,
                                         null,
                                         null );
    
        if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
    
        final SoundEntity sound_entity = SoundEntity.from_cursor( cursor );
    
        cursor.close();
        return sound_entity;
    }
}
