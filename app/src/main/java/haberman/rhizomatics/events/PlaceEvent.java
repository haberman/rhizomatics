package haberman.rhizomatics.events;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import haberman.rhizomatics.fsm.entities.PlaceEntity;

/**
 * {@link IEvent} implementation for {@link haberman.rhizomatics.fsm.Place} related events.
 *
 * @author haberman
 */

public class PlaceEvent implements IEvent
{
    public static final String ENTER = "event_place_enter";
    public static final String EXIT  = "event_place_exit";
    
    /**
     * The type of event being fired.
     */
    @NonNull
    private final String event_type_;
    
    /**
     * An optional place entity to be attached with this event.
     */
    @Nullable
    private final PlaceEntity entity_;
    
    /**
     * Constructor.
     *
     * @param event_type The event type
     * @param entity     A (possibly {@code null}) place entity firing the event
     */
    public PlaceEvent( @NonNull final String event_type, @Nullable final PlaceEntity entity ) {
        event_type_ = event_type;
        entity_ = entity;
    }
    
    @NonNull
    @Override
    public final String event_type() {
        return event_type_;
    }
    
    @Nullable
    public final PlaceEntity entity() {
        return entity_;
    }
}
