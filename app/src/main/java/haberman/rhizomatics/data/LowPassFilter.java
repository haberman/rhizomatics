package haberman.rhizomatics.data;

/**
 * @author s. conversy from n. roussel c++ version
 */

class LowPassFilter
{
    
    private double  y;
    private double  a;
    private double  s;
    private boolean initialized;
    
    public LowPassFilter( double alpha ) throws Exception {
        init( alpha, 0 );
    }
    
    private void init( double alpha, double initval ) throws Exception {
        y = s = initval;
        setAlpha( alpha );
        initialized = false;
    }
    
    private void setAlpha( double alpha ) throws Exception {
        if (alpha <= 0.0 || alpha > 1.0) {
            throw new Exception( "alpha should be in (0.0, 1.0] and is now " + alpha );
        }
        a = alpha;
    }
    
    public double filterWithAlpha( double value, double alpha ) throws Exception {
        setAlpha( alpha );
        return filter( value );
    }
    
    private double filter( double value ) {
        double result;
        if (initialized) {
            result = a * value + (1.0 - a) * s;
        } else {
            result = value;
            initialized = true;
        }
        y = value;
        s = result;
        return result;
    }
    
    public boolean hasLastRawValue() {
        return initialized;
    }
    
    public double lastRawValue() {
        return y;
    }
}
