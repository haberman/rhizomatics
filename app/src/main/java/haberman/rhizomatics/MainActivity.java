package haberman.rhizomatics;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.BindingAdapter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.constants.MapboxConstants;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.OnLocationLayerClickListener;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;

import java.util.Objects;

import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.data.DataManager;
import haberman.rhizomatics.data.OneEuroFilter;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.AudioEvent;
import haberman.rhizomatics.events.GPSEvent;
import haberman.rhizomatics.events.IEvent;
import haberman.rhizomatics.events.IEventHandler;
import haberman.rhizomatics.events.MapEvent;
import haberman.rhizomatics.events.PlaceEvent;
import haberman.rhizomatics.events.UXEvent;
import haberman.rhizomatics.fsm.Application;
import haberman.rhizomatics.fsm.AudioPlayer;
import haberman.rhizomatics.fsm.GPS;
import haberman.rhizomatics.fsm.Map;
import haberman.rhizomatics.fsm.Place;
import haberman.rhizomatics.fsm.Reading;
import haberman.rhizomatics.fsm.Soundscape;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import haberman.rhizomatics.fsm.entities.SoundEntity;
import haberman.rhizomatics.ui.CreditsFragment;
import haberman.rhizomatics.ui.HeaderFragment;
import haberman.rhizomatics.ui.MapMenuFragment;
import haberman.rhizomatics.ui.toggles.ToggleCredits;
import haberman.rhizomatics.ui.toggles.ToggleLocation;
import haberman.rhizomatics.ui.toggles.ToggleMarker;
import haberman.rhizomatics.utils.GeoUtils;
import haberman.rhizomatics.utils.MathUtils;
import timber.log.Timber;

import static haberman.rhizomatics.data.SharedBuffer.LocationState.OFF;
import static haberman.rhizomatics.data.SharedBuffer.PositionState.INSIDE;
import static haberman.rhizomatics.data.SharedBuffer.PositionState.OUTSIDE;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.BUFFERING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.COMPLETED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.IDLE;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PAUSED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PLAYING;
import static haberman.rhizomatics.fsm.AudioPlayer.AudioType.READING;
import static haberman.rhizomatics.fsm.AudioPlayer.AudioType.SOUNDSCAPE;
import static haberman.rhizomatics.fsm.Map.AppState.STOPPED;
import static haberman.rhizomatics.fsm.Map.AppState.TRACKING_OFF;


public class MainActivity extends AppCompatActivity implements
                                                    ActivityCompat.OnRequestPermissionsResultCallback,
                                                    IEventHandler, LocationEngineListener,
                                                    OnLocationLayerClickListener,
                                                    SensorEventListener
{
    private enum DevMode
    {
        OFF, SIMULATE, TOUCH
    }
    
    private final DevMode dev_mode    = DevMode.OFF;
    private final Handler dev_handler = new Handler();
    
    private WindowManager window_manager;
    
    /** Views */
    private MapView        map_view;
    private ToggleMarker   toggle_marker;
    private ToggleLocation toggle_location;
    
    /** Location */
    private LocationLayerPlugin location_plugin;
    private LocationEngine      location_engine;
    
    /** Compass */
    private SensorManager sensor_manager;
    private Sensor        compass;

//    /** GL */
//    private GLSurface gl_surface;
    
    /** Fragments */
    private HeaderFragment  header_fragment;
    private CreditsFragment credits_fragment;
    private MapMenuFragment mapmenu_fragment;
    
    /** FSMs */
    private Application application;
    private Map         map;
    private GPS         gps;
    private Reading     reading;
    private Soundscape  soundscape;
    
    @Nullable
    private       LatLng   previous_location = null;
    private final Runnable touch_runnable    = new Runnable()
    {
        @Override
        public void run() {
            update_tracking( GeoUtils.location_at_screen_center() );
            dev_handler.postDelayed( touch_runnable, Constants.INTERVAL_SLOW );
        }
    };
    
    private OneEuroFilter tilt_filter;
    
    /** Dev */
    private       float    sim_progress = .0f;
    private final Runnable sim_runnable = new Runnable()
    {
        @Override
        public void run() {
            sim_progress += Constants.INTERVAL_NORMAL;
            
            if (sim_progress >= Constants.SIMULATION_DURATION) {
                sim_progress = 0.f;
            } else {
                update_tracking( Objects.requireNonNull( GeoUtils.interpolate_list( Constants.SIMULATION_LOCATIONS,
                                                                                    sim_progress /
                                                                                    (float) Constants.SIMULATION_DURATION ) ) );
                dev_handler.postDelayed( sim_runnable, Constants.INTERVAL_NORMAL );
            }
        }
    };
    private boolean restore_tracking_on_resume;
    
    /**
     * Layout height data bindable.
     *
     * @see HeaderFragment
     */
    @BindingAdapter("android:layout_height")
    public static void setLayoutHeight( View view, int layout_height ) {
        final ViewGroup.LayoutParams layout_params = view.getLayoutParams();
        layout_params.height = layout_height;
    }
    
    private void state_log() {
        Timber.i( "STATES:"
                  + "\n        -shared location state: " + SharedBuffer.location_state()
                  + "\n        -shared position state: " + SharedBuffer.position_state()
                  + "\n        -shared audio state: " + SharedBuffer.audio_state()
                  + "\n        -shared audio type: " + SharedBuffer.audio_type()
                  + "\n        -application: " + application.state()
                  + "\n        -map: " + map.state()
                  + "\n        -gps: " + gps.state()
                  + "\n        -reading: " + reading.state()
                  + "\n        -soundscape: " + soundscape.state() );
    }
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        
        if (BuildConfig.DEBUG) { Timber.plant( new Timber.DebugTree() ); }
        
        final Context        context = getApplicationContext();
        final DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getRealMetrics( metrics );
        
        SharedBuffer.init_instance( new DataManager( context ), metrics );
        
        setContentView( R.layout.main_activity );
        Mapbox.getInstance( context, getString( R.string.token_mapbox ) );
        
        map_view = findViewById( R.id.map_view );
        map_view.onCreate( savedInstanceState );
        
        toggle_marker = findViewById( R.id.marker_toggle );
        toggle_location = findViewById( R.id.location_toggle );
        
        window_manager = (WindowManager) context.getSystemService( Context.WINDOW_SERVICE );
        
        location_engine = new LocationEngineProvider( this ).obtainBestLocationEngineAvailable();
        location_engine.setPriority( LocationEnginePriority.HIGH_ACCURACY );
        location_engine.setFastestInterval( 1000 );
        
        sensor_manager = (SensorManager) getSystemService( SENSOR_SERVICE );
        compass = sensor_manager !=
                  null ? sensor_manager.getDefaultSensor( Sensor.TYPE_ROTATION_VECTOR ) : null;
        
        try {
            tilt_filter = new OneEuroFilter( 120, 5 );
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        header_fragment = (HeaderFragment) getFragmentManager().findFragmentById( R.id.header_fragment );
        credits_fragment = (CreditsFragment) getFragmentManager().findFragmentById( R.id.credits_fragment );
        mapmenu_fragment = (MapMenuFragment) getFragmentManager().findFragmentById( R.id.mapmenu_fragment );

//        gl_surface = findViewById(R.id.gl_surface);
//        gl_surface.init(new GLRenderer(new SurroundEcho(),
//                                       getResources(),
//                                       R.raw.line_shader_v, R.raw.line_shader_f));
        
        application = new Application( context, MainActivity.this, getWindow().getDecorView() );
        
        map = new Map( context, map_view );
        gps = new GPS( context, MainActivity.this );
        reading = new Reading( context );
        soundscape = new Soundscape( context );
    }
    
    @Override
    protected void onStart() {
        state_log();
        
        super.onStart();
        map_view.onStart();
        
        SharedBuffer.dispatcher().add_listener( UXEvent.PLAYER_ACTION, this );
        SharedBuffer.dispatcher().add_listener( UXEvent.MAPMENU_ACTION, this );
        
        SharedBuffer.dispatcher().add_listener( GPSEvent.START, this );
        SharedBuffer.dispatcher().add_listener( GPSEvent.SEARCH, this );
        SharedBuffer.dispatcher().add_listener( GPSEvent.TIMEOUT, this );
        
        SharedBuffer.dispatcher().add_listener( MapEvent.READY, this );
        SharedBuffer.dispatcher().add_listener( MapEvent.CLICK, this );
        SharedBuffer.dispatcher().add_listener( MapEvent.CLICK_LONG, this );
        SharedBuffer.dispatcher().add_listener( MapEvent.CLICK_MARKER, this );
        
        SharedBuffer.dispatcher().add_listener( AudioEvent.PREPARE, this );
        SharedBuffer.dispatcher().add_listener( AudioEvent.BUFFER, this );
        SharedBuffer.dispatcher().add_listener( AudioEvent.PLAY, this );
        SharedBuffer.dispatcher().add_listener( AudioEvent.COMPLETE, this );
        SharedBuffer.dispatcher().add_listener( AudioEvent.DESTROY, this );
        
        SharedBuffer.dispatcher().add_listener( PlaceEvent.ENTER, this );
        SharedBuffer.dispatcher().add_listener( PlaceEvent.EXIT, this );
    }
    
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map_view.onLowMemory();
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        map_view.onPause();
        
        application.pause();
        state_log();
    }
    
    @SuppressWarnings("ConstantConditions")
    @SuppressLint("MissingPermission")
    @Override
    protected void onResume() {
        super.onResume();
        map_view.onResume();
        
        if (SharedBuffer.audio_state().equals( PAUSED )) { update_audio( true, false ); }
        
        if (map.ready() && map.state().equals( STOPPED )) {
            map.resume();
            
            if (restore_tracking_on_resume) {
                restore_tracking_on_resume = false;
                start_tracking();
            }
            
            if (map.active_place() != null) {
                SharedBuffer.map()
                            .animateCamera( CameraUpdateFactory.newLatLngZoom( map.active_place()
                                                                                  .entity()
                                                                                  .center(),
                                                                               Constants.GPS_INITIAL_ZOOM ),
                                            Constants.DURATION_CAMERA_ANIMATION );
                
            }
        }
        
        if (application.granted()) {
            if (gps.state().equals( GPS.AppState.FAILURE )) { gps.search(); }
        }
        
        application.start();
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        map_view.onStop();
        
        if (map.tracked()) {
            stop_tracking();
            restore_tracking_on_resume = true;
        }
        
        gps.stop();
        map.stop();
        
        update_audio( false, false );
        
        SharedBuffer.dispatcher().remove_listener( UXEvent.PLAYER_ACTION, this );
        SharedBuffer.dispatcher().remove_listener( UXEvent.MAPMENU_ACTION, this );
        
        SharedBuffer.dispatcher().remove_listener( GPSEvent.START, this );
        SharedBuffer.dispatcher().remove_listener( GPSEvent.SEARCH, this );
        SharedBuffer.dispatcher().remove_listener( GPSEvent.TIMEOUT, this );
        
        SharedBuffer.dispatcher().remove_listener( MapEvent.READY, this );
        SharedBuffer.dispatcher().remove_listener( MapEvent.CLICK, this );
        SharedBuffer.dispatcher().remove_listener( MapEvent.CLICK_LONG, this );
        SharedBuffer.dispatcher().remove_listener( MapEvent.CLICK_MARKER, this );
        
        SharedBuffer.dispatcher().remove_listener( AudioEvent.PREPARE, this );
        SharedBuffer.dispatcher().remove_listener( AudioEvent.BUFFER, this );
        SharedBuffer.dispatcher().remove_listener( AudioEvent.PLAY, this );
        SharedBuffer.dispatcher().remove_listener( AudioEvent.COMPLETE, this );
        SharedBuffer.dispatcher().remove_listener( AudioEvent.DESTROY, this );
        
        SharedBuffer.dispatcher().remove_listener( PlaceEvent.ENTER, this );
        SharedBuffer.dispatcher().remove_listener( PlaceEvent.EXIT, this );
        
        application.stop();
        state_log();
    }
    
    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        super.onSaveInstanceState( outState );
        map_view.onSaveInstanceState( outState );
    }
    
    @Override
    protected void onDestroy() {
        application.destroy();
        map.destroy();
        
        super.onDestroy();
        map_view.onDestroy();
    }
    
    /**
     * Where we respond to permissions user response. Whatever happens, we fire {@link Application#run()}.
     *
     * @param requestCode  Exactly {@link haberman.rhizomatics.data.Constants#PERMISSION_REQUEST_CODE}
     * @param permissions  An array of requested permissions
     * @param grantResults An array of request responses' matching permissions' requests
     */
    @Override
    public void onRequestPermissionsResult( int requestCode,
                                            @NonNull String[] permissions,
                                            @NonNull int[] grantResults ) {
        application.run();
    }
    
    @Override
    @SuppressLint("MissingPermission")
    public void onConnected() {
        location_plugin.setCameraMode( CameraMode.TRACKING_GPS );
        location_plugin.setRenderMode( RenderMode.COMPASS );
        
        location_engine.requestLocationUpdates();
    }
    
    @SuppressWarnings("ConstantConditions")
    @Override
    public void onLocationChanged( @Nullable Location location ) {
        final LatLng lat_lng = new LatLng();
        lat_lng.setLatitude( location.getLatitude() );
        lat_lng.setLongitude( location.getLongitude() );
        
        update_tracking( lat_lng );
    }
    
    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onSensorChanged( SensorEvent se ) {
        if (previous_location == null) { return; }
        if (se.accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE) { return; }
        if (se.sensor.getType() != Sensor.TYPE_ROTATION_VECTOR) { return; }
        
        float[] rotationMatrix = new float[ 9 ];
        SensorManager.getRotationMatrixFromVector( rotationMatrix, se.values );
        
        final int worldAxisForDeviceAxisX;
        final int worldAxisForDeviceAxisY;
        
        // Remap the axes as if the device screen was the instrument panel,
        // and adjust the rotation matrix for the device orientation.
        switch (window_manager.getDefaultDisplay().getRotation()) {
            case Surface.ROTATION_90:
                worldAxisForDeviceAxisX = SensorManager.AXIS_Z;
                worldAxisForDeviceAxisY = SensorManager.AXIS_MINUS_X;
                break;
            case Surface.ROTATION_180:
                worldAxisForDeviceAxisX = SensorManager.AXIS_MINUS_X;
                worldAxisForDeviceAxisY = SensorManager.AXIS_MINUS_Z;
                break;
            case Surface.ROTATION_270:
                worldAxisForDeviceAxisX = SensorManager.AXIS_MINUS_Z;
                worldAxisForDeviceAxisY = SensorManager.AXIS_X;
                break;
            case Surface.ROTATION_0:
            default:
                worldAxisForDeviceAxisX = SensorManager.AXIS_X;
                worldAxisForDeviceAxisY = SensorManager.AXIS_Z;
                break;
        }
        
        float[] adjustedRotationMatrix = new float[ 9 ];
        SensorManager.remapCoordinateSystem( rotationMatrix, worldAxisForDeviceAxisX,
                                             worldAxisForDeviceAxisY, adjustedRotationMatrix );
        
        // Transform rotation matrix into azimuth/pitch/roll
        float[] orientation = new float[ 3 ];
        SensorManager.getOrientation( adjustedRotationMatrix, orientation );
        
        float tilt = MathUtils.map_number( orientation[ 1 ],
                                           0,
                                           (float) Math.PI * .5f,
                                           (float) MapboxConstants.MAXIMUM_TILT,
                                           (float) MapboxConstants.MINIMUM_TILT );
        
        try { tilt = (float) tilt_filter.filter( tilt ); } catch (Exception e) {
            e.printStackTrace();
        }
        
        final float zoom = MathUtils.map_number( tilt,
                                                 (float) MapboxConstants.MINIMUM_TILT,
                                                 (float) MapboxConstants.MAXIMUM_TILT,
                                                 Constants.MAP_TILT_MIN,
                                                 Constants.MAP_TILT_MAX );
        CameraPosition.Builder cam_builder = new CameraPosition.Builder( SharedBuffer.map()
                                                                                     .getCameraPosition() );
        cam_builder.tilt( tilt );
        cam_builder.zoom( zoom );
        SharedBuffer.map()
                    .moveCamera( CameraUpdateFactory.newCameraPosition( cam_builder.build() ) );
    }
    
    @Override
    public void onAccuracyChanged( Sensor sensor, int i ) { /* pass */ }
    
    @Override
    public void onLocationLayerClick() { /* pass */ }
    
    /**
     * Starts tracking.
     */
    private void start_tracking() {
        previous_location = null;
        
        switch (dev_mode) {
            case OFF:
                location_engine.addLocationEngineListener( this );
                location_engine.activate();
                
                location_plugin.addOnLocationClickListener( this );
                location_plugin.setLocationLayerEnabled( true );
                break;
            case SIMULATE:
                dev_handler.postDelayed( sim_runnable, Constants.INTERVAL_NORMAL );
                break;
            case TOUCH:
                dev_handler.postDelayed( touch_runnable, Constants.INTERVAL_NORMAL );
                break;
        }
    }
    
    private void start_gyro() {
        location_plugin.setCameraMode( CameraMode.TRACKING_COMPASS );
        sensor_manager.registerListener( this, compass, Constants.INTERVAL_FAST );
    }
    
    /**
     * Callback function invoked when a new location is found.
     *
     * @param lat_lng The new {@code LatLng} location.
     */
    @SuppressLint("MissingPermission")
    private void update_tracking( final LatLng lat_lng ) {
        SharedBuffer.set_location( lat_lng );
        map.track( true );
        
        CameraPosition.Builder cam_builder = new CameraPosition.Builder().target( lat_lng );
        
        // first entrance into track mode
        if (previous_location == null) {
            // on first entrance, we'll zoom-in if we're too far.
            if (SharedBuffer.map().getCameraPosition().zoom < 16) { cam_builder.zoom( 16 ); }
        }
        
        // There's no sound to be played and we're not inside -> awaits soundscape
        if (audio_free() && !SharedBuffer.position_state().equals( INSIDE )) {
            header_fragment.hide();
            soundscape.await();
        }
        
        final CameraUpdate cam_update = CameraUpdateFactory.newCameraPosition( cam_builder.build() );
        SharedBuffer.map().animateCamera( cam_update, Constants.DURATION_CAMERA_ANIMATION, null );
        
        previous_location = lat_lng;
    }
    
    @SuppressLint("MissingPermission")
    private void stop_tracking() {
        sensor_manager.unregisterListener( this, compass );
        previous_location = null;
        
        switch (dev_mode) {
            case OFF:
                location_engine.removeLocationEngineListener( this );
                location_engine.removeLocationUpdates();
                location_engine.deactivate();
                
                location_plugin.removeOnLocationClickListener( this );
                location_plugin.setLocationLayerEnabled( false );
                
                sensor_manager.registerListener( null, compass, Constants.INTERVAL_FAST );
                break;
            case SIMULATE:
                dev_handler.removeCallbacks( sim_runnable );
                break;
            case TOUCH:
                dev_handler.removeCallbacks( touch_runnable );
                break;
            
        }
    }
    
    /**
     * Changes the active audio state.
     *
     * @param toggle  {@code true} to play_, {@code false} to pause
     * @param destroy {@link AudioPlayer#destroy()} trigger that takes precedence over toggle
     */
    private void update_audio( final boolean toggle, final boolean destroy ) {
        final AudioPlayer.AudioType audio_type = SharedBuffer.audio_type();
        assert audio_type != null;
        
        switch (audio_type) {
            case READING:
                if (destroy) { reading.destroy(); } else if (toggle) {
                    reading.play( null );
                } else { reading.pause(); }
                break;
            case SOUNDSCAPE:
                if (destroy) { soundscape.destroy(); } else if (toggle) {
                    soundscape.play_();
                } else { soundscape.pause(); }
                break;
        }
        
        if (destroy) {
            header_fragment.hide();
            return;
        }
        
        if (toggle) {
            if (audio_type.equals( READING )) { header_fragment.ui_pause(); } else {
                header_fragment.ui_next();
            }
        } else { header_fragment.ui_play(); }
        
        if (map.active_place() != null) {
            if (map.marked() && audio_type.equals( READING )) {
                header_fragment.show_reading( Objects.requireNonNull( map.active_place() )
                                                     .entity() );
            } else if (audio_type.equals( SOUNDSCAPE )) {
                header_fragment.show();
            }
        }
    }
    
    /**
     * Helper that indicates if an audio player is busy.
     *
     * @return {@code True} if both {@link #reading} and {@link #soundscape} are {@code IDLE}, {@code false} otherwise.
     */
    public boolean audio_free() {
        return (reading.state().equals( IDLE ) && soundscape.state().equals( IDLE ));
    }
    
    /**
     * IEventHandler implementation that switch / case over every events shared between different FSM.
     */
    @SuppressWarnings("ConstantConditions")
    @SuppressLint("MissingPermission")
    @Override
    public void on_event( IEvent e ) {
        Timber.i( "on_event: %s ", e.event_type() );
        
        final AudioPlayer.AppState  audio_state = SharedBuffer.audio_state();
        final AudioPlayer.AudioType audio_type  = SharedBuffer.audio_type();
        
        switch (e.event_type()) {
            case AudioEvent.PREPARE:
            case AudioEvent.BUFFER:
                if (audio_type.equals( SOUNDSCAPE )) {
                    header_fragment.show_soundscape();
                    reading.destroy();
                } else if (audio_type.equals( READING )) {
                    soundscape.destroy();
                    if (!header_fragment.visible()) { header_fragment.show(); }
                }
                
                header_fragment.ui_busy( true );
                break;
            case AudioEvent.PLAY:
                header_fragment.ui_busy( false );
                final AudioEvent ae = (AudioEvent) e;
                switch (ae.audio_type()) {
                    case READING:
                        if (!map.marked() && SharedBuffer.location_state().equals( OFF )) {
                            map.mark( true );
                            toggle_marker.set_state( ToggleMarker.MARKER_ON, false );
                        }
//                        map.active_place().marker_select( true );
                        header_fragment.ui_pause();
                        header_fragment.show_reading( map.active_place().entity() );
                        break;
                    case SOUNDSCAPE:
                        header_fragment.show_soundscape();
                        // consumes the current active place
                        if (map.active_place() != null) {
                            assert map.active_place().marker() != null;
                            map.active_place().marker_select( false );
                            map.set_active_place( null );
                        }
                        break;
                }
                break;
            case AudioEvent.COMPLETE:
                switch (((AudioEvent) e).audio_type()) {
                    case READING:
                        header_fragment.ui_replay();
                        if (map.active_place() != null &&
                            SharedBuffer.position_state().equals( INSIDE )) {
                            map.active_place().in();
                        }
                        if (!map.tracked() || SharedBuffer.position_state().equals( INSIDE )) {
                            return;
                        }
                        map.track( true );
                        if (SharedBuffer.position_state().equals( OUTSIDE )) { soundscape.await();}
                        break;
                    case SOUNDSCAPE:
                        if (!reading.state().equals( PLAYING ) ||
                            !reading.state().equals( BUFFERING )) { soundscape.next(); }
                        break;
                }
                break;
            case AudioEvent.DESTROY:
                state_log();
                if (reading.has_queue()) {
                    reading.play( null );
                } else if (audio_free() && header_fragment.visible()) {
                    header_fragment.hide();
                }
                break;
            
            case GPSEvent.SEARCH:
                header_fragment.show( R.string.hold, R.string.gps_search );
                header_fragment.ui_busy( true );
                break;
            case GPSEvent.START:
                // This may happen very quickly (eg. during the `show` animation), so we force the disappearance of HUD.
                header_fragment.hide();
                toggle_marker.set_state( ToggleMarker.MARKER_OFF, true );
                start_tracking();
                break;
            case GPSEvent.TIMEOUT:
                header_fragment.error( R.string.gps_timeout );
                toggle_location.set_state( ToggleLocation.LOCATION_OFF, false );
                SharedBuffer.set_location_state( OFF );
                stop_tracking();
                break;
            
            case MapEvent.READY:
                map.mark( true );
                location_plugin = new LocationLayerPlugin( map_view,
                                                           SharedBuffer.map(),
                                                           location_engine );
                location_plugin.applyStyle( R.style.LocationLayer );
                getLifecycle().addObserver( location_plugin );
                
                break;
            case MapEvent.CLICK:
                if (map.active_place() == null && !audio_state.equals( PLAYING )) {
                    header_fragment.hide();
                }
//                if (!SharedBuffer.location_state().equals( SharedBuffer.LocationState.OFF )) {
//                    // TODO -> trigger GL rendered echo animation...
//                }
                break;
            case MapEvent.CLICK_MARKER:
                if (!audio_state.equals( IDLE )) { reading.destroy(); }
                header_fragment.show_reading( ((MapEvent) e).place_entity() );
                break;
            
            case PlaceEvent.ENTER:
                final PlaceEntity place_entity = ((PlaceEvent) e).entity();
                final SoundEntity sound_entity = SharedBuffer.manager()
                                                             .inside_sound( place_entity.id() );
                
                if (audio_state.equals( PLAYING )) {
                    if (audio_type.equals( SOUNDSCAPE )) {
                        header_fragment.queue_reading( place_entity );
                        soundscape.destroy();
                        reading.play( sound_entity );
                    } else if (reading.sound_entity().id() != sound_entity.id()) {
                        header_fragment.queue_reading( place_entity );
                        reading.destroy();
                        reading.queue( sound_entity );
                    }
                } else {
                    header_fragment.show_reading( place_entity );
                    header_fragment.ui_pause();
                    header_fragment.show();
                    reading.play( sound_entity );
                }
                
                break;
            case PlaceEvent.EXIT:
                if (!reading.state().equals( PLAYING )) { reading.destroy(); }
                break;
            
            case UXEvent.PLAYER_ACTION:
                switch (((UXEvent) e).action()) {
                    case "PLAY":
                        final PlaceEntity place = header_fragment.place_entity();
                        final SoundEntity sound = SharedBuffer.manager().inside_sound( place.id() );
                        
                        if (SharedBuffer.location_state().equals( TRACKING_OFF )) {
                            map.mark( true );
                        }
                        
                        reading.play( sound );
                        
                        SharedBuffer.map()
                                    .animateCamera( CameraUpdateFactory.newLatLngZoom( place.center(),
                                                                                       17 ),
                                                    Constants.DURATION_CAMERA_ANIMATION );
                        break;
                    case "REPLAY":
                        final Place active_place = map.active_place();
                        if (active_place.marker() == null) { active_place.mark( true ); }
                        
                        reading.play( null );
                        break;
                    case "PAUSE":
                        reading.pause();
                        if (SharedBuffer.position_state().equals( OUTSIDE )) { soundscape.await(); }
                        break;
                    case "NEXT":
                        soundscape.next();
                        break;
                }
                break;
            case UXEvent.MAPMENU_ACTION:
                boolean clear_audio = false;
                boolean location_action = true;
                
                switch (((UXEvent) e).action()) {
                    case ToggleLocation.LOCATION_OFF:
                        stop_tracking();
                        gps.stop();
                        map.stop();
                        map.mark( true );
                        soundscape.destroy();
                        toggle_marker.set_state( ToggleMarker.MARKER_ON, false );
                        clear_audio = audio_state.equals( PAUSED ) ||
                                      audio_state.equals( COMPLETED );
                        break;
                    case ToggleLocation.LOCATION_ON:
                        gps.search();
                        break;
                    case ToggleLocation.LOCATION_COMPASS:
                        start_gyro();
                        break;
                    
                    case ToggleMarker.MARKER_OFF:
                        location_action = false;
                        map.mark( false );
                        break;
                    case ToggleMarker.MARKER_ON:
                        location_action = false;
                        map.mark( true );
                        if (audio_free()) { update_audio( audio_state.equals( PLAYING ), false ); }
                        break;
                    
                    case ToggleCredits.CREDITS_OFF:
                        credits_fragment.hide();
                        mapmenu_fragment.credits_mode( false );
                        if (map.active_place() != null && !header_fragment.visible()) {
                            header_fragment.show();
                        }
                        break;
                    case ToggleCredits.CREDITS_ON:
                        credits_fragment.show();
                        mapmenu_fragment.credits_mode( true );
                        if (header_fragment.visible()) { header_fragment.hide(); }
                        break;
                }
                
                if (clear_audio && location_action) { update_audio( false, true ); }
                break;
            
        }
    }
}
