package haberman.rhizomatics.events;

/**
 * Interface for a minimalistic event handler implementation.
 *
 * @author haberman
 */
public interface IEventHandler
{
    void on_event( IEvent e );
}