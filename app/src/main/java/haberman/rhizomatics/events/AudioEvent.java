package haberman.rhizomatics.events;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.fsm.AudioPlayer;

/**
 * {@link IEvent} implementation for audio related events.
 *
 * @author haberman
 */

public class AudioEvent implements IEvent
{
    public static final String PREPARE  = "event_audio_prepare";
    public static final String BUFFER   = "event_audio_buffer";
    public static final String PLAY     = "event_audio_play";
    public static final String PAUSE    = "event_audio_pause";
    public static final String COMPLETE = "event_audio_complete";
    public static final String DESTROY  = "event_audio_destroy";
    
    /** The type of event being fired. */
    @NonNull
    private final String event_type_;
    
    /** Optional type of audio involved with the event. */
    @Nullable
    private final AudioPlayer.AudioType audio_type_;
    
    /**
     * Constructor.
     * Will also update {@link SharedBuffer#audio_type_} if given.
     *
     * @param event_type The event type
     * @param audio_type An eventual {@link AudioPlayer.AudioType}
     */
    public AudioEvent( @NonNull final String event_type,
                       @Nullable final AudioPlayer.AudioType audio_type ) {
        event_type_ = event_type;
        audio_type_ = audio_type;
    
        if (audio_type_ != null) {
            SharedBuffer.set_audio_type( audio_type_ );
        }
    }
    
    @NonNull
    @Override
    public final String event_type() {
        return event_type_;
    }
    
    @Nullable
    public final AudioPlayer.AudioType audio_type() {
        return audio_type_;
    }
    
}
