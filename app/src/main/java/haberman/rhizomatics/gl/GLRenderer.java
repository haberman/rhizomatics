package haberman.rhizomatics.gl;

import android.content.res.Resources;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import haberman.rhizomatics.gl.drawers.IDrawer;

/**
 * Created by haberman on 3/9/18.
 */

public class GLRenderer extends GLProgram implements GLSurfaceView.Renderer
{
    
    @Nullable
    private IDrawer drawer_ = null;
    
    public GLRenderer( @Nullable IDrawer drawer ) {
        super();
        drawer_ = drawer;
    }
    
    public GLRenderer( @Nullable IDrawer drawer,
                       @NonNull String vertex_shader,
                       @NonNull String fragment_shader ) {
        super( vertex_shader, fragment_shader );
        drawer_ = drawer;
    }
    
    public GLRenderer( @Nullable IDrawer drawer,
                       @NonNull Resources res,
                       int vertex_shader,
                       int fragment_shader ) {
        super( res, vertex_shader, fragment_shader );
        drawer_ = drawer;
    }
    
    @Override
    public void onSurfaceCreated( GL10 gl10, EGLConfig eglConfig ) {
        GLES20.glEnable( GLES20.GL_BLEND );
        GLES20.glBlendFunc( GLES20.GL_SRC_ALPHA, GLES20.GL_ONE_MINUS_SRC_ALPHA );
        setup();
    }
    
    @Override
    public void onSurfaceChanged( GL10 unused, int width, int height ) {
        GLES20.glViewport( 0, 0, width, height );
    }
    
    @Override
    public void onDrawFrame( GL10 gl10 ) {
        if (drawer_ == null) {
            return;
        }
        
        GLES20.glClear( GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT );
        GLES20.glClearColor( .0f, .0f, .0f, .0f );
        
        drawer_.draw();
    }
    
    public IDrawer drawer() { return drawer_; }
}
