package haberman.rhizomatics.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RawRes;
import android.support.v4.content.ContextCompat;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ResourceUtils
{
    
    /**
     * Returns the mapped {@code JSONObject} of a given {@code RawRes} id whose content is json encoded.
     *
     * @param res    A {@code Resources} instance
     * @param res_id A {@code RawRes} id of the corresponding json file to read
     *
     * @return The json file as {@code JSONObject} interface.
     */
    @Nullable
    public static JSONObject read_json( @NonNull final Resources res, @RawRes final int res_id ) {
        final String str = read_text( res, res_id );
        
        try {
            return new JSONObject( str );
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    /**
     * Returns the raw string content of a given {@code RawRes} id.
     *
     * @param res    A {@code Resources} instance
     * @param res_id A {@code RawRes} id of the corresponding json file to read
     *
     * @return The content of resource or null in case of an {@code RawRes}.
     */
    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Nullable
    public static String read_text( @NonNull final Resources res, @RawRes final int res_id ) {
        final InputStream is = res.openRawResource( res_id );
    
        try {
            final int    size   = is.available();
            final byte[] buffer = new byte[ size ];
        
            is.read( buffer );
            is.close();
        
            return new String( buffer, "UTF-8" );
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    
    /**
     * Interprets a given resource id as text and splits it into an array of paragraphs.
     *
     * @param res         A {@code Resources} instance
     * @param resource_id A {@code RawRes} id for the text file
     *
     * @return List of {@code String} or {@code null} if a problem happened.
     */
    @Nullable
    public static ArrayList<String> text_to_paragraphs( @NonNull final Resources res,
                                                        @RawRes int resource_id ) {
        InputStream is;
    
        try {
            is = res.openRawResource( resource_id );
        } catch (Resources.NotFoundException e) {
            return null;
        }
    
        if (is == null) {
            return null;
        }
    
        final InputStreamReader isr = new InputStreamReader( is );
        final BufferedReader    br  = new BufferedReader( isr );
    
        final ArrayList<String> paragraphs = new ArrayList<>();
    
        String        line;
        StringBuilder text = new StringBuilder();
        try {
            while ((line = br.readLine()) != null) {
                if (line.isEmpty() && text.length() > 0) {
                    // removes trailing /n
                    text.delete( text.length() - 1, text.length() );
                    paragraphs.add( text.toString() );
                    text = new StringBuilder();
                } else {
                    text.append( line ).append( "\n" );
                }
            }
            // adds last paragraph
            text.delete( text.length() - 1, text.length() );
            paragraphs.add( text.toString() );
        } catch (IOException e) {
            return null;
        }
    
        return paragraphs;
    }
    
    /** Shortcuts that check if a given parmission has been granted by the end user. */
    public static boolean granted( final Context context, final String permission ) {
        return ContextCompat.checkSelfPermission( context,
                                                  permission ) == PackageManager.PERMISSION_GRANTED;
    }
}
