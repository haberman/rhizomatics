package haberman.rhizomatics.ui.anim;

import android.support.annotation.Nullable;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;

import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.utils.MathUtils;

/**
 * 2 states animator that will take a view in or out on a given side of the screen.
 *
 * @author haberman
 */

public class SlideAnimation extends Animation
{
    
    /** Reference to the view that receives transformations. */
    private final View view_;
    
    /** Side along which the animation occurs. */
    private final Side side_;
    
    /** Resize animation boundary by state. */
    private final float hidden_;
    
    /** Whether this view is visible or not. */
    private final float visible_;
    
    /** Current state of the animation: either {@link State#VISIBLE} or {@link State#HIDDEN}. */
    private State state_;
    
    /** Start / end values of the animation, regardless of the state. */
    private float from_;
    private float to_;
    
    /** Whether the view is being transformed by the animation or not. */
    private boolean animating_ = false;
    
    /**
     * Constructor.
     * A slide animation is given by a inset amount of pixels to slide the view in, along a given {@link Side}.
     *
     * @param view  The view on which animation occurs
     * @param inset The amount of pixels slide in.
     * @param side  The side from which the {@link #view_} comes in
     */
    public SlideAnimation( final View view, float inset, @Nullable final Side side ) {
        view_ = view;
        side_ = side != null ? side : Side.TOP;
        state_ = State.HIDDEN;
    
        switch (side_) {
            case BOTTOM:
                hidden_ = SharedBuffer.screen_height();
                visible_ = inset;
                break;
            case RIGHT:
                hidden_ = SharedBuffer.screen_width();
                visible_ = hidden_ - inset;
                break;
            default:
                hidden_ = -inset;
                visible_ = 0;
                break;
        
        }
    
        setInterpolator( new AccelerateDecelerateInterpolator() );
        setDuration( view.getResources().getInteger( android.R.integer.config_shortAnimTime ) );
    }
    
    /**
     * Method called during animation progress.
     *
     * @param interpolated_time The current 0/1 progress value
     * @param transformation    A {@code Transformation} instance
     */
    @Override
    protected void applyTransformation( float interpolated_time, Transformation transformation ) {
        float transform = (to_ - from_) * interpolated_time + from_;
        float alpha     = MathUtils.map_number( transform, hidden_, visible_, 0f, 1f );
        
        view_.setAlpha( alpha );
        if (interpolated_time == 1 && animating_) {
            animating_ = false;
            state_ = state_.equals( State.VISIBLE ) ? State.HIDDEN : State.VISIBLE;
        }
    
        switch (side_) {
            case TOP:
            case BOTTOM:
                view_.setY( transform );
                break;
            case LEFT:
            case RIGHT:
                view_.setX( transform );
                break;
        
        }
    
        view_.requestLayout();
    }
    
    /** Animates the view to {@link State#VISIBLE}. */
    public boolean show() {
        if (state_.equals( State.VISIBLE ) || animating_) { return false; }
    
        from_ = hidden_;
        to_ = visible_;
        animating_ = true;
    
        view_.startAnimation( this );
        return true;
    }
    
    /** Animates the view to {@link State#HIDDEN}. */
    public boolean hide() {
        if (state_.equals( State.HIDDEN ) || animating_) { return false; }
    
        from_ = visible_;
        to_ = hidden_;
        animating_ = true;
    
        view_.startAnimation( this );
    
        return true;
    }
    
    public State state() {
        return state_;
    }
    
    public float hidden() { return hidden_; }
    
    /** Enum the four possible sides of transformation. */
    public enum Side
    {
        TOP, LEFT, BOTTOM, RIGHT
    }
    
    /** Enum the possible animations' states. */
    public enum State
    {
        VISIBLE, HIDDEN
    }
}
