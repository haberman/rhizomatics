package haberman.rhizomatics.data;

import android.graphics.PointF;

/**
 * Encapsulates a 2D vector. Allows chaining methods by returning a reference to itself.
 * Also contains static methods for computing vector math.
 *
 * @author haberman
 */
public class Vector2 implements Cloneable
{
    public double x;
    public double y;
    
    public Vector2( double x, double y ) {
        this.x = x;
        this.y = y;
    }
    
    // STATIC METHODS
    public static Vector2 diff( Vector2 v1, Vector2 v2 ) {
        return new Vector2( v1.x - v2.x, v1.y - v2.y );
    }
    
    public static Vector2 from_pointf( PointF point ) {
        return new Vector2( point.x, point.y );
    }
    
    public static double angle( Vector2 origin, Vector2 target ) {
        
        return Math.atan2( target.x - origin.x, origin.y - target.y );
    }
    
    public void set( Vector2 v ) {
        x = v.x;
        y = v.y;
    }
    
    public Vector2 add( Vector2 v ) {
        x += v.x;
        y += v.y;
        
        return this;
    }
    
    public Vector2 sub( Vector2 v ) {
        x -= v.x;
        y -= v.y;
        
        return this;
    }
    
    public Vector2 mult( Vector2 v ) {
        x *= v.x;
        y *= v.y;
        
        return this;
    }
    
    public Vector2 mult_scalar( float m ) {
        x *= m;
        y *= m;
        
        return this;
    }
    
    public Vector2 div( Vector2 v ) {
        x /= v.x;
        y /= v.y;
        
        return this;
    }
    
    public Vector2 div_scalar( float m ) {
        x /= m;
        y /= m;
        
        return this;
    }
    
    public Vector2 norm() {
        final double length = length();
        
        if (length != 0) {
            x /= length;
            y /= length;
        }
        
        return this;
    }
    
    private double length() {
        return Math.sqrt( x * x + y * y );
    }
    
    public float angle_to( Vector2 v ) {
        return (float) Math.atan2( cross_product( v ), dot_product( v ) );
    }
    
    // NON-CHAINABLE
    private double cross_product( Vector2 v ) {
        return x * v.y - y * v.x;
    }
    
    private double dot_product( Vector2 v ) {
        return x * v.x + y * v.y;
    }
    
    @Override
    public Vector2 clone() throws CloneNotSupportedException {
        super.clone();
        return new Vector2( x, y );
    }
    
    @Override
    public String toString() {
        return "x: " + x + " - y: " + y;
    }
    
}
