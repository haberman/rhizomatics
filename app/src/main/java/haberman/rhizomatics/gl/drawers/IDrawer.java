package haberman.rhizomatics.gl.drawers;

import android.os.Bundle;

import javax.microedition.khronos.opengles.GL10;

/**
 * Base drawer interface used {@link haberman.rhizomatics.gl.GLRenderer#onDrawFrame(GL10)} to handle
 * the actual drawing logic.
 *
 * @author haberman
 */

public interface IDrawer
{
    /**
     * Where internals should be updated, typically before a new rendering frame.
     */
    void update( final Bundle args );
    
    /**
     * Where the rendering code should be written.
     */
    void draw();
}
