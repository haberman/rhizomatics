package haberman.rhizomatics.data;

import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;

import haberman.rhizomatics.utils.MathUtils;

/**
 * Given an array of colors, this class mostly exposes a single method {@link #get_color_at(float)}
 * that interpolates a new {@code Color} at the given position. This utility class supports alpha gradient.
 *
 * @author haberman
 */

public class Gradient
{
    private final ColorPoint[] colors_;
    
    /**
     * Constructor.
     *
     * @param colors An initial array of colors to compute the gradient. Will create a default blue
     *               -> green -> red gradient of 5 steps if {@code null}.
     */
    public Gradient( @Nullable int[] colors ) {
        if (colors == null) {
            colors_ = new ColorPoint[ 5 ];
    
            colors_[ 0 ] = new ColorPoint( Color.BLUE, 0 );
            colors_[ 1 ] = new ColorPoint( Color.CYAN, .25f );
            colors_[ 2 ] = new ColorPoint( Color.GREEN, .5f );
            colors_[ 3 ] = new ColorPoint( Color.YELLOW, .75f );
            colors_[ 4 ] = new ColorPoint( Color.RED, 1f );
        } else {
            final int s = colors.length;
    
            colors_ = new ColorPoint[ s ];
    
            for (int i = 0; i < s; i++) {
                colors_[ i ] = new ColorPoint( colors[ i ],
                                               MathUtils.map_number( i, 0, s - 1, 0, 1f ) );
            }
        }
    }
    
    /**
     * Returns a new {@code ColorInt} at the given gradient's position.
     *
     * @param position The position where to compute the new color
     *
     * @return The corresponding {@code ColorInt} value.
     */
    @ColorInt
    public int get_color_at( float position ) {
        if (colors_.length == 0) {
            return Color.WHITE;
        }
    
        final int a, r, g, b;
    
        for (int i = 0; i < colors_.length; i++) {
            final ColorPoint current_color = colors_[ i ];
        
            if (position < current_color.position) {
                final ColorPoint previous_color = colors_[ Math.max( 0, i - 1 ) ];
            
                final float diff = previous_color.position - current_color.position;
                final float frac = (diff == 0) ? 0 : (position - current_color.position) / diff;
            
                a = Math.round(
                        (previous_color.a() - current_color.a()) * frac + current_color.a() );
                r = Math.round(
                        (previous_color.r() - current_color.r()) * frac + current_color.r() );
                g = Math.round(
                        (previous_color.g() - current_color.g()) * frac + current_color.g() );
                b = Math.round(
                        (previous_color.b() - current_color.b()) * frac + current_color.b() );
            
                return Color.argb( a, r, g, b );
            }
        }
    
        final int k = colors_.length - 1;
    
        a = colors_[ k ].a();
        r = colors_[ k ].r();
        g = colors_[ k ].g();
        b = colors_[ k ].b();
    
        return Color.argb( a, r, g, b );
    }
    
    /** Internal class used to store a color at a given position in the gradient interpolation. */
    private final class ColorPoint
    {
        /** The color. */
        @ColorInt
        final int color;
    
        /** The position inside the gradient (0/1). */
        final float position;
    
        ColorPoint( @ColorInt int c, float p ) {
            color = c;
            position = p;
        }
    
        /** Alpha component. */
        @ColorInt
        int a() {
            return Color.alpha( color );
        }
    
        /** Red component. */
        @ColorInt
        int r() {
            return Color.red( color );
        }
    
        /** Green component. */
        @ColorInt
        int g() {
            return Color.green( color );
        }
    
        /** Blue component. */
        @ColorInt
        int b() {
            return Color.blue( color );
        }
    }
}
