Le gazon a remplacé les herbes folles
Le chant de la source celui des grues
Les froides barrières se sont ouvertes sur des sentiers jolis
Une auberge espagnole s’improvise les midis ensoleillés
Même Tintin a choisi d’y poser sa fusée…
Dans cet écrin de verdure où il fait bon flâner,
Promeneur, ferme les yeux, dévie le flot de tes pensées
Tu entendras alors murmurer une toute petite voix
Celle des blés et du colza des fermes d’autrefois.
