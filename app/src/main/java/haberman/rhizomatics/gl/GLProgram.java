package haberman.rhizomatics.gl;

import android.content.res.Resources;
import android.opengl.GLES20;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;

import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import haberman.rhizomatics.utils.ResourceUtils;
import timber.log.Timber;

/**
 * Created by haberman on 3/13/18.
 */

class GLProgram
{
    private final String fragment_src_;
    private final String vertex_src_;
    private       int    vertex_handle_;
    private       int    fragment_handle_;
    private int program_handle_ = -1;
    private FloatBuffer vertex_buffer_;
    private ShortBuffer drawlist_buffer;
    
    GLProgram() {
        fragment_src_ = null;
        vertex_src_ = null;
    }
    
    GLProgram( @NonNull final String vertex_shader, @NonNull final String fragment_shader ) {
        vertex_src_ = vertex_shader;
        fragment_src_ = fragment_shader;
    }
    
    GLProgram( @NonNull final Resources res,
               @RawRes final int vertex_shader,
               @RawRes final int fragment_shader ) {
        vertex_src_ = ResourceUtils.read_text( res, vertex_shader );
        fragment_src_ = ResourceUtils.read_text( res, fragment_shader );
    }
    
    void setup() {
        tear_down();
        
        if (vertex_src_.isEmpty() || fragment_src_.isEmpty()) {
            return;
        }
        
        // Lets load and compile our shaders, link the program
        // and tell OpenGL ES to use it for future drawing.
        vertex_handle_ = load_shader_( GLES20.GL_VERTEX_SHADER, vertex_src_ );
        fragment_handle_ = load_shader_( GLES20.GL_FRAGMENT_SHADER, fragment_src_ );
        program_handle_ = create_program_( vertex_handle_, program_handle_ );
    }
    
    private void tear_down() {
        if (program_handle_ != -1) {
            GLES20.glDeleteProgram( program_handle_ );
            GLES20.glDeleteShader( vertex_handle_ );
            GLES20.glDeleteShader( fragment_handle_ );
        }
    }
    
    private int load_shader_( int shader_type, String shader_source ) {
        final int handle = GLES20.glCreateShader( shader_type );
        
        if (handle == GLES20.GL_FALSE) {
            throw new RuntimeException( "Error creating shader!" );
        }
        
        // set and compile the shader
        GLES20.glShaderSource( handle, shader_source );
        GLES20.glCompileShader( handle );
        
        // check if the compilation was OK
        int[] compile_status = new int[ 1 ];
        GLES20.glGetShaderiv( handle, GLES20.GL_COMPILE_STATUS, compile_status, 0 );
        
        if (compile_status[ 0 ] == 0) {
            String error = GLES20.glGetShaderInfoLog( handle );
            GLES20.glDeleteShader( handle );
            throw new RuntimeException( "Error compiling shader: " + error );
        } else {
            Timber.i( "%s shader compiled",
                      (shader_type == GLES20.GL_FRAGMENT_SHADER ? "Fragment" : " Vertex") );
            return handle;
        }
    }
    
    private int create_program_( int vertex_shader, int fragment_shader ) {
        final int handle = GLES20.glCreateProgram();
        
        if (handle == GLES20.GL_FALSE) {
            throw new RuntimeException( "Error creating program!" );
        }
        
        // attach the shaders and link the program
        GLES20.glAttachShader( handle, fragment_shader );
        GLES20.glAttachShader( handle, vertex_shader );
        GLES20.glLinkProgram( handle );
        
        // check if the link was successful
        int[] link_status = new int[ 1 ];
        GLES20.glGetProgramiv( handle, GLES20.GL_LINK_STATUS, link_status, 0 );
        
        if (link_status[ 0 ] == 0) {
            String error = GLES20.glGetProgramInfoLog( handle );
            GLES20.glDeleteProgram( handle );
            throw new RuntimeException( "Error in program linking: " + error );
        } else {
            Timber.i( "we have a program" );
            return handle;
        }
    }
}
