Les foulées d’une passante scandent son retour au foyer. Le silence vespéral, de l’autre côté du pont, fait accélérer leur cadence. Les enjambées s’allongent, les yeux sondent les ténèbres de la scavée solitaire.
 
Ascension précipitée, essoufflement, peur d’une mauvaise rencontre…

Enfin délivrées de l’obscurité forestière, les jambes de la marcheuse s’élancent sur les pavés. Ses deux chevilles crispées la portent hors de ces lieux gorgés d’effroi. La passante disparait, et soudainement s’assourdit le tempo de son angoisse.

Dans le chemin creux, le bruissement des feuilles réveille tendrement l’ouïe d’une passante matinale. Les rires des jeunes collégiens rebondissent sous la voûte du tunnel végétal. L’aurore verdoyante coule le long de la scavée.

Chaque jour, à l’aube, la marcheuse ensommeillée savoure sa traversée, qui prolonge en un songe diurne la féérie d’un rêve trop vite abandonné sur l’oreiller. En bas du chemin s’interrompent ces moments de douceur discrètement dérobés à la nuit. La nature devient timide, sa mélodie fait place aux rumeurs citadines. Un fugace instant a suffi, la ville jaillit.
