package haberman.rhizomatics.data;

import com.mapbox.mapboxsdk.geometry.LatLng;

import haberman.rhizomatics.fsm.Soundscape;

/**
 * A class storing application wide constants.
 *
 * @author haberman
 */

public class Constants
{
    /** Used to track the {@code ACCESS_FINE_LOCATION} and {@code RECORD_AUDIO} permission requests. */
    public final static short PERMISSION_REQUEST_CODE = 1;
    
    /** Min / Max tilt when using sensor. */
    public final static float MAP_TILT_MIN = 15f;
    public final static float MAP_TILT_MAX = 20f;
    
    /** Initial volume gain for every sounds. */
    public final static float VOLUME_GAIN = .7f;
    
    /** How much rings should be drawn for {@link haberman.rhizomatics.fsm.Place}s. */
    public final static short RINGS_AMOUNT = 10;
    
    /** How opaque is the 3D buildings' layer? */
    public final static float OPACITY_BUILDING = .25f;
    
    /** Amount of tracks played in the {@link Soundscape}. */
    public final static short AREA_TRACKS_AMOUNT = 12;
    
    /** Used to track a GPS activation task. */
    public final static short GPS_RESOLVE_CODE = 2;
    /** An initial zoom passed along the focus point of an intial user location retrieval. */
    public final static short GPS_INITIAL_ZOOM = 17;
    
    /** The minimum distance (from user) for a {@link haberman.rhizomatics.fsm.Place} to be considered visible (in meters). */
    public final static float DISTANCE_PLACE_VISIBILITY = 75f;
    
    /** 30 fps ms interval. */
    public final static short INTERVAL_MAX    = 33;
    /** 20 fps ms interval. */
    public final static short INTERVAL_FAST   = 50;
    /** 10 fps ms interval. */
    public final static short INTERVAL_NORMAL = 100;
    /** 1 fps ms interval. */
    public final static short INTERVAL_SLOW   = 1000;
    
    /** Amount of silence (in seconds) before starting the {@link Soundscape}'s playback (8s). */
    public final static short TIMEOUT_LANDSCAPE_START = 2000;
    /** Amount of time we wait before considering that there's no location to be found (20s). */
    public final static short TIMEOUT_LOCATION_SEARCH = 20000;
    
    /** The duration of camera animation usually invoked whith {@link SharedBuffer#map()}{@code .animateCamera()}. */
    public final static short DURATION_CAMERA_ANIMATION = 500;
    /** Amount of time (in ms) after playback for a sound to be considered enough buffered. */
    public final static short DURATION_MIN_BUFFERING    = 4000;
    /** Fading duration. */
    public final static short DURATION_FADING_FAST      = 500;
    public final static short DURATION_FADING_SLOW      = 3000;
    
    /** A list of {@code LatLng} to simulate a track. */
    public final static LatLng[] SIMULATION_LOCATIONS = new LatLng[]{
            new LatLng( 50.82996146474417, 4.338984015567377 ),
            new LatLng( 50.82988175097674, 4.339303238340136 ),
            new LatLng( 50.829783280841156, 4.3397041227823365 ),
            new LatLng( 50.82967543236231, 4.340268330505012 ),
            new LatLng( 50.829586339952925, 4.34067663871582 ),
            new LatLng( 50.82952069280148, 4.340973590143619 ),
            new LatLng( 50.82962385256775, 4.341255694014791 ),
            new LatLng( 50.82980672613152, 4.3416343070925905 ),
            new LatLng( 50.82998490994507, 4.342057462879666 ),
            new LatLng( 50.83018184915787, 4.342480618666826 ),
            new LatLng( 50.83037409853887, 4.342376685652425 ),
            new LatLng( 50.830674193554756, 4.342042615303399 ),
            new LatLng( 50.830969597694065, 4.3417530876637045 ),
            new LatLng( 50.831082132113124, 4.342042615303399 ),
            new LatLng( 50.83118997734806, 4.342406380804903 ),
            new LatLng( 50.83130251123583, 4.34273302738535 ),
            new LatLng( 50.83155571149439, 4.342540008952369 ),
            new LatLng( 50.83183235465242, 4.342324719155016 ),
            new LatLng( 50.832062107891005, 4.3421539720864075 ),
            new LatLng( 50.83197770887102, 4.341842173082313 ),
            new LatLng( 50.83182297694216, 4.34147840758078 ),
            new LatLng( 50.83180891037941, 4.340921623646267 ),
            new LatLng( 50.83162135578394, 4.3404390775735635 ),
            new LatLng( 50.83147600044944, 4.340119854781278 ),
            new LatLng( 50.83126031105817, 4.339615037344231 ),
            new LatLng( 50.83103524280787, 4.3391102199267095 ),
            new LatLng( 50.83084768510366, 4.338724183060748 ),
            new LatLng( 50.83063668177786, 4.338635097622557 ),
            new LatLng( 50.83033189752186, 4.3387538781937 ),
            new LatLng( 50.83005524547096, 4.338887506341166 ) };
    
    
    /* -------- DEV MODE RELATED -------- */
    /** The duration of the simulation (in ms). */
    public static final int   SIMULATION_DURATION  = 45 * 1000;
    /** The minimum distance (in meters) wherein which a point is considered to be near an an other one. */
    final static        short DISTANCE_MIN_NEAREST = 10000;
}
