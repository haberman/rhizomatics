package haberman.rhizomatics.data;

import android.support.annotation.Nullable;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import haberman.rhizomatics.map.PolyFactory;

/**
 * The root area.
 * Doesn't do much but assert that {@link haberman.rhizomatics.data.SharedBuffer#instance_} isn't {@code null}.
 *
 * @author haberman
 */

public final class Area
{
    private final String name_;
    
    /** Boundary of the area as a list of locations. */
    @Nullable
    private ArrayList<LatLng> boundary_;
    
    /** Aligned bounding box. */
    @Nullable
    private LatLngBounds bounds_ = null;
    
    /**
     * Constructor.
     *
     * @param area_data A {@code JSONObject} that describes the geographic boundary of the application.
     *
     * @throws JSONException when area_data has no "boundary" member.
     */
    public Area( final JSONObject area_data ) throws JSONException {
        name_ = area_data.getString( "name" );
        boundary_ = PolyFactory.decode_polygon( area_data.getJSONArray( "boundary" ) );
    
        if (boundary_ == null) {
            return;
        }
    
        final LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        bounds.includes( boundary_ );
    
        bounds_ = bounds.build();
    }
    
    public final String name() {
        return name_;
    }
    
    public final ArrayList<LatLng> boundary() { return boundary_; }
    
    public final LatLngBounds bounds() { return bounds_; }
}
