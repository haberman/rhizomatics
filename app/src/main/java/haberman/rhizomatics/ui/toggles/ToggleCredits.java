package haberman.rhizomatics.ui.toggles;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.UXEvent;

public class ToggleCredits extends ToggleButton
{
    public static final String CREDITS_OFF = "toggle_credits_off";
    public static final String CREDITS_ON  = "toggle_credits_on";
    
    public ToggleCredits( Context context ) {
        super( context );
    }
    
    public ToggleCredits( Context context, AttributeSet attrs ) { super( context, attrs ); }
    
    @Override
    void init() {
        super.init();
        
        states_.add( CREDITS_OFF );
        states_.add( CREDITS_ON );
        
        state_ = CREDITS_OFF;
    }
    
    @Override
    public void set_state( @Nullable String state, final boolean dispatch_change ) {
        super.set_state( state, dispatch_change );
        
        switch (state_) {
            case CREDITS_OFF:
                drawable_ = R.drawable.vd_info_off;
                break;
            case CREDITS_ON:
                drawable_ = R.drawable.vd_info_on;
                break;
            
        }
        
        setBackground( getContext().getDrawable( drawable_ ) );
        
        if (dispatch_change) {
            SharedBuffer.dispatcher()
                        .dispatch_event( new UXEvent( UXEvent.MAPMENU_ACTION, state_ ) );
        }
    }
}
