Promeneur, au fond, à gauche, tu aperçois une photo
Noire et blanche, elle a le charme des clichés d’autrefois.
Si tu te penches vers elle et que tu tends l’oreille,
Tu entendras la jolie histoire d’un papa à ses enfants :

« Au commencement était le silence
Le calme et le vert, à peine troublé par le bruit du vent
Puis vint un frémissement, une rumeur
Un souffle nouveau sur les plaines et les champs.

Rapidement, le murmure devint bruit…
Bruit d’une terre qui s’ouvre, du béton qui coule
Bruit des bottes en caoutchouc qui se perdent dans la boue
Bruit de chants latins ressuscités d’un autre temps
Bruit d’un train qui se vide le dimanche, se remplit le vendredi
Bruit de ces Christophe Colomb calottés, à l’assaut d’un nouveau monde.
Bruit d’une jeunesse impétueuse et libre, qui construit une ville nouvelle »
