package haberman.rhizomatics.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.webkit.WebView;
import android.widget.FrameLayout;

import java.util.Locale;

import haberman.rhizomatics.R;
import haberman.rhizomatics.ui.anim.SlideAnimation;

public class CreditsFragment extends Fragment implements Animation.AnimationListener
{
    private FrameLayout wrapper_;
    
    /** View animations */
    private SlideAnimation slide_animation_;
    
    
    public View onCreateView( @NonNull LayoutInflater inflater,
                              ViewGroup container,
                              Bundle bundle ) {
        wrapper_ = (FrameLayout) inflater.inflate( R.layout.credits_fragment, container, false );
        
        slide_animation_ = new SlideAnimation( wrapper_, 0, SlideAnimation.Side.BOTTOM );
        slide_animation_.setAnimationListener( this );
        
        final Locale  locale   = getResources().getConfiguration().locale;
        final WebView web_view = wrapper_.findViewById( R.id.web_view );
        
        web_view.loadUrl( getString( R.string.url_remote ) +
                          "credits/index-" + locale.getLanguage() + ".html" );
        
        wrapper_.setAlpha( 0 );
        wrapper_.setY( slide_animation_.hidden() );
        return wrapper_;
        
    }
    
    @Override
    public void onDestroyView() {
        slide_animation_.setAnimationListener( null );
        slide_animation_ = null;
        
        super.onDestroyView();
    }
    
    /** Slides {@link #wrapper_} out. */
    public void hide() { if (slide_animation_.hide()) { slide_animation_.start(); } }
    
    /** Slides {@link #wrapper_} in. */
    public void show() { if (slide_animation_.show()) { slide_animation_.start(); } }
    
    @Override
    public void onAnimationStart( Animation animation ) {
        if (animation.equals( slide_animation_ )) { wrapper_.setVisibility( View.VISIBLE ); }
    }
    
    @Override
    public void onAnimationEnd( Animation animation ) {
        if (slide_animation_.state().equals( SlideAnimation.State.HIDDEN )) {
            wrapper_.setVisibility( View.GONE );
        }
    }
    
    @Override
    public void onAnimationRepeat( Animation animation ) { /* pass */ }
    
}
