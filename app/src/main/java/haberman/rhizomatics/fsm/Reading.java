package haberman.rhizomatics.fsm;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.delegates.Action;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.fsm.entities.SoundEntity;
import timber.log.Timber;

import static haberman.rhizomatics.fsm.AudioPlayer.AppState.BUFFERING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.COMPLETED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.IDLE;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PAUSED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PLAYING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PREPARING;
import static haberman.rhizomatics.fsm.AudioPlayer.AudioType.READING;

/**
 * The Reading state machine driving events related to a {@link AudioType#READING} playback.
 *
 * @author haberman
 */

public class Reading extends AudioPlayer
{
    /** The entity handled by this state machine. */
    @Nullable
    private SoundEntity sound_entity_ = null;
    
    public Reading( @NonNull final Context context ) {
        super( context, READING );
    
        fsm_config_.configure( IDLE )
                   .onEntry( new IdleEntry() )
                   .ignore( AppTrigger.complete ) // not sure why, but this happens
                   .permitDynamic( AppTrigger.play, () -> prepared_ ? PLAYING : PREPARING );
        
        fsm_config_.configure( PLAYING )
                   .onEntry( new PlayEntry() )
                   .permit( AppTrigger.buffer, BUFFERING )
                   .permit( AppTrigger.pause, PAUSED )
                   .permit( AppTrigger.complete, COMPLETED )
                   .permit( AppTrigger.destroy, IDLE );
    
        fsm_ = new StateMachine<>( IDLE, fsm_config_ );
        fsm_.onUnhandledTrigger( ( arg1, arg2 ) -> Timber.i( "unhandled  trigger %s from state %s ",
                                                             arg2,
                                                             fsm_.getState() ) );
    }
    
    /**
     * Given the {@link SoundEntity.Where} enum value, returns a matching track uri.
     *
     * @return An {@code Uri} instance.
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    protected Uri select_track() {
        if (sound_entity_.where().equals( SoundEntity.Where.INSIDE )) {
            return Uri.parse( context_.getString( R.string.url_remote )
                              + sound_entity_.place_id() + ".ogg" );
        } else {
            return null; // TODO: implements
        }
    }
    
    /** {@link AppTrigger#play} trigger. */
    public void play( @Nullable SoundEntity sound_entity ) {
        if (sound_entity != null) { sound_entity_ = sound_entity; }
        fsm_.fire( AppTrigger.play );
    }
    
    /**
     * Adds a new entity to be queued.
     *
     * @param sound_entity The entity to add
     */
    public void queue( @NonNull SoundEntity sound_entity ) {
        sound_entity_ = sound_entity;
        queue_ = true;
    }
    
    @Nullable
    public SoundEntity sound_entity() { return sound_entity_; }
    
    /**
     * Action taken before the state machine enters {@link AppState#IDLE}
     * -> releases the {@link #player_} & sets the {@link AudioPlayer.AppState} to {@link AudioPlayer.AppState#IDLE}.
     */
    class IdleEntry implements Action
    {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void doIt() {
            buffer_handler_.removeCallbacks( buffer_runnable_ );
            if (player_ != null) {
                fade_out_( IDLE, Constants.DURATION_FADING_FAST, true );
            }
        }
    }
}
