package haberman.rhizomatics.fsm;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.StateMachineConfig;
import com.github.oxo42.stateless4j.delegates.Action;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.plugins.building.BuildingPlugin;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Objects;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.Area;
import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.IEvent;
import haberman.rhizomatics.events.IEventHandler;
import haberman.rhizomatics.events.MapEvent;
import haberman.rhizomatics.events.PlaceEvent;
import haberman.rhizomatics.events.SoundEvent;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import timber.log.Timber;

import static haberman.rhizomatics.fsm.AudioPlayer.AudioType.READING;

/**
 * The Map state machine.
 *
 * @author haberman
 */
public class Map implements OnMapReadyCallback, IEventHandler
{
    
    /** @link haberman.rhizomatics.MainActivity}'s {@code Context} backref. */
    private final Context context_;
    
    /** {@link haberman.rhizomatics.MainActivity}'s map view. */
    private final MapView view_;
    
    /** The state machine. */
    private final StateMachine<AppState, AppTrigger> fsm_;
    
    /** The soundscape area. */
    @Nullable
    private Area area_ = null;
    
    /** An array containing every {@link Place}s. */
    private ArrayList<Place> places_;
    
    /** A reference to the last touched place. */
    @Nullable
    private Place active_place_ = null;
    
    /** Indicates that the map is ready. */
    private boolean map_ready_ = false;
    
    /** Keeps track of {@link AppState#RUNNING} sub-states. */
    private boolean marked_  = false;
    private boolean tracked_ = false;
    
    /** {@code BuildingPlugin} implementation. */
    private BuildingPlugin building_plugin_;
    
    /**
     * Constructor.
     *
     * @param context Application {@code Context}
     * @param view    Mapbox {@code MapView}
     */
    public Map( @NonNull final Context context,
                @NonNull final MapView view ) {
        context_ = context;
        view_ = view;
        
        final StateMachineConfig<AppState, AppTrigger> fsm_config = new StateMachineConfig<>();
        
        fsm_config.configure( AppState.IDLE )
                  .onEntry( new IdleEntry() )
                  .permit( AppTrigger.load, AppState.LOADING );
        
        fsm_config.configure( AppState.LOADING )
                  .onEntry( new LoadEntry() )
                  .permit( AppTrigger.stop, AppState.STOPPED )
                  .permit( AppTrigger.track, AppState.TRACKING_ON )
                  .permit( AppTrigger.mark, AppState.MARKING_ON );
        
        fsm_config.configure( AppState.RUNNING )
                  .permit( AppTrigger.stop, AppState.STOPPED )
                  .permitDynamic( AppTrigger.mark,
                                  () -> marked_ ? AppState.MARKING_ON : AppState.MARKING_OFF )
                  .permitDynamic( AppTrigger.track,
                                  () -> tracked_ ? AppState.TRACKING_ON : AppState.TRACKING_OFF );
        
        fsm_config.configure( AppState.MARKING_ON )
                  .substateOf( AppState.RUNNING )
                  .onEntry( new MarkEntry() );
        
        fsm_config.configure( AppState.MARKING_OFF )
                  .substateOf( AppState.RUNNING )
                  .onEntry( new MarkExit() );
        
        fsm_config.configure( AppState.TRACKING_ON )
                  .substateOf( AppState.RUNNING )
                  .onEntry( new TrackEntry() )
                  .permitReentry( AppTrigger.track );
        
        fsm_config.configure( AppState.TRACKING_OFF )
                  .substateOf( AppState.RUNNING )
                  .onEntry( new TrackExit() );
        
        fsm_config.configure( AppState.STOPPED )
                  .onEntry( new StopEntry() )
                  .ignore( AppTrigger.stop ) // this happens when we exit the app
                  .permit( AppTrigger.destroy, AppState.IDLE )
                  .permit( AppTrigger.track, AppState.TRACKING_ON )
                  .permit( AppTrigger.mark, AppState.MARKING_ON );
        
        fsm_ = new StateMachine<>( AppState.IDLE, fsm_config );
        fsm_.onUnhandledTrigger( ( arg1, arg2 ) -> Timber.i( "unhandled  trigger %s from state %s",
                                                             arg2,
                                                             fsm_.getState() ) );
        
        fsm_.fire( AppTrigger.load );
    }
    
    /** {@code OnMapReadyCallback} implementation. */
    @SuppressWarnings("ConstantConditions")
    @Override
    public void onMapReady( final MapboxMap mapboxMap ) {
        mapboxMap.getUiSettings().setLogoEnabled( false );
        mapboxMap.getUiSettings().setAttributionEnabled( false );
        mapboxMap.getUiSettings().setCompassEnabled( false );
        
        SharedBuffer.set_map( mapboxMap );
        
        final ArrayList<PlaceEntity> places = SharedBuffer.manager().all_places();
        places_ = new ArrayList<>();
        for (final PlaceEntity place_entity : places) {
            places_.add( new Place( context_, place_entity ) );
        }
        
        mapboxMap.setOnMarkerClickListener( marker -> {
            final Place new_place = place_from_marker_( marker );
            boolean     share;
            
            if (active_place_ == null) {
                active_place_ = new_place;
                active_place_.marker_select( true );
                share = true;
            } else {
                final boolean new_is_different = !new_place.entity()
                                                           .id()
                                                           .equals( active_place_.entity()
                                                                                 .id() );
                if (new_is_different) {
                    active_place_.marker_select( false ); // un-select current
                    active_place_ = new_place;            // set new
                    active_place_.marker_select( true );  // select new
                }
                
                share = new_is_different;
            }
            
            if (share) {
                final MapEvent me = new MapEvent( MapEvent.CLICK_MARKER,
                                                  active_place_.entity() );
                
                
                SharedBuffer.set_audio_type( READING );
                SharedBuffer.dispatcher().dispatch_event( me );
            }
            
            return true;
        } );
        
        mapboxMap.addOnMapLongClickListener( latLng -> SharedBuffer.dispatcher()
                                                                   .dispatch_event( new MapEvent(
                                                                           MapEvent.CLICK_LONG,
                                                                           null ) ) );
        
        mapboxMap.addOnMapClickListener( latLng -> {
            if (SharedBuffer.user_tracking()) {
                SharedBuffer.map()
                            .animateCamera( CameraUpdateFactory.newLatLng( SharedBuffer.location() ),
                                            Constants.DURATION_CAMERA_ANIMATION );
            }
            
            SharedBuffer.dispatcher().dispatch_event( new MapEvent( MapEvent.CLICK, null ) );
        } );
        
        building_plugin_ = new BuildingPlugin( view_, mapboxMap );
        building_plugin_.setMinZoomLevel( 15 );
        building_plugin_.setColor( context_.getResources().getColor( R.color.map_building ) );
        building_plugin_.setOpacity( Constants.OPACITY_BUILDING );
        
        map_ready_ = true;
        SharedBuffer.dispatcher().dispatch_event( new MapEvent( MapEvent.READY, null ) );
    }
    
    /**
     * Static method that finds a place sharing the same location as a marker.
     *
     * @param marker The marker
     *
     * @return A place or null if no match.
     */
    @Nullable
    private Place place_from_marker_( final Marker marker ) {
        for (final Place place : places_) {
            final LatLng place_location  = place.entity().center();
            final LatLng marker_location = marker.getPosition();
            
            if (place_location.equals( marker_location )) {
                return place;
            }
        }
        
        return null;
    }
    
    /**
     * Sets the current active place.
     *
     * @param place_entity The new entity
     */
    public void set_active_place( @NonNull PlaceEntity place_entity ) {
        active_place_ = place_from_entity_( place_entity );
    }
    
    /**
     * Given a {@link PlaceEntity}, returns the corresponding {@link Place}, as found within {@link #places_}.
     *
     * @param place_entity A place entity
     *
     * @return A matching member of {@link #places_} or {@code null} in case nothing has been found.
     */
    @Nullable
    private Place place_from_entity_( final PlaceEntity place_entity ) {
        for (final Place place : places_) {
            if (place.entity().equals( place_entity )) {
                return place;
            }
        }
        
        return null;
    }
    
    /** Animates the {@link SharedBuffer#map()} camera to frame the given locations found in {@link #places_}. */
    private void frame_places_() {
        final LatLngBounds.Builder bounds = new LatLngBounds.Builder();
        
        for (final Place place : places_) {
            bounds.includes( place.entity().points() );
        }
        
        SharedBuffer.map().animateCamera( CameraUpdateFactory.newLatLngBounds( bounds.build(), 20 ),
                                          Constants.DURATION_CAMERA_ANIMATION,
                                          null );
    }
    
    /** @link AppTrigger#track} trigger. */
    public void track( final boolean value ) {
        tracked_ = value;
        fsm_.fire( AppTrigger.track );
    }
    
    /** {@link AppTrigger#mark} trigger. */
    public void mark( final boolean value ) {
        marked_ = value;
        fsm_.fire( AppTrigger.mark );
    }
    
    /** {@link AppTrigger#stop} trigger. */
    public void stop() { fsm_.fire( AppTrigger.stop ); }
    
    /** Removes every markers actually visible. */
    private void remove_markers_() {
        for (final Marker marker : SharedBuffer.map().getMarkers()) {
            Objects.requireNonNull( place_from_marker_( marker ) ).mark( false );
        }
    }
    
    /** {@link AppTrigger#destroy} trigger. */
    public void destroy() {
        fsm_.fire( AppTrigger.destroy );
    }
    
    /** Resume trigger. */
    public void resume() {
        if (tracked_) {
            fsm_.fire( AppTrigger.track );
        }
        if (marked_) {
            fsm_.fire( AppTrigger.mark );
        }
    }
    
    public boolean ready() {
        return map_ready_;
    }
    
    public boolean tracked() { return tracked_; }
    
    public boolean marked() { return marked_; }
    
    @Nullable
    public Place active_place() {
        return active_place_;
    }
    
    public AppState state() {
        return fsm_.getState();
    }
    
    @Override
    public void on_event( IEvent e ) {
        if (e.event_type().equals( SoundEvent.VOLUME )) {
            if (active_place_ != null) { active_place_.apply_sound_event( (SoundEvent) e ); }
        }
    }
    
    /** States enum. */
    public enum AppState
    {
        IDLE, LOADING, RUNNING, TRACKING_ON, TRACKING_OFF, MARKING_ON, MARKING_OFF, STOPPED
    }
    
    /** Triggers enum. */
    enum AppTrigger
    {
        load, track, mark, stop, destroy
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#IDLE}
     * -> clears the {@link #places_} list, nullify and destroy the Mapbox {@link #view_}.
     */
    class IdleEntry implements Action
    {
        @Override
        public void doIt() {
            places_.clear();
            places_ = null;
            
            area_ = null;
            
            building_plugin_.setVisibility( false );
        }
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#LOADING}
     * -> inits the {@link #area_} and gets a new Mapbox instance (asynchronously).
     */
    class LoadEntry implements Action
    {
        @Override
        public void doIt() {
            if (area_ == null) {
                try {
                    area_ = SharedBuffer.manager().init();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            
            if (!map_ready_) {
                view_.getMapAsync( Map.this );
            }
        }
    }
    
    /**
     * Action taken when the state machine enters {@link AppState#TRACKING_ON}.
     * -> {@link Place#update_()} every {@link #places_}.
     */
    class TrackEntry implements Action
    {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void doIt() {
            building_plugin_.setVisibility( true );
            
            // First pass -> update_ each place
            for (final Place place : places_) { place.update_(); }
            
            // Second pass -> try to collect an eventual inside place
            Place inside_place = null;
            for (final Place place : places_) {
                if (place.state().equals( Place.AppState.INSIDE )) {
                    inside_place = place;
                    break;
                }
            }
            
            final boolean in = inside_place != null
                               && !SharedBuffer.position_state()
                                               .equals( SharedBuffer.PositionState.INSIDE );
            
            final boolean out = inside_place == null
                                && !SharedBuffer.position_state()
                                                .equals( SharedBuffer.PositionState.OUTSIDE );
            if (in) {
                set_active_place( inside_place.entity() );
                SharedBuffer.set_position_state( SharedBuffer.PositionState.INSIDE );
                
                final PlaceEvent pe = new PlaceEvent( PlaceEvent.ENTER, active_place_.entity() );
                SharedBuffer.dispatcher().dispatch_event( pe );
                SharedBuffer.dispatcher().add_listener( SoundEvent.VOLUME, Map.this );
                
            } else if (out) {
                SharedBuffer.set_position_state( SharedBuffer.PositionState.OUTSIDE );
                
                if (active_place_ != null) {
                    assert active_place_.entity() != null;
                    
                    final PlaceEvent pe = new PlaceEvent( PlaceEvent.EXIT, active_place_.entity() );
                    SharedBuffer.dispatcher().remove_listener( SoundEvent.VOLUME, Map.this );
                    SharedBuffer.dispatcher().dispatch_event( pe );
                    
                    active_place_ = null;
                }
            }
        }
    }
    
    /**
     * Action taken when the state machine exits {@link AppState#TRACKING_OFF}
     * -> {@link Place#stop_()} every {@link #places_}.
     */
    class TrackExit implements Action
    {
        @Override
        public void doIt() {
            if (!SharedBuffer.user_tracking()) { return; }
            
            for (final Place place : places_) { place.stop_();}
            
            building_plugin_.setVisibility( false );
            frame_places_();
        }
    }
    
    /**
     * Action taken when the state machine enters {@link AppState#MARKING_ON}.
     * -> {@link Place#mark(boolean)} every {@link #places_} and {@link #frame_places_()} if necessary.
     */
    class MarkEntry implements Action
    {
        @Override
        public void doIt() {
            for (final Place place : places_) {
                place.mark( true );
                place.marker_select( false );
            }
            
            if (active_place_ != null) { active_place_.marker_select( true ); }
            
            // only frame places if we're not tracking user.
            if (!SharedBuffer.user_tracking()) { frame_places_(); }
        }
    }
    
    /**
     * Action taken when we're leaving the discovery mode
     * -> removes each marker from the map.
     */
    class MarkExit implements Action
    {
        @Override
        public void doIt() {
            remove_markers_();
        }
    }
    
    /**
     * Action taken when the state machine enters {@link AppState#STOPPED}
     * -> {@link Place#stop_()} every {@link #places_} and sets {@link #tracked_} to {@code false}.
     */
    class StopEntry implements Action
    {
        @Override
        public void doIt() {
            remove_markers_();
            
            for (final Place place : places_) { place.stop_(); }
        }
    }
}
