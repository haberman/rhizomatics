package haberman.rhizomatics.events;

import android.support.annotation.NonNull;

/**
 * {@link IEvent} implementation for UI related events.
 *
 * @author haberman
 */

public class UXEvent implements IEvent
{
    public static final String PLAYER_ACTION  = "event_ux_player_action";
    public static final String MAPMENU_ACTION = "event_ux_mapmenu_action";
    
    /**
     * The type of event being fired (which here corresponds to the fragment emitting it).
     */
    @NonNull
    private final String event_type_;
    
    /**
     * A string to indicates the fragment-specific action that occurred.
     */
    @NonNull
    private final String action_;
    
    /**
     * Constructor.
     *
     * @param event_type The
     * @param action     The action
     */
    public UXEvent( @NonNull final String event_type, @NonNull final String action ) {
        event_type_ = event_type;
        action_ = action;
    }
    
    @NonNull
    @Override
    public final String event_type() { return event_type_; }
    
    @NonNull
    public final String action() { return action_; }
}
