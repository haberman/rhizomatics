package haberman.rhizomatics.ui.toggles;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.UXEvent;

/**
 * 3-states locations' toggle button.
 *
 * @author haberman
 */

public class ToggleLocation extends ToggleButton
{
    /** Location is unknown. */
    public static final String LOCATION_OFF = "toggle_location_off";
    
    /** GPS is active. */
    public static final String LOCATION_ON = "toggle_location_on";
    
    /** Location is also refined by compass sensor. */
    public static final String LOCATION_COMPASS = "toggle_location_compass";
    
    public ToggleLocation( Context context ) { super( context ); }
    
    public ToggleLocation( Context context, AttributeSet attrs ) { super( context, attrs ); }
    
    @Override
    void init() {
        super.init();
        
        states_.add( LOCATION_OFF );
        states_.add( LOCATION_ON );
        states_.add( LOCATION_COMPASS );
        
        state_ = LOCATION_OFF;
    }
    
    @Override
    public void set_state( @Nullable String state, final boolean dispatch_change ) {
        super.set_state( state, dispatch_change );
    
        SharedBuffer.LocationState location_state = SharedBuffer.LocationState.OFF;
    
        switch (state_) {
            case LOCATION_OFF:
                drawable_ = R.drawable.vd_location_off;
                break;
            case LOCATION_ON:
                drawable_ = R.drawable.vd_location_on;
                location_state = SharedBuffer.LocationState.ON;
                break;
            case LOCATION_COMPASS:
                drawable_ = R.drawable.vd_location_compass;
                location_state = SharedBuffer.LocationState.COMPASS;
                break;
        
        }
    
        setBackgroundDrawable( getContext().getDrawable( drawable_ ) );
        SharedBuffer.set_location_state( location_state );
    
        if (dispatch_change) {
            SharedBuffer.dispatcher()
                        .dispatch_event( new UXEvent( UXEvent.MAPMENU_ACTION, state_ ) );
        }
    }
}
