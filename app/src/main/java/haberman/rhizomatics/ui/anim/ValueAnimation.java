/*
 * Copyright (C) 2015 Hooked On Play
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package haberman.rhizomatics.ui.anim;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.animation.Interpolator;

import static android.animation.ValueAnimator.INFINITE;
import static android.animation.ValueAnimator.RESTART;

/**
 * ValueAnimation provides the ability to animate changing in numbers using the builtin Android Interpolator animation functionality.
 *
 * @author haberman
 */
public class ValueAnimation
{
    /** Duration of the animation. */
    private final long duration_;
    
    /** Initial value to start animation. */
    private final float start_value_;
    
    /** End value to finish animation. */
    private final float end_value_;
    
    /** Repeat mode, either. */
    private final int repeat_mode_;
    
    /** Amount of repetitions. */
    private final int repeat_count_;
    
    /** Interpolator functionality to apply to animation. */
    private final Interpolator  interpolator_;
    private       ValueAnimator value_animator_;
    
    /**
     * Private constructor.
     *
     * @param builder The public builder on which the animation has been configured.
     */
    private ValueAnimation( Builder builder ) {
        duration_ = builder.duration_;
        
        start_value_ = builder.start_value_;
        end_value_ = builder.end_value_;
        
        repeat_mode_ = builder.repeat_mode_;
        repeat_count_ = builder.repeat_count_;
        
        interpolator_ = builder.interpolator_;
    }
    
    /**
     * Executes the animation.
     *
     * @param listener Event listener on which animation end will be dispatched.
     */
    public void execute( @NonNull final AnimateListener listener ) {
        if (value_animator_ == null) {
            value_animator_ = android.animation.ValueAnimator.ofFloat( start_value_, end_value_ );
    
            value_animator_.setDuration( duration_ );
            value_animator_.setInterpolator( interpolator_ );
            value_animator_.setRepeatMode( repeat_mode_ );
            value_animator_.setRepeatCount( repeat_count_ );
        }
    
        value_animator_.addUpdateListener( valueAnimator -> {
            final float current = Float
                    .valueOf( valueAnimator.getAnimatedValue().toString() );
            listener.on_animate_update( current );
        } );
        
        value_animator_.addListener( new AnimatorListenerAdapter()
        {
            @Override
            public void onAnimationEnd( Animator animation ) {
                listener.on_animate_end();
            }
    
            @Override
            public void onAnimationRepeat( Animator animation ) {
                listener.on_animate_repeat();
            }
    
        } );
    
        value_animator_.start();
    }
    
    /** Cancels and eventually running animation. */
    public void cancel() {
        if (value_animator_ == null) {
            return;
        }
        
        value_animator_.cancel();
        value_animator_.removeAllListeners();
    }
    
    public boolean running() {
        return value_animator_ != null && value_animator_.isRunning();
    }
    
    /** Callback interface for notification of animations' events. */
    public interface AnimateListener
    {
        void on_animate_update( float value );
    
        void on_animate_repeat();
    
        void on_animate_end();
    }
    
    /**
     * A class with a bunch of chained methods used to configure a {@link ValueAnimation}.
     */
    public static class Builder
    {
        /** Default duration **/
        private long duration_ = 2000;
        
        /** Default start value. */
        private float start_value_ = 0;
        
        /** Default end values. */
        private float end_value_ = 1f;
        
        /** Default repeat mode. */
        private int repeat_mode_ = RESTART;
        
        /** Default repeat count. */
        private int repeat_count_ = INFINITE;
        
        /** {@code Interpolator} instance. */
        private Interpolator interpolator_ = null;
        
        /**
         * Empty constructor for default values fallback.
         */
        public Builder() { /* pass */ }
        
        /**
         * Set the start_animation and end floating point numbers to be animated
         *
         * @param start     initial value
         * @param end       final value
         * @param precision number of decimals involved in the animation's computation
         *
         * @return This Builder object to allow chaining methods.
         */
        public Builder set_count( final float start, final float end, final int precision ) {
            if (Math.abs( start - end ) < 0.001) {
                throw new IllegalArgumentException( "Count start and end must be different" );
            } else if (precision < 0) {
                throw new IllegalArgumentException( "Precision can't be negative" );
            }
            
            start_value_ = start;
            end_value_ = end;
            
            return this;
        }
        
        /**
         * Set the duration of the animation from start_animation to end
         *
         * @param duration total duration of animation in ms
         *
         * @return This Builder object to allow chaining methods.
         */
        public Builder set_duration( long duration ) {
            if (duration <= 0) {
                throw new IllegalArgumentException( "Duration must be positive value" );
            }
            
            duration_ = duration;
            
            return this;
        }
        
        /**
         * Set the interpolator to be used with the animation
         *
         * @param interpolator Optional interpolator to set
         *
         * @return This Builder object to allow chaining methods.
         */
        public Builder set_interpolator( @Nullable Interpolator interpolator ) {
            interpolator_ = interpolator;
            
            return this;
        }
        
        /**
         * Set repetition related variables.
         *
         * @param repeat_mode  The mode, either: REVERSE or REPEAT
         * @param repeat_count The amount of repetitions (set to 0 for none and INFINITE for never-ending)
         *
         * @return This Builder object to allow chaining methods.
         */
        public Builder set_repeat( int repeat_mode, int repeat_count ) {
            repeat_mode_ = repeat_mode;
            repeat_count_ = repeat_count;
            
            return this;
        }
        
        /**
         * Creates a {@link ValueAnimation} with the arguments supplied to this builder.
         * Use {@link #execute(AnimateListener)} to start it.
         *
         * @return New ValueAnimation sets from its private constructor.
         */
        public ValueAnimation build() {
            return new ValueAnimation( this );
        }
    }
}