package haberman.rhizomatics.data;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import haberman.rhizomatics.events.EventDispatcher;
import haberman.rhizomatics.fsm.AudioPlayer;

/**
 * This singleton class is used to sync instances of shared pieces of the application.
 *
 * @author haberman
 */

public class SharedBuffer
{
    /** This singleton. */
    private static SharedBuffer instance_;
    
    /** A {@link DataManager} instance. */
    private final DataManager data_manager_;
    
    /** An {@link EventDispatcher} engine instance. */
    private final EventDispatcher event_dispatcher_;
    
    /** Last user location. */
    private final LatLng user_location_;
    
    /** Size of the screen (in pixels). */
    private final short screen_width_;
    private final short screen_height_;
    
    /** The current {@link LocationState}. */
    @NonNull
    private LocationState location_state_ = LocationState.OFF;
    
    /** The current {@link PositionState}. */
    @NonNull
    private PositionState position_state_ = PositionState.IDLE;
    
    /** The current {@link AudioPlayer.AppState} audio state. */
    @NonNull
    private AudioPlayer.AppState audio_state_ = AudioPlayer.AppState.IDLE;
    
    /** The current {@link AudioPlayer.AudioType} audio type. */
    @Nullable
    private AudioPlayer.AudioType audio_type_ = null;
    
    /** Pointer to the displayed {@code MapboxMap}. */
    private MapboxMap map_;
    
    /**
     * Private Constructor.
     * Only called from {@link #init_instance(DataManager, DisplayMetrics)} when {@link #instance_}
     * is {@code null}.
     *
     * @param data_manager Final {@link DataManager} instance
     * @param metrics      Final {@code DisplayMetrics} instance
     */
    private SharedBuffer( final DataManager data_manager, final DisplayMetrics metrics ) {
        data_manager_ = data_manager;
        event_dispatcher_ = new EventDispatcher();
        user_location_ = new LatLng();
    
        screen_width_ = (short) metrics.widthPixels;
        screen_height_ = (short) metrics.heightPixels;
    }
    
    /**
     * Constructs and returns the {@link #instance_} singleton.
     *
     * @param data_manager Final {@link DataManager} instance
     * @param metrics      Final {@code DisplayMetrics} instance
     */
    public static void init_instance( @NonNull final DataManager data_manager,
                                      @NonNull final DisplayMetrics metrics ) {
        instance_ = new SharedBuffer( data_manager, metrics );
    }
    
    public synchronized static short screen_height() {
        return instance().screen_height_;
    }
    
    /**
     * Returns the singleton {@link #instance_}.
     * This method remains private since the instance is mostly used to access its members.
     *
     * @return {@link SharedBuffer} singleton.
     * @throws RuntimeException if the {@link #instance_} hasn't been initialized.
     */
    @NonNull
    private synchronized static SharedBuffer instance() {
        if (instance_ == null) {
            throw new RuntimeException( "The instance member has not been initialized." );
        }
    
        return instance_;
    }
    
    public synchronized static short screen_width() { return instance().screen_width_; }
    
    @NonNull
    public synchronized static DataManager manager() {
        return instance().data_manager_;
    }
    
    @NonNull
    public synchronized static EventDispatcher dispatcher() {
        return instance().event_dispatcher_;
    }
    
    @NonNull
    public synchronized static MapboxMap map() {
        return instance().map_;
    }
    
    public synchronized static void set_map( final MapboxMap map ) {
        instance().map_ = map;
    }
    
    @NonNull
    public synchronized static LatLng location() {
        return instance().user_location_;
    }
    
    public synchronized static void set_location( LatLng location ) {
        instance().user_location_.setLongitude( location.getLongitude() );
        instance().user_location_.setLatitude( location.getLatitude() );
    }
    
    @NonNull
    public synchronized static PositionState position_state() { return instance().position_state_; }
    
    public synchronized static void set_position_state( @NonNull PositionState position_state ) {
        instance().position_state_ = position_state;
    }
    
    @NonNull
    public synchronized static LocationState location_state() { return instance().location_state_; }
    
    public synchronized static void set_location_state( @NonNull LocationState location_state ) {
        instance().location_state_ = location_state;
    }
    
    public synchronized static void set_audio_state( @Nullable AudioPlayer.AppState audio_state ) {
        instance().audio_state_ = audio_state;
    }
    
    @Nullable
    public synchronized static AudioPlayer.AudioType audio_type() {
        return instance().audio_type_;
    }
    
    public synchronized static void set_audio_type( @NonNull AudioPlayer.AudioType audio_type ) {
        instance().audio_type_ = audio_type;
    }
    
    @NonNull
    public synchronized static AudioPlayer.AppState audio_state() { return instance().audio_state_; }
    
    /**
     * Shortcut to find out if the application is currently tracking user.
     *
     * @return {@code true} is GPS is active, {@code false} otherwise.
     */
    public synchronized static boolean user_tracking() {
        return !instance().location_state_.equals( LocationState.OFF );
    }
    
    /** An enum for the possible location (GPS) states. */
    public enum LocationState
    {
        OFF, ON, COMPASS
    }
    
    /** An enum to indicate where the {@link #user_location_} stands relatively to places. */
    public enum PositionState
    {
        IDLE, INSIDE, OUTSIDE
    }
    
}
