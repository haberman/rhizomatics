As-tu déjà entraperçu la chapelle des Dominicains ? Je te donne un indice, sa forme typique fait penser à la fête de Pâques. Tu ne vois pas ? Songe aux petits poussins. Que dis-tu ? Un œuf ? Oui c’est cela, un œuf copieux rempli de religieux.

Savais-tu qu’en son sein est installé un bar irlandais prisé par les fins gourmets ? Cet ordre fondé au 13e siècle s’est vu attribué les nobles missions de prédication et d’enseignement, destinées tant aux croyants qu’aux athées. En 2010, il est arrivé dans cette ville, ses valises remplies de victuailles, apportant la bonne parole à des ouailles en quête de guindaille.

Depuis, sa vie est rythmée par les processions de foi, unissant l’humain et le divin. Cette présence confirme qu’à Louvain-la-Neuve, une aide sera toujours apportée, à celui ou celle qui en fait la demande.
