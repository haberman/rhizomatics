package haberman.rhizomatics.ui.toggles;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.UXEvent;

/**
 * 2-states markers' toggle button.
 *
 * @author haberman
 */

public class ToggleMarker extends ToggleButton
{
    public static final String MARKER_OFF = "toggle_marker_off";
    public static final String MARKER_ON  = "toggle_marker_on";
    
    public ToggleMarker( Context context ) { super( context ); }
    
    public ToggleMarker( Context context, AttributeSet attrs ) { super( context, attrs ); }
    
    @Override
    void init() {
        super.init();
        
        states_.add( MARKER_OFF );
        states_.add( MARKER_ON );
        
        state_ = MARKER_ON;
    }
    
    @Override
    public void set_state( @Nullable String state, final boolean dispatch_change ) {
        super.set_state( state, dispatch_change );
        
        switch (state_) {
            case MARKER_OFF:
                drawable_ = R.drawable.vd_marker_off;
                break;
            case MARKER_ON:
                drawable_ = R.drawable.vd_marker_on;
                break;
            
        }
        
        setBackground( getContext().getDrawable( drawable_ ) );
        
        if (dispatch_change) {
            SharedBuffer.dispatcher()
                        .dispatch_event( new UXEvent( UXEvent.MAPMENU_ACTION, state_ ) );
        }
    }
}
