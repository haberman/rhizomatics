package haberman.rhizomatics.fsm.entities;

import android.database.Cursor;
import android.support.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import haberman.rhizomatics.fsm.Reading;

/**
 * Class describing every attributes of a {@link Reading}.
 *
 * @author haberman
 */

public class SoundEntity
{
    /**
     * The (SQL AUTOINCREMENT) id of this sound.
     */
    private final int id_;
    
    /**
     * The track selector used to load audio files.
     */
    private final String track_;
    
    /**
     * Id of the place entity this sound belongs to.
     */
    private final String place_id_;
    
    /**
     * Where's the sound being played? Defaults to {@link Where#INSIDE}.
     */
    private final Where where_;
    
    /**
     * A value that holds a factor of interpolation (only used in {@link Where#AROUND} state).
     * TODO: @implements
     */
    private float around_extent_;
    
    /**
     * Private constructor since we always use {@link #from_cursor(Cursor)} to create new instances.
     *
     * @param id       The id
     * @param track    The track
     * @param where    A {@code JSONObject} holding the {@link Where} key
     * @param place_id The id of the associated {@link PlaceEntity}
     */
    private SoundEntity( final int id,
                         final String track,
                         final JSONObject where,
                         final String place_id ) throws JSONException {
        id_ = id;
        track_ = track;
        place_id_ = place_id;
    
        where_ = Where.valueOf( where.getString( "key" ).toUpperCase() );
    
        if (where_ == Where.AROUND) {
            around_extent_ = (float) where.getDouble( "extent" );
        }
    }
    
    /**
     * Creates a new sound entity instance from a {@code SQLite} response.
     *
     * @param cursor The {@code Cursor} response
     *
     * @return A sound entity or {@code null} is a {@code JSONException} is encountered.
     */
    @Nullable
    public static SoundEntity from_cursor( final Cursor cursor ) {
        try {
            final int    id    = cursor.getInt( 0 );
            final String track = cursor.getString( 1 );
            final JSONObject where = (JSONObject) new JSONTokener( cursor.getString( 2 ) )
                    .nextValue();
            final String place_id = cursor.getString( 3 );
    
            return new SoundEntity( id, track, where, place_id );
        } catch (JSONException e) {
            e.printStackTrace();
    
            return null;
        }
    }
    
    public int id() { return id_; }
    
    public String place_id() { return place_id_; }
    
    public Where where() { return where_; }
    
    /**
     * Given the {@link #track_} string value, returns a list of actual audio files.
     * TODO @implements when area will have multiple tracks and some kind of encoded selectors.
     *
     * @return An array of audio files.
     */
    public ArrayList<String> tracks() {
        final ArrayList<String> tracks = new ArrayList<>();
    
        if (!track_.contains( "[" ) && !track_.contains( "]" )) {
            tracks.add( track_ );
        } else {
            final Pattern pattern = Pattern.compile( "(\\w+)\\[(\\d+)-(\\d+)]" );
            final Matcher matcher = pattern.matcher( track_ );
        
            if (matcher.matches()) {
                final String base_name   = matcher.group( 1 );
                final int    start_index = Integer.valueOf( matcher.group( 2 ) );
                final int    stop_index  = Integer.valueOf( matcher.group( 3 ) );
                
                for (int i = start_index; i < stop_index; i++) {
                    tracks.add( base_name + i );
                }
            }
        }
    
        return tracks;
    }
    
    /**
     * List of all possible types of audible area:
     * <ul>
     * <li>{@link #INSIDE}: the sound is a reading played within the place contour;</li>
     * <li>{@link #AROUND}: the sound is audible around the place by a given {@link #around_extent_} factor;</li>
     * <li>{@link #ELSEWHERE}: the playback is played anywhere else.</li>
     * </ul>
     * <p>
     * TODO: @implements {@link #ELSEWHERE}
     */
    public enum Where
    {
        INSIDE, AROUND, ELSEWHERE
    }
}
