package haberman.rhizomatics.events;

import android.support.annotation.NonNull;

/**
 * Interface for a minimalistic event implementation.
 *
 * @author haberman
 */
public interface IEvent
{
    @NonNull
    String event_type();
}
