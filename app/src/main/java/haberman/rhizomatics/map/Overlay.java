package haberman.rhizomatics.map;

import android.view.animation.LinearInterpolator;

import java.util.ArrayList;

import haberman.rhizomatics.data.Gradient;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import haberman.rhizomatics.ui.anim.ValueAnimation;
import timber.log.Timber;

/**
 * This class is the root polygon which describes every overlay within Rhizomatics. It's a solution
 * I've found for polygons' animation, since we can't do it in mapbox (can't use {@code AnimatedVectorDrawable},
 * can't extend the final {@code Polygon})...
 *
 * @author haberman
 * @see <a href="https://github.com/mapbox/maki/issues/247">git issue #247</a>
 */

public class Overlay
{
    /** The default stoke width of polygons. */
    final float STROKE_WIDTH = .5f;
    
    /** An opacity master value to be applied generically. */
    private float opacity_ = 0;
    
    /** How fast should we animated growth **/
    private float growth_speed_ = 1f;
    
    /**
     * An array of {@code float} to be applied to the list of {@link PolyDrawer}. Index 0 is for the
     * polygon, index 1 is for the polyline.
     */
    final float[] ALPHAS = { .6f, .9f };
    
    /** A place entity object descriptor this overlay belongs to. */
    final PlaceEntity place_;
    
    /** A gradient object for rings' fill color interpolation. */
    final Gradient fill_gradient_;
    
    /** A gradient object for rings' stroke color interpolation. */
    final Gradient stroke_gradient_;
    
    /** A list of {@link PolyDrawer} for animation. */
    final ArrayList<PolyDrawer> rings_;
    
    /** {@link ValueAnimation} instance and listeners for triggering growth animations. */
    private final ValueAnimation                 growth_animator_;
    private final ValueAnimation.AnimateListener growth_listener_;
    
    /** A boolean that indicates if this overlay is on the map. */
    private boolean growing_;
    
    /** A boolean that indicates if the user is within this overlay. */
    private boolean inside_;
    
    /** A boolean to follow states of {@link #rings_} {@link PolyDrawer}. */
    private boolean rings_on_;
    
    /**
     * Constructor.
     * Sets up the overlay with the given {@link PlaceEntity}.
     *
     * @param place The {@link PlaceEntity} this overlay is attached to
     */
    Overlay( final PlaceEntity place ) {
        place_ = place;
        
        fill_gradient_ = new Gradient( place_.colors( "fill" ) );
        stroke_gradient_ = new Gradient( place_.colors( "stroke" ) );
        
        rings_ = new ArrayList<>();
        
        growth_animator_ = new ValueAnimation.Builder()
                .set_repeat( android.animation.ValueAnimator.RESTART,
                             android.animation.ValueAnimator.INFINITE )
                .set_count( (float) Math.PI * 2, 0, 4 )
                .set_duration( 1000 )
                .set_interpolator( new LinearInterpolator() )
                .build();
        
        growth_listener_ = new ValueAnimation.AnimateListener()
        {
            @Override
            public void on_animate_update( float value ) {
                for (short i = 0; i < rings_.size(); i++) {
                    final float p      = (float) i / (float) rings_.size();
                    final float growth = (p * (float) Math.PI - value) * growth_speed_;
                    final float alpha  = (.5f + (float) (Math.sin( growth ) * .5f)) * opacity_;
                    
                    rings_.get( i ).set_alpha_( alpha );
                    
                    if (inside_) { // only animate the outer (index 0) ring if inside
                        break;
                    }
                }
            }
            
            @Override
            public void on_animate_repeat() { /* pass */ }
            
            @Override
            public void on_animate_end() { /* pass */ }
        };
        
        compute_rings();
    }
    
    /**
     * The actual {@link #rings_} computation should be done by overriding the method in subclasses.
     */
    void compute_rings() { /* implements */ }
    
    /** Starts the growth animation. */
    private void start_growth() {
        if (growing_) { return; }
        
        growing_ = true;
        growth_animator_.execute( growth_listener_ );
    }
    
    /** Stops the growth animation. */
    private void stop_growth() {
        if (!growing_) { return; }
        
        growing_ = false;
        growth_animator_.cancel();
    }
    
    public void set_spectrum( final float[] spectrum ) {
        if (growing_) { stop_growth(); }
        
        for (short i = 0; i < rings_.size(); i++) {
            rings_.get( i ).on();
            rings_.get( i ).set_alpha_( spectrum[ i ] );
        }
    }
    
    /** The user location is inside. */
    public void in() {
        opacity_ = 1f;
        growth_speed_ = 2f;
        
        if (!rings_on_) {
            for (PolyDrawer ring : rings_) { ring.on(); }
            rings_on_ = true;
        }
    
        if(!inside_)
        {
            for (PolyDrawer ring : rings_) { ring.set_alpha_(0); }
        }
        inside_ = true;
        
        if (!growing_) { start_growth(); }
        
    }
    
    /**
     * The user is outside.
     *
     * @param opacity The new {@link #opacity_} to set.
     */
    public void out( float opacity ) {
        opacity_ = opacity;
        growth_speed_ = 1f;
        inside_ = false;
        
        if (!rings_on_) {
            for (PolyDrawer ring : rings_) { ring.on(); }
            rings_on_ = true;
        }
        
        if (!growing_) { start_growth(); }
    }
    
    /** Removes this overlay from the map. */
    public void off() {
        if (growing_) {
            stop_growth();
            
            inside_ = false;
            rings_on_ = false;
            
            for (PolyDrawer ring : rings_) { ring.off_(); }
        }
    }
}