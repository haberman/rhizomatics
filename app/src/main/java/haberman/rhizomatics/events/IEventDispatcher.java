package haberman.rhizomatics.events;

/**
 * Interface describing the design expected from a high-end event dispatcher.
 *
 * @author haberman
 */
interface IEventDispatcher
{
    void add_listener( final String et, final IEventHandler h );
    
    void remove_listeners_for_event( final String et );
    
    void remove_listeners_for_handler( final IEventHandler h );
    
    void remove_listener( final String et, final IEventHandler h );
    
    void remove_listeners();
    
    void dispatch_event( final IEvent e );
}
