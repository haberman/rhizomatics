package haberman.rhizomatics.fsm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.StateMachineConfig;
import com.github.oxo42.stateless4j.delegates.Action;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.Task;

import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.GPSEvent;
import timber.log.Timber;

/**
 * The GPS state machine.
 *
 * @author haberman
 */
public class GPS
{
    /**
     * Location timeout thread runnable.
     */
    private final Runnable timeout_runnable_;
    
    /**
     * Location timeout thread handler.
     */
    private final Handler timeout_handler_;
    
    /**
     * {@link haberman.rhizomatics.MainActivity}'s {@code Context} backref.
     */
    private final Context context_;
    
    /**
     * {@link haberman.rhizomatics.MainActivity} backref.
     */
    private final Activity activity_;
    
    /**
     * The state machine.
     */
    private final StateMachine<AppState, AppTrigger> fsm_;
    
    /**
     * A {@code FusedLocationProviderClient} instance.
     *
     * @see <a href="https://developers.google.com/android/reference/com/google/android/gms/location/FusedLocationProviderClient">FusedLocationProviderClient</a>
     */
    @Nullable
    private FusedLocationProviderClient location_provider_;
    
    /**
     * A {@code LocationCallback} instance.
     *
     * @see <a href="https://developers.google.com/android/reference/com/google/android/gms/location/LocationCallback">LocationCallback</a>
     */
    @Nullable
    private LocationCallback location_callback_;
    
    /** Amount of time (in ms) spent between 2 location updates when searching for an initial (and acceptable) user location. */
    private short search_duration_ = 0;
    
    /**
     * Constructor.
     *
     * @param context  A {@code context}'s activity backref
     * @param activity An {@code Activity} backref
     */
    public GPS( @NonNull final Context context, @NonNull final Activity activity ) {
        context_ = context;
        activity_ = activity;
        
        timeout_handler_ = new Handler();
        timeout_runnable_ = new Runnable()
        {
            @Override
            public void run() {
                search_duration_ += Constants.INTERVAL_SLOW;
                
                if (search_duration_ >= Constants.TIMEOUT_LOCATION_SEARCH) {
                    search_duration_ = 0;
                    fsm_.fire( AppTrigger.sleep );
                } else {
                    timeout_handler_.postDelayed( timeout_runnable_, Constants.INTERVAL_SLOW );
                }
            }
        };
        
        final StateMachineConfig<AppState, AppTrigger> fsm_config = new StateMachineConfig<>();
        
        fsm_config.configure( AppState.OFF )
                  .onEntry( new StopEntry() )
                  .permit( AppTrigger.search, AppState.SEARCHING );
        
        fsm_config.configure( AppState.SEARCHING )
                  .onEntry( new SearchEntry() )
                  .permit( AppTrigger.start, AppState.ON )
                  .permit( AppTrigger.fail, AppState.FAILURE )
                  .permit( AppTrigger.sleep, AppState.TIMEOUT );
        
        fsm_config.configure( AppState.FAILURE )
                  .onEntry( new FailEntry() )
                  .permit( AppTrigger.search, AppState.SEARCHING );
        
        fsm_config.configure( AppState.TIMEOUT )
                  .onEntry( new SleepEntry() )
                  .permit( AppTrigger.search, AppState.SEARCHING );
        
        fsm_config.configure( AppState.ON )
                  .onEntry( new StartEntry() )
                  .permit( AppTrigger.search, AppState.SEARCHING )
                  .permit( AppTrigger.stop, AppState.OFF );
        
        
        fsm_ = new StateMachine<>( AppState.OFF, fsm_config );
        fsm_.onUnhandledTrigger( ( arg1, arg2 ) -> Timber.i( "unhandled  trigger: %s from state  %s",
                                                             arg2.toString(),
                                                             fsm_.getState() ) );
    }
    
    /** Removes location updates and nullify related holders. */
    private void destroy_internals() {
        assert location_callback_ != null;
        assert location_provider_ != null;
        location_provider_.removeLocationUpdates( location_callback_ );
    
        location_provider_ = null;
        location_callback_ = null;
    }
    
    /** {@link AppTrigger#search} trigger. */
    public void search() {
        fsm_.fire( AppTrigger.search );
    }
    
    /** {@link AppTrigger#stop} trigger. */
    public void stop() {
        fsm_.fire( AppTrigger.stop );
    }
    
    /**
     * Returns the current state of the {@link #fsm_}.
     *
     * @return An {@link AppState} variable.
     */
    public AppState state() {
        return fsm_.getState();
    }
    
    /** States enum. */
    public enum AppState
    {
        ON, SEARCHING, FAILURE, TIMEOUT, OFF
    }
    
    /** Triggers enum. */
    private enum AppTrigger
    {
        start, search, sleep, fail, stop
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#SEARCHING} -> requests a new
     * location by using the {@link #location_provider_}.
     *
     * @see <a href="https://developer.android.com/training/location/retrieve-current.html">Getting the Last Known Location</a>
     */
    private class SearchEntry implements Action
    {
        @SuppressLint("MissingPermission")
        @Override
        public void doIt() {
            final LocationRequest location_request = new LocationRequest();
            location_request.setInterval( 500 );
            location_request.setPriority( LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY );
    
            final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(
                    location_request );
            
            final SettingsClient                 client = LocationServices.getSettingsClient( context_ );
            final Task<LocationSettingsResponse> task   = client.checkLocationSettings( builder.build() );
            
            location_provider_ = LocationServices.getFusedLocationProviderClient( context_ );
            location_callback_ = new LocationCallback()
            {
                @SuppressWarnings("ConstantConditions")
                public void onLocationResult( LocationResult location_result ) {
                    location_provider_.removeLocationUpdates( location_callback_ );
                    fsm_.fire( AppTrigger.start );
                }
            };
    
            // it's fine to suppress warning here since this isn't called if we're not granted
            task.addOnSuccessListener( locationSettingsResponse -> {
                location_provider_.requestLocationUpdates( location_request,
                                                           location_callback_,
                                                           null );
    
                timeout_handler_.postDelayed( timeout_runnable_, Constants.INTERVAL_SLOW );
            } );
    
            task.addOnFailureListener( e -> {
                final int status_code = ((ApiException) e).getStatusCode();
                switch (status_code) {
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            ResolvableApiException resolvable = (ResolvableApiException) e;
                            resolvable.startResolutionForResult( activity_,
                                                                 Constants.GPS_RESOLVE_CODE );
                        } catch (IntentSender.SendIntentException sie) { sie.printStackTrace(); }
                        break;
                }
        
                fsm_.fire( AppTrigger.fail );
            } );
    
            SharedBuffer.dispatcher().dispatch_event( new GPSEvent( GPSEvent.SEARCH ) );
        }
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#ON}
     * -> dispatches a new {@link GPSEvent#START} event.
     */
    private class StartEntry implements Action
    {
    
        @Override
        public void doIt() {
            timeout_handler_.removeCallbacks( timeout_runnable_ );
            SharedBuffer.dispatcher().dispatch_event( new GPSEvent( GPSEvent.START ) );
        }
    }
    
    /**
     * Action taken before the state machine enter {@link AppState#OFF}
     * -> {@link #destroy_internals()} and dispatches a new {@link GPSEvent#STOP}.
     */
    private class StopEntry implements Action
    {
    
        @Override
        public void doIt() {
            destroy_internals();
            SharedBuffer.dispatcher().dispatch_event( new GPSEvent( GPSEvent.STOP ) );
        }
    }
    
    /**
     * Action taken before the state machine enter {@link AppState#FAILURE}
     * -> {@link #destroy_internals()} and dispatches a new {@link GPSEvent#STOP}.
     */
    private class FailEntry implements Action
    {
    
        @Override
        public void doIt() {
            destroy_internals();
            SharedBuffer.dispatcher().dispatch_event( new GPSEvent( GPSEvent.FAILURE ) );
        }
    }
    
    /**
     * Action taken before the state machine enter {@link AppState#OFF}
     * -> {@link #destroy_internals()} and dispatches a new {@link GPSEvent#STOP}.
     */
    private class SleepEntry implements Action
    {
    
        @Override
        public void doIt() {
            destroy_internals();
            SharedBuffer.dispatcher().dispatch_event( new GPSEvent( GPSEvent.TIMEOUT ) );
        }
    }
}
