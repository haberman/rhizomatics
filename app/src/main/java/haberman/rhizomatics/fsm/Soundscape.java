package haberman.rhizomatics.fsm;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.delegates.Action;

import haberman.rhizomatics.R;
import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.data.SharedBuffer;
import timber.log.Timber;

import static haberman.rhizomatics.fsm.AudioPlayer.AppState.BUFFERING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.COMPLETED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.IDLE;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PAUSED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PENDING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PLAYING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PREPARING;
import static haberman.rhizomatics.fsm.AudioPlayer.AudioType.SOUNDSCAPE;

/**
 * The FSM driving the map related updates for a soundscape playback.
 *
 * @author haberman
 */
public class Soundscape extends AudioPlayer
{
    /**
     * Thread tracked the elapsed amount of silence.
     */
    @NonNull
    private final Handler  silence_handler_;
    /**
     * Silence thread action's interface.
     */
    @NonNull
    private final Runnable silence_runnable_;
    
    /**
     * The elpased duration of silence (in seconds).
     */
    private short silence_duration_;
    
    /**
     * Constructor.
     *
     * @param context Application {@code Context}
     */
    public Soundscape( @NonNull final Context context ) {
        super( context, SOUNDSCAPE );
    
        silence_handler_ = new Handler();
        silence_runnable_ = new Runnable()
        {
            @Override
            public void run() {
                silence_duration_ += Constants.INTERVAL_SLOW;
                if (silence_duration_ >= Constants.TIMEOUT_LANDSCAPE_START) {
                    silence_duration_ = 0;
                    fsm_.fire( AppTrigger.play );
                } else {
                    silence_handler_.postDelayed( silence_runnable_, Constants.INTERVAL_SLOW );
                }
            }
        };
    
        fsm_config_.configure( IDLE )
                   .onEntry( new IdleEntry() )
                   .permit( AppTrigger.await, PENDING )  // when the audio state is muted
                   .permit( AppTrigger.play, PREPARING ); // when we skip a track being played
        
        fsm_config_.configure( PENDING )
                   .onEntry( new PendingEntry() )
                   .permitDynamic( AppTrigger.play, () -> prepared_ ? PLAYING : PREPARING )
                   .permit( AppTrigger.destroy, IDLE );
    
        fsm_config_.configure( PLAYING )
                   .onEntry( new PlayEntry() )
                   .permit( AppTrigger.buffer, BUFFERING )
                   .permit( AppTrigger.pause, PAUSED )
                   .permit( AppTrigger.complete, COMPLETED )
                   .permit( AppTrigger.destroy, IDLE );
    
        fsm_ = new StateMachine<>( IDLE, fsm_config_ );
        fsm_.onUnhandledTrigger( ( arg1, arg2 ) -> Timber.i( "unhandled  trigger %s from state %s ", arg2, fsm_.getState() ) );
    }
    
    /** {@link AppTrigger#play} trigger. */
    public void play_() { fsm_.fire( AppTrigger.play ); }
    
    /**
     * Given {@link Constants#AREA_TRACKS_AMOUNT}, returns a random track.
     *
     * @return An {@code Uri} instance.
     */
    @Override
    protected final Uri select_track() {
        final short track_number = (short) (Math.floor( Math.random() *
                                                        Constants.AREA_TRACKS_AMOUNT ) + 1);
        return Uri.parse( context_.getString(
                R.string.url_remote ) + "area/soundscape_" + track_number + ".ogg" );
    }
    
    /** Returns the actual fsm's {@link AppState}. */
    public AppState state() {
        return fsm_.getState();
    }
    
    /** {@link AppTrigger#await} trigger. */
    public void await() {
        if (fsm_.canFire( AppTrigger.await )) { fsm_.fire( AppTrigger.await ); }
    }
    
    /** Requests the next random track of the soundscape. */
    public void next() {
        if (fsm_.canFire( AppTrigger.complete )) { fsm_.fire( AppTrigger.complete ); }
        
        fsm_.fire( AppTrigger.play );
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#IDLE}
     * -> removes every callbacks, nullify {@link #player_} and sets the application wide audio state to {@link AudioPlayer.AppState#IDLE}.
     */
    class IdleEntry implements Action
    {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void doIt() {
            silence_handler_.removeCallbacks( silence_runnable_ );
            silence_duration_ = 0;
            
            if (player_ != null) {
                if (!SharedBuffer.audio_state().equals( COMPLETED )) {
                    fade_out_( AppState.IDLE, Constants.DURATION_FADING_SLOW, false );
                } else {
                    clear_( IDLE, false );
                }
            }
        }
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#PENDING}
     * -> starts the {@link #silence_runnable_}.
     */
    class PendingEntry implements Action
    {
        @Override
        public void doIt() {
            share_( PENDING, null );
            silence_handler_.postDelayed( silence_runnable_, Constants.INTERVAL_SLOW );
        }
    }
    
}
