package haberman.rhizomatics.gl.drawers;

import android.os.Bundle;
import android.support.annotation.ColorInt;

/**
 * A color based extension of {@link IDrawer}.
 *
 * @author haberman
 */

public interface IColorDrawer extends IDrawer
{
    /**
     * Where drawing internals should be initialized.
     *
     * @param args  Arguments used to control the drawing in the form of {@code Bundle}
     * @param color Resource id of a color.
     */
    void setup( final Bundle args, @ColorInt int color );
}
