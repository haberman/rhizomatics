package haberman.rhizomatics.map;

import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;

import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import haberman.rhizomatics.utils.GeoUtils;

/**
 * {@link Overlay}'s child class used for raw polygon's points.
 *
 * @author haberman
 */

public class OverlayPolygon extends Overlay
{
    
    public OverlayPolygon( final PlaceEntity place ) {
        super( place );
    }
    
    /**
     * Computes rings with locations found on the {@link #place_} points.
     */
    @Override
    protected void compute_rings() {
        super.compute_rings();
    
        final float growth_inc = 1f / (float) Constants.RINGS_AMOUNT;
        float       growth     = 1f;
        final int   s          = place_.points().size();
    
        for (short i = Constants.RINGS_AMOUNT; i > 0; --i) {
            ArrayList<LatLng> ring_points = new ArrayList<>( s );
        
            for (short j = 0; j < s; j++) {
                ring_points.add( GeoUtils.interpolate( place_.center(),
                                                       place_.points().get( j ),
                                                       growth ) );
            }
            
            PolyDrawer ring = new PolyDrawer( ring_points, null,
                                              fill_gradient_.get_color_at( growth ),
                                              stroke_gradient_.get_color_at( growth ),
                                              STROKE_WIDTH, ALPHAS );
        
            rings_.add( ring );
            
            growth -= growth_inc;
        }
        
    }
    
}
