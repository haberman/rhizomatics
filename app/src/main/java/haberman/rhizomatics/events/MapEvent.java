package haberman.rhizomatics.events;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import haberman.rhizomatics.fsm.entities.PlaceEntity;

/**
 * {@link IEvent} implementation for map related events.
 *
 * @author haberman
 */

public class MapEvent implements IEvent
{
    public static final String READY        = "event_map_ready";
    public static final String CLICK        = "event_map_click";
    public static final String CLICK_LONG   = "event_map_click_long";
    public static final String CLICK_MARKER = "event_map_click_marker";
    
    /**
     * The type of event being fired.
     */
    @NonNull
    private final String event_type_;
    
    /**
     * An optional place entity to be attached with this event.
     */
    @Nullable
    private final PlaceEntity place_entity_;
    
    /**
     * Constructor.
     *
     * @param event_type   The event type
     * @param place_entity A (possibly {@code null}) place entity firing the event
     */
    public MapEvent( @NonNull final String event_type, @Nullable PlaceEntity place_entity ) {
        event_type_ = event_type;
        place_entity_ = place_entity;
    }
    
    @NonNull
    @Override
    public final String event_type() {
        return event_type_;
    }
    
    @Nullable
    public final PlaceEntity place_entity() {
        return place_entity_;
    }
}
