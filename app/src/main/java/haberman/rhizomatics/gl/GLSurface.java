package haberman.rhizomatics.gl;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import haberman.rhizomatics.gl.drawers.IDrawer;
import timber.log.Timber;

/**
 * Created by haberman on 3/9/18.
 */

public class GLSurface extends GLSurfaceView
{
    private GLRenderer renderer_;
    
    public GLSurface( Context context ) {
        super( context );
    }
    
    public GLSurface( Context context, AttributeSet attrs ) {
        super( context, attrs );
    }
    
    public void init( @NonNull GLRenderer renderer ) {
        Timber.i( "init GLSurface" );
        renderer_ = renderer;
        
        setEGLContextClientVersion( 2 );
        setEGLConfigChooser( 8, 8, 8, 8, 16, 0 );
        
        getHolder().setFormat( PixelFormat.TRANSPARENT );
        
        setRenderer( renderer_ );
        setRenderMode( GLSurfaceView.RENDERMODE_WHEN_DIRTY );
    }
    
    public GLRenderer renderer() { return renderer_; }
    
    public IDrawer drawer() { return renderer_.drawer(); }
}
