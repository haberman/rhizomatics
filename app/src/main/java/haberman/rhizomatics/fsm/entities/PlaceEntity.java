package haberman.rhizomatics.fsm.entities;

import android.database.Cursor;
import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapbox.mapboxsdk.geometry.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;

import haberman.rhizomatics.map.PolyFactory;
import haberman.rhizomatics.utils.GeoUtils;

/**
 * Class describing every attributes of a {@link haberman.rhizomatics.fsm.Place}.
 *
 * @author haberman
 */
public class PlaceEntity
{
    
    /**
     * Unique identifier.
     */
    private final String id_;
    
    /**
     * The name of this place.
     */
    private final String name_;
    
    /**
     * The author of the text read on this place.
     */
    private final String author_;
    
    /**
     * A {@code JSONObject} describing fill and stroke colors of this place's polygon.
     */
    private final JSONObject theme_;
    
    /**
     * A {@code JSONObject} describing the polygon of the sound audible area.
     */
    private final JSONObject shape_;
    
    /**
     * A list of {@code LatLng} for the base polygon points' locations.
     */
    private ArrayList<LatLng> points_;
    
    /**
     * The centroid of this place.
     */
    private LatLng center_;
    
    /**
     * Private constructor since we always use {@link #from_cursor(Cursor)} to create new instances.
     *
     * @param id     The id
     * @param name   The name
     * @param author The author of the underlying reading
     * @param theme  A {@code JSONObject} for the polygon theme
     * @param shape  A {@code JSONObject} for the polygon shape
     */
    private PlaceEntity( final String id,
                         final String name,
                         final String author,
                         final JSONObject theme,
                         final JSONObject shape ) {
        id_ = id;
        name_ = name;
        author_ = author;
        theme_ = theme;
        shape_ = shape;
    
        try {
            points_ = PolyFactory.decode_shape( shape );
            center_ = GeoUtils.compute_centroid( points_ );
        } catch (JSONException e) {
            e.printStackTrace();
        }
    
    }
    
    /**
     * Creates a new place entity instance from a {@code SQLite} response.
     *
     * @param cursor The {@code Cursor} response
     *
     * @return A new place entity or {@code null} if a {@code JSONException} is encountered.
     */
    @Nullable
    public static PlaceEntity from_cursor( Cursor cursor ) {
        final String id     = cursor.getString( 0 );
        final String name   = cursor.getString( 1 );
        final String author = cursor.getString( 2 );
    
        try {
            final JSONObject theme = (JSONObject) new JSONTokener( cursor.getString( 3 ) )
                    .nextValue();
            final JSONObject shape = (JSONObject) new JSONTokener( cursor.getString( 4 ) )
                    .nextValue();
        
            return new PlaceEntity( id, name, author, theme, shape );
        
        } catch (JSONException e) {
            e.printStackTrace();
        
            return null;
        }
    }
    
    /**
     * Translates an edge of the {@code JSONObject} {@link #theme_} to an array of {@code ColorInt}.
     *
     * @return The list of colors or a default {@code Color.WHITE} if no theme can be found.
     */
    @ColorInt
    @NonNull
    public int[] colors( final String edge ) {
        try {
            final JSONArray fill_theme  = theme_.getJSONArray( edge );
            final int[]     fill_colors = new int[ fill_theme.length() ];
            
            for (int i = 0; i < fill_theme.length(); i++) {
                fill_colors[ i ] = Color.parseColor( fill_theme.getString( i ) );
            }
    
            return fill_colors;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    
        return new int[]{ Color.WHITE };
    }
    
    public final String id() {
        return id_;
    }
    
    public final String name() {
        return name_;
    }
    
    public final String author() {
        return author_;
    }
    
    public final JSONObject shape() {
        return shape_;
    }
    
    public LatLng center() {
        return center_;
    }
    
    public ArrayList<LatLng> points() {
        return points_;
    }
    
    @Override
    public String toString() {
        return "Place " + id_
               + "\n " + author_
               + "\n " + center_.getLatitude() + " / " + center_.getLongitude();
        
    }
}
