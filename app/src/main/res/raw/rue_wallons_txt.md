Promeneur entends-tu la guindaille commencer ? 

Dans l’odeur de la chope et leurs tablards crottés,
De la Rue des Wallons, ils sont montés en hordes,
Bleus, poils, plumes et calottés.

Entonnant les chants du P’tit bitu, du Gaudeamus à Nini Peau de chien
Scandant les hymnes de leurs régionales, 
en Valeureux liégeois ou en Petits jeunes hommes de Binche.

Alors le haut de la ville prend des airs de taverne 
De gargote paillarde où Rabelais raillait le bourgeois.

Dans les salles obscures et moites on allume des bougies, 
On ôte les couvre chefs d’astrakan,
On se salue en partageant une gorgée de bière, les yeux dans les yeux.
Les termes latins surgissent du fond des âges, 
Habes, non habes rythment le bâton du grand-maître,
Les chants s’élèvent plus haut, à mesure que la bière coule.

Les étoiles des calottes brillent encore quand s’éteignent celles du ciel,
Puis on sort de l’ombre au petit matin,
Etourdi et saoul d’une nuit initiatique,
Tels les derniers acteurs d’un rite immuable et secret.
