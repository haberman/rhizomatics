package haberman.rhizomatics.gl.drawers;

import haberman.rhizomatics.data.Gradient;

/**
 * A {@link Gradient} based extension of {@link IDrawer}.
 *
 * @author haberman
 */

public interface IGradientDrawer extends IDrawer
{
    /**
     * Where drawing internals should be initialized.
     *
     * @param gradient    A {@link Gradient} instance to control color pickup
     * @param spread_mode The {@link SpreadMode} used to spread the gradient
     */
    void setup( Gradient gradient, SpreadMode spread_mode );
    
    enum SpreadMode
    {
        RANDOM
    }
}
