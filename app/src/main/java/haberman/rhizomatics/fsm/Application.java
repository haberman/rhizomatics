package haberman.rhizomatics.fsm;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.StateMachineConfig;
import com.github.oxo42.stateless4j.delegates.Action;

import java.util.ArrayList;

import haberman.rhizomatics.utils.ResourceUtils;
import timber.log.Timber;

import static haberman.rhizomatics.data.Constants.PERMISSION_REQUEST_CODE;

/**
 * The Application state machine.
 *
 * @author haberman
 */

public class Application
{
    /**
     * Fullscreen thread {@code Handler}.
     */
    private final Handler fullscreen_handler_;
    
    /**
     * Fullscreen thread {@code Runnable}.
     */
    private final Runnable fullscreen_runnable_;
    
    /**
     * {@link haberman.rhizomatics.MainActivity}'s {@code Context} backref.
     */
    private final Context context_;
    
    /**
     * {@link haberman.rhizomatics.MainActivity} backref.
     */
    private final Activity activity_;
    
    /**
     * {@link haberman.rhizomatics.MainActivity}'s content view.
     */
    private final View view_;
    
    /**
     * The state machine.
     */
    private final StateMachine<AppState, AppTrigger> fsm_;
    
    /**
     * Indicates that the application is granted with {@code ACCESS_FINE_LOCATION} permission.
     */
    private boolean location_ = false;
    
    /**
     * Indicates that the application is granted with {@code RECORD_AUDIO} permission.
     */
    private boolean record_ = false;
    
    /**
     * Constructor
     * Where we register activity backrefs', configure and start the FSM.
     *
     * @param context  Application {@code Context}
     * @param activity Application {@code Activity}
     * @param view     Application {@code View}
     */
    public Application( @NonNull final Context context,
                        @NonNull final Activity activity,
                        @NonNull final View view ) {
        context_ = context;
        activity_ = activity;
        view_ = view;
    
        fullscreen_handler_ = new Handler();
        fullscreen_runnable_ = () -> {
            final int ui_flags = View.SYSTEM_UI_FLAG_LOW_PROFILE
                                 | View.SYSTEM_UI_FLAG_FULLSCREEN
                                 | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                 | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                                 | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                 | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION;
        
            view_.setSystemUiVisibility( ui_flags );
        };
    
        final StateMachineConfig<AppState, AppTrigger> fsm_config = new StateMachineConfig<>();
    
        fsm_config.configure( AppState.IDLE )
                  .permit( AppTrigger.start, AppState.STARTED );
    
        fsm_config.configure( AppState.STARTED )
                  .onEntry( new StartEntry() )
                  .permitDynamic( AppTrigger.run, () -> location_ && record_ ? AppState.RUNNING : AppState.GRANTING );
    
        fsm_config.configure( AppState.GRANTING )
                  .onEntry( new GrantEntry() )
                  .substateOf( AppState.PAUSED )
                  .permit( AppTrigger.run, AppState.RUNNING )
                  .ignore( AppTrigger.pause )
                  .ignore( AppTrigger.start );
    
        fsm_config.configure( AppState.RUNNING )
                  .onEntry( new RunEntry() )
                  .permit( AppTrigger.pause, AppState.PAUSED )
                  .ignore( AppTrigger.start );
    
        fsm_config.configure( AppState.PAUSED )
                  .permit( AppTrigger.start, AppState.STARTED )
                  .permit( AppTrigger.run, AppState.RUNNING )
                  .permit( AppTrigger.stop, AppState.STOPPED );
    
        fsm_config.configure( AppState.STOPPED )
                  .onEntry( new StopEntry() )
                  .permit( AppTrigger.start, AppState.STARTED )
                  .permit( AppTrigger.destroy, AppState.IDLE );
    
        fsm_ = new StateMachine<>( AppState.IDLE, fsm_config );
        fsm_.onUnhandledTrigger( ( arg1, arg2 ) -> Timber.i( "unhandled  trigger: %s", arg2.toString() ) );
    }
    
    /**
     * Sets immersive full screen mode.
     *
     * @see <a href="https://developer.android.com/training/system-ui/immersive.html">Using Immersive Full-Screen Mode</a>
     */
    private void schedule_fullscreen_() {
        fullscreen_handler_.removeCallbacks( fullscreen_runnable_ );
        fullscreen_handler_.postDelayed( fullscreen_runnable_, 200 );
    }
    
    /**
     * Grants the application with required permissions.
     */
    private void grant() {
        final ArrayList<String> missing_permissions = new ArrayList<>();
    
        if (!location_) {
            missing_permissions.add( Manifest.permission.ACCESS_FINE_LOCATION );
        }
    
        if (!record_) {
            missing_permissions.add( Manifest.permission.RECORD_AUDIO );
        }
    
        final String[] required = new String[ missing_permissions.size() ];
        missing_permissions.toArray( required );

        /*
         * FOR EXPLANATIONS

         ArrayList<String> permissions_to_explain = new ArrayList<>();

         for (final String permission : required_permissions) {
         if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
         permissions_to_explain.add(permission);
         }
         }

         if (handler_ != null && permissions_to_explain.size() > 0) {
         handler_.on_explanation_needed(permissions_to_explain);
         }
         */

        /*
      Used to track the {@code ACCESS_FINE_LOCATION} and {@code RECORD_AUDIO} permission requests.
     */
        ActivityCompat.requestPermissions( activity_, required, PERMISSION_REQUEST_CODE );
    }
    
    /**
     * {@link #grant_check()} with boolean response clone.
     *
     * @return {@code true} is both {@link #location_} and {@link #record_} are {@code true}, false
     * otherwise.
     */
    public boolean granted() {
        grant_check();
        return location_ && record_;
    }
    
    /**
     * Sets {@link #location_} and {@link #record_} values by checking that the application is
     * granted with required permissions.
     */
    private void grant_check() {
        location_ = ResourceUtils.granted( context_, Manifest.permission.ACCESS_FINE_LOCATION );
        record_ = ResourceUtils.granted( context_, Manifest.permission.RECORD_AUDIO );
    }
    
    /**
     * {@link AppTrigger#start} trigger.
     */
    public void start() { fsm_.fire( AppTrigger.start ); }
    
    /**
     * {@link AppTrigger#run} trigger.
     */
    public void run() { fsm_.fire( AppTrigger.run ); }
    
    /**
     * {@link AppTrigger#pause} trigger.
     */
    public void pause() { fsm_.fire( AppTrigger.pause ); }
    
    /**
     * {@link AppTrigger#stop} trigger.
     */
    public void stop() { fsm_.fire( AppTrigger.stop ); }
    
    /**
     * {@link AppTrigger#destroy} trigger.
     */
    public void destroy() { fsm_.fire( AppTrigger.destroy ); }
    
    /**
     * Returns the current state of the {@link #fsm_}.
     *
     * @return An {@link AppState} variable.
     */
    public AppState state() {
        return fsm_.getState();
    }
    
    /**
     * States enum.
     */
    public enum AppState
    {
        STARTED, GRANTING, RUNNING, PAUSED, STOPPED, IDLE
    }
    
    /**
     * Triggers enum.
     */
    enum AppTrigger
    {
        start, run, pause, stop, destroy
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#STARTED} -> {@link #grant_check()}
     * and {@link #run()}.
     */
    class StartEntry implements Action
    {
    
        @Override
        public void doIt() {
            grant_check();
            Application.this.run();
        }
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#GRANTING} -> {@link #grant()}.
     */
    class GrantEntry implements Action
    {
        @Override
        public void doIt() {
            grant();
        }
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#RUNNING} -> {@link #schedule_fullscreen_()}.
     */
    class RunEntry implements Action
    {
        @Override
        public void doIt() {
            schedule_fullscreen_();
        }
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#STOPPED} -> nothing yet.
     */
    class StopEntry implements Action
    {
        @Override
        public void doIt() { /* pass */ }
    }
    
}
