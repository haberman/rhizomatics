package haberman.rhizomatics.gl.drawers;

import android.graphics.PointF;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import haberman.rhizomatics.data.Gradient;
import haberman.rhizomatics.fsm.entities.PlaceEntity;
import timber.log.Timber;

/**
 * Created by haberman on 3/13/18.
 */

public class SurroundEcho implements IGradientDrawer
{
    
    @Nullable
    private ArrayList<PlaceEntity> places_ = null;
    
    @Nullable
    private ArrayList<PointF> points_ = null;
    
    private short amount_ = 3;
    private float scale_  = .5f;
    
    @Nullable
    private Gradient   gradient_    = null;
    private SpreadMode spread_mode_ = SpreadMode.RANDOM;
    
    private PointF center_;
    
    @Override
    public void setup( Gradient gradient, SpreadMode spread_mode ) {
        gradient_ = gradient;
        spread_mode_ = spread_mode;
    }
    
    @Override
    public void update( final Bundle args ) {
//        assign_bundle_( args );
    }

//    private void assign_bundle_( final Bundle args ) {
//        final Projection projection = SharedBuffer.map().getProjection();
//
//        center_ = projection.toScreenLocation( SharedBuffer.location() );
//        scale_ = args.getFloat( "scale", scale_ );
//        amount_ = args.getShort( "amount", amount_ );
//
//        places_ = DataManager.nearest_place_list( SharedBuffer.location(),
//                                                  SharedBuffer.manager().all_places(),
//                                                  amount_ );
//    }
    
    @Override
    public void draw() {
        Timber.i( "should draw echo" );
    }
    
    public void set_gradient( final Gradient gradient ) { gradient_ = gradient; }
    
    public void set_gradient_mode( final SpreadMode spread_mode ) { spread_mode_ = spread_mode; }
    
}
