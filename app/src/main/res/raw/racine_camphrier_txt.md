Il aura fallu près de six siècles sur terre, peut être autant dans la mer pour qu’une racine, un simple morceau de bois, une sculpture végétale vienne à quitter l’orient japonais pour Les Bruyères. Passant, regarde au sol, son histoire est écrite !

A bâbord, les résidents sont chez eux, dans cette champignonnière urbanistique, dans cette Louvain-la-Neuve qui est née d’un champ. A tribord, on voit un îlot de verdure au cœur des eaux. Il rappelle l’île où naquit l’arbre dont la racine est devant toi ! Sacrée, c’est presque une relique que tu vois : forte, puissante, depuis des siècles elle brave les eaux du déluge, la pluie et Dieu sait s’il en tombe en Belgique. 
Cette racine n’est pas neuve, comme la ville d’ailleurs, Louvain la nouvelle, arrachée au sol des Flandres presque malgré elle. Zébrée de trous, de creux, de cavités… des ferrures arriment cette enfant de la terre, vieille déjà.

Paradoxe, qu’est-ce qu’une racine hors de terre ? une racine sans racines ! Est-ce un objet ? un artifice réifié ? un être qu’on muséalise ? une fossile ? un faux ? S’il était resté japonais, se serait-il vivifié ? Hors de son sol, la ville a poussé comme ces trésors des prés que l’on trouve en été, dans les recoins humides. Si nous étions restés en Flandre, s’il n’y avait pas eu deux Louvain… rien, cessons les suppositions et venons-en aux faits.

Nous avons une racine, centenaire, millénaire peut-être, qui échoua à Louvain-la-Neuve, dont la fondation n’était pas prévue. C’est l’histoire d’une université qui migra de Leuven à Louvain : la cause ? une banale histoire de langue. On aurait pu en rester là, mais voyez-vous, il a fallu qu’on ne fasse pas comme les autres.

Ailleurs, on creuse pour construire, ici on comble un vallon : comme si on avait voulu rejoindre les collines, seules limites à une ville qui s’accroît exponentiellement. Qu’est-ce qu’une ville surélevée ? une ville sans fondements ? Et si les racines étaient ailleurs ? hors de terre, en plein jour, là où sont les branches et les fruits. 
La ville est comme ce camphrier que tu vois, elle a voyagé, elle a vécu, elle se métamorphose. Ses racines sont le monde, c’est une université, une ville qui tient de l’universel. Sans frontières, sans barrières, la ville change comme ce vénérable bout de bois. L’être est devenir, il accompagne son temps.

Il est écrit que quand on a vu le camphrier, il faut partir. Approche tes narines de la racine, sens l’odeur, c’est une invitation au voyage, là on se voit enfant, on prend son panier, on sort le matin pour la cueillette et on s’émerveille.

On est à Louvain-la-Neuve, près d’un lac. Une ville d’esprit qui est née des champs : écoutons-la.
