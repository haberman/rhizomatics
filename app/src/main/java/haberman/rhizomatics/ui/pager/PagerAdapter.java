package haberman.rhizomatics.ui.pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Fragment that adapts an input array of paragraphs into a stack of {@code ViewPager}'s.
 *
 * @author haberman
 */

public class PagerAdapter extends FragmentStatePagerAdapter
{
    /** The list of paragraphs to adapt. */
    @Nullable
    private final ArrayList<String> paragraphs_;
    
    /**
     * Constructor.
     *
     * @param fm         A {@code FragmentManager} instance to construct super {@code PageAdapter}
     * @param paragraphs An {@code ArrayList} of {@code String}s
     */
    public PagerAdapter( final FragmentManager fm, @Nullable final ArrayList<String> paragraphs ) {
        super( fm );
        paragraphs_ = paragraphs;
    }
    
    /**
     * Binds paragraphs to the pager interactions.
     *
     * @param position The actual position within the pagination
     *
     * @return A Fragment for the current page.
     */
    @Override
    public Fragment getItem( int position ) {
        final PagerFragment pager_fragment = new PagerFragment();
    
        final Bundle args = new Bundle();
        args.putInt( "page_current", position + 1 );
        assert paragraphs_ != null;
        args.putInt( "page_total", paragraphs_.size() );
        args.putString( "content", paragraphs_.get( position ) );
        pager_fragment.setArguments( args );
    
        return pager_fragment;
    }
    
    /**
     * Return the amount of paragraphs this pager contains.
     *
     * @return An integer
     */
    @Override
    public int getCount() {
        assert paragraphs_ != null;
        return paragraphs_.size();
    }
}
