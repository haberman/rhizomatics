Perché au sommet de sa tour,
Irradiant la contrée de son amour,
Le grand œil couve de son regard
Cette cité à l’air souvent hagard,
Attentif à ses moindres battements,
Observant parfois avec effarement,
La multiplicité de ses évolutions
Croissant à la vitesse d’un champignon. 

Passant, laisse-moi te conter cette histoire ancrée dans les mémoires, celle de Saint-François, édifice bâti au bord du précipice. Attention où tu mets les pieds, car si ton âme est celle d’un aventurier, partout règne le danger. Tandis que la cloche retentit, nous guidant tel un phare dans la nuit, observe, tapis dans l’ombre, sur ce chemin céleste, la silhouette sombre, surnommée le détraqueur d’après les moldus, pardon mordus d’Harry Potter. Le changement des générations conduisant à la formation de nouvelles appellations. Sous ces dehors impénétrables, cette église se révèle abordable.

Gravis les marches de la montagne du destin, n’aie crainte d’entrer dans l’antre du divin. En hauteur, trône un instrument adoucissant le cœur des gens. Notre orgue ne retentit point uniquement pour la morgue, il est le centre névralgique des épopées lyriques. Lieu d’émoi et de foi catholique pour les uns, emblème mythique de romans épiques ou fantastiques pour les autres, elle est avant tout un endroit de rassemblement pour les étudiants. Nombreux sont les sceptiques, mais la vocation catholique reste enracinée dans l’identité de notre université.

Les baptêmes ici sont légion, néanmoins ils n’ont pas toujours la même signification. Selon la légende, l’attrait des étudiants louvanistes pour la bière n’a rien de mystique. Il parait que saint François produit en secret depuis des siècles, non pas de l’eau bénite mais au contraire une production digne des moines trappistes. Au sein d’un paysage labyrinthique, ce mirage esthétique surprend par l’aventure de son architecture, seul le langage des mages ayant pu ériger une telle beauté, conférant à cet établissement un panache éblouissant.
