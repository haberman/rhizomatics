package haberman.rhizomatics.map;

import android.graphics.Color;
import android.support.annotation.ColorInt;
import android.support.annotation.Nullable;

import com.mapbox.mapboxsdk.annotations.Polygon;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;

import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.utils.GeoUtils;

/**
 * Since we can't set the stroke width of a polygon (<a href="https://github.com/mapbox/mapbox-gl-js/issues/4087">issue #4087</a>),
 * this container stores the drawing of an area as both a {@code Polyline} and a {@code Polygon}.
 * Also contains different public method for quick styling updates.
 *
 * @author haberman
 */

class PolyDrawer
{
    
    /** Points describing this {@code PolyDrawer}. */
    private final ArrayList<LatLng> points_;
    
    /** {@code ColorInt} value for the (for both {@link #polygon_} drawing. */
    @ColorInt
    private final int fill_color_;
    
    /** {@code ColorInt} value for the {@link #polyline_} drawing. */
    @ColorInt
    private final int stroke_color_;
    
    /** Width of the {@link #polyline_} stroke. */
    private final float stroke_width_;
    
    /**
     * An array of two floats which will be factor of polygon's and polyline's transparency (in this
     * order), when set by calling the {@link #set_alpha_(float)} method.
     */
    private final float[] alphas_;
    
    /** An internal alpha value to ease opacity. */
    private float alpha_ = 0f;
    
    /** Points describing eventual holes. */
    @Nullable
    private ArrayList<LatLng> holes_;
    
    /** Mapbox polygon instance. */
    @Nullable
    private Polygon polygon_;
    
    /** Mapbox polyline instance. */
    @Nullable
    private Polyline polyline_;
    
    /** Indicates that the instance is actually attache to the map. */
    private boolean attached_ = false;
    
    /**
     * Constructor.
     *
     * @param points       An {@code ArrayList} of {@code LatLng} locations describing the shape of this
     *                     {@code PolyDrawer}
     * @param holes        A list of {@code LatLng} describing eventual holes in the base polygon
     * @param fill_color   The fill {@code ColorInt} of this shape
     * @param stroke_color The fill {@code ColorInt} of this shape
     * @param alphas       An array of alpha's factors for both {@link #polygon_} and {@link #polyline_}
     */
    PolyDrawer( final ArrayList<LatLng> points,
                @Nullable final ArrayList<LatLng> holes,
                @ColorInt int fill_color,
                @ColorInt int stroke_color,
                final float stroke_width,
                float[] alphas ) {
        points_ = points;
        holes_ = holes;
        fill_color_ = fill_color;
        stroke_color_ = stroke_color;
        stroke_width_ = stroke_width;
        alphas_ = alphas;
    }
    
    /** Attaches the drawer to the map. */
    public void on() {
        if (attached_) { return; }
        
        PolygonOptions polygon = new PolygonOptions().addAll( points_ );
        if (holes_ != null) { polygon.addHole( holes_ ); }
        
        polygon_ = SharedBuffer.map().addPolygon( polygon );
        
        PolylineOptions polyline = new PolylineOptions().addAll( points_( true ) );
        polyline_ = SharedBuffer.map().addPolyline( polyline );
        
        apply_style_();
        attached_ = true;
    }
    
    /**
     * Gets an {@code ArrayList} of locations of this {@code PolyDrawer}.
     *
     * @param close Should we close this polygon ?
     *
     * @return This polygon's shape as an array of {@code LatLng}.
     * @see GeoUtils
     */
    private ArrayList<LatLng> points_( final boolean close ) {
        if (close) {
            return GeoUtils.close_poly( points_ );
        } else {
            return points_;
        }
    }
    
    /** Applies the default style. */
    @SuppressWarnings("ConstantConditions")
    private void apply_style_() {
        polygon_.setFillColor( fill_color_ );
        polygon_.setStrokeColor( Color.TRANSPARENT );
        polygon_.setAlpha( 0 );
        
        polyline_.setColor( stroke_color_ );
        polyline_.setWidth( stroke_width_ );
        polyline_.setAlpha( 0 );
    }
    
    /** Removes the drawer from the map. */
    @SuppressWarnings("ConstantConditions")
    void off_() {
        if (!attached_) { return; }
        
        SharedBuffer.map().removePolygon( polygon_ );
        SharedBuffer.map().removePolyline( polyline_ );
        
        polyline_ = null;
        polygon_ = null;
        
        attached_ = false;
    }
    
    /**
     * Sets the alpha value of both {@link #polyline_} & {@link #polygon_}.
     *
     * @param alpha the alpha value (between 0 and 1)
     */
    @SuppressWarnings("ConstantConditions")
    void set_alpha_( float alpha ) {
        alpha = Math.min( alpha, 1 );
        alpha_ += (alpha - alpha_) * .92f;
        
        if (polygon_ == null || polyline_ == null) { return; }
        
        polygon_.setAlpha( alphas_[ 0 ] * alpha );
        polyline_.setAlpha( alphas_[ 1 ] * alpha );
        
    }
}
