package haberman.rhizomatics.fsm;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.audiofx.Visualizer;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.github.oxo42.stateless4j.StateMachine;
import com.github.oxo42.stateless4j.StateMachineConfig;
import com.github.oxo42.stateless4j.delegates.Action;
import com.github.oxo42.stateless4j.delegates.FuncBoolean;

import java.io.IOException;
import java.util.Objects;

import haberman.rhizomatics.data.Constants;
import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.events.AudioEvent;
import haberman.rhizomatics.events.SoundEvent;
import haberman.rhizomatics.ui.anim.ValueAnimation;

import static haberman.rhizomatics.fsm.AudioPlayer.AppState.BUFFERING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.COMPLETED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.IDLE;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PAUSED;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PLAYING;
import static haberman.rhizomatics.fsm.AudioPlayer.AppState.PREPARING;


/**
 * Base state machine for audio playback.
 *
 * @author haberman
 */

public class AudioPlayer implements MediaPlayer.OnPreparedListener,
                                    MediaPlayer.OnCompletionListener,
                                    MediaPlayer.OnBufferingUpdateListener
{
    
    /** Threaded audio buffering. */
    @NonNull
    final Handler buffer_handler_;
    
    /** Action's taken on the buffering thread. */
    @NonNull
    final Runnable buffer_runnable_;
    
    /** Threaded peak rms measurement. */
    @NonNull
    final Handler peakrms_handler_;
    
    /** Action's taken on the peak rms thread. */
    @NonNull
    final Runnable peakrms_runnable_;
    
    /** {@link haberman.rhizomatics.MainActivity}'s {@code Context} backref. */
    final Context context_;
    
    /** The state machine config. */
    final StateMachineConfig<AppState, AppTrigger> fsm_config_;
    
    /** The audio type handled by this player. */
    @NonNull
    private final AudioType audio_type_;
    
    /** Animator used to fade audio volume. */
    @NonNull
    private final ValueAnimation.Builder fader_;
    
    /** The player. */
    @Nullable
    MediaPlayer player_;
    
    /** An {@code AudioManager} to out of which we can extract amplitude. */
    @Nullable
    private Visualizer sound_visualizer_;
    
    /** Indicates that a sound has been prepared and is ready to play_. */
    boolean prepared_ = false;
    
    /** Indicates that the player expects a queue to be consumed whenever shared audio state allows it. */
    boolean queue_ = false;
    
    /** The state machine. */
    StateMachine<AppState, AppTrigger> fsm_;
    
    /** Keeps track of the fading states. */
    private boolean fading_ = false;
    
    /** An {@code Uri} pointing to an audio file. */
    @Nullable
    private Uri track_ = null;
    
    /** The fraction of buffered audio time. */
    private float buffer_progress_ = 0;
    
    /**
     * Constructor.
     *
     * @param context    A {@code context}'s activity backref
     * @param audio_type The {@link AudioType} this player behaves around
     */
    AudioPlayer( @NonNull final Context context, @NonNull final AudioType audio_type ) {
        context_ = context;
        audio_type_ = audio_type;
        
        buffer_handler_ = new Handler();
        buffer_runnable_ = new Runnable()
        {
            @Override
            public void run() {
                final boolean buffered = buffered_ms() > Constants.DURATION_MIN_BUFFERING;
                
                if (buffered && !fsm_.getState().equals( PLAYING )) {
                    play_();
                } else if (!buffered && !SharedBuffer.audio_state().equals( BUFFERING )) {
                    buffer_();
                }
                
                if (!buffered) {
                    buffer_handler_.postDelayed( buffer_runnable_, Constants.INTERVAL_FAST );
                }
            }
        };
        
        peakrms_handler_ = new Handler();
        peakrms_runnable_ = new Runnable()
        {
            @Override
            public void run() {
                assert sound_visualizer_ != null;
                
                byte[] fft = new byte[ sound_visualizer_.getCaptureSize() ];
                sound_visualizer_.getFft( fft );
                
                final int inc      = (fft.length / 2) / (int) Constants.RINGS_AMOUNT;
                float[]   spectrum = new float[ Constants.RINGS_AMOUNT ];
                
                for (int i = 0; i < Constants.RINGS_AMOUNT; ++i) {
                    final int idx = i * inc;
                    
                    final double db_left  = Math.pow( fft[ idx * 2 ], 2 );
                    final double db_right = Math.pow( fft[ idx * 2 + 1 ], 2 );
                    
                    float db = (float) Math.sqrt( db_left + db_right ) / 10;
                    spectrum[ i ] = db;
                }

//                final Visualizer.MeasurementPeakRms measurement = new Visualizer.MeasurementPeakRms();
//
//                sound_visualizer_.getMeasurementPeakRms( measurement );
//                final float vol = MathUtils.map_number( measurement.mRms, -9600f, 0f, 0f, 1f );
                
                SharedBuffer.dispatcher()
                            .dispatch_event( new SoundEvent( SoundEvent.VOLUME, spectrum ) );
                
                peakrms_handler_.postDelayed( peakrms_runnable_, Constants.INTERVAL_FAST );
            }
        };
        
        fader_ = new ValueAnimation.Builder()
                .set_interpolator( new AccelerateDecelerateInterpolator() )
                .set_repeat( android.animation.ValueAnimator.INFINITE, 0 );
        
        fsm_config_ = new StateMachineConfig<>();
        fsm_config_.configure( PREPARING )
                   .onEntry( new PrepareEntry() )
                   .permit( AppTrigger.destroy, IDLE )
                   .permitIf( AppTrigger.play, PLAYING, new PlayGuard() );
        
        fsm_config_.configure( BUFFERING )
                   .onEntry( new BufferEntry() )
                   .permit( AppTrigger.play, PLAYING )
                   .permit( AppTrigger.destroy, IDLE );
        
        fsm_config_.configure( PAUSED )
                   .onEntry( new PauseEntry() )
                   .permit( AppTrigger.play, PLAYING )
                   .permit( AppTrigger.destroy, IDLE );
        
        fsm_config_.configure( AppState.COMPLETED )
                   .onEntry( new CompleteEntry() )
                   .permit( AppTrigger.play, PREPARING );
    }
    
    /**
     * Converts the {@link #buffer_progress_} to the amount of ms it represents on the buffering track.
     *
     * @return An integer
     */
    private int buffered_ms() {
        if (player_ == null) { return 0; }
        return Math.round( buffer_progress_ * player_.getDuration() );
    }
    
    /** Private {@link AppTrigger#play} trigger. */
    void play_() { fsm_.fire( AppTrigger.play ); }
    
    /** Start or stops the {@link #sound_visualizer_}. */
    private void visualize_( final boolean value ) {
        if (value) {
            assert player_ != null;
            
            if (sound_visualizer_ == null) {
                sound_visualizer_ = new Visualizer( player_.getAudioSessionId() );
            }
            
            sound_visualizer_.setMeasurementMode( Visualizer.MEASUREMENT_MODE_PEAK_RMS );
            sound_visualizer_.setEnabled( true );
            
            peakrms_handler_.postDelayed( peakrms_runnable_, Constants.INTERVAL_FAST );
        } else if (sound_visualizer_ != null && sound_visualizer_.getEnabled()) {
            peakrms_handler_.removeCallbacks( peakrms_runnable_ );
            
            sound_visualizer_.setEnabled( false );
            sound_visualizer_.release();
            sound_visualizer_ = null;
        }
    }
    
    /** Private {@link AppTrigger#buffer} trigger. */
    private void buffer_() {
        fsm_.fire( AppTrigger.buffer );
    }
    
    /**
     * Fades the audio in.
     *
     * @param duration Duration of the effect
     */
    private void fade_in( final long duration ) {
        fading_ = true;
        visualize_( true );
        
        if (queue_) { queue_ = false; }
        
        fader_.set_count( 0.f, Constants.VOLUME_GAIN, 6 )
              .set_duration( duration );
        
        final ValueAnimation fader = fader_.build();
        fader.execute( new ValueAnimation.AnimateListener()
        {
            @Override
            public void on_animate_update( float value ) {
                assert player_ != null;
                player_.setVolume( value, value );
            }
            
            @Override
            public void on_animate_repeat() { /* pass */ }
            
            @Override
            public void on_animate_end() { fading_ = false; }
        } );
        
        assert player_ != null;
        player_.setOnCompletionListener( AudioPlayer.this );
        player_.start();
        
        share_( PLAYING, AudioEvent.PLAY );
    }
    
    
    /**
     * Fades audio out.
     * This will immediately {@link #share_(AppState, String)} but only {@link #clear_(AppState, boolean)}
     * the player once volume has faded away.
     *
     * @param target_state The target application state
     * @param duration     Duration of the effect
     * @param share_on_end {@link #share_(AppState, String)} state when faded out
     */
    void fade_out_( final AppState target_state, final long duration, final boolean share_on_end ) {
        fading_ = true;
        fader_.set_count( Constants.VOLUME_GAIN, 0.f, 6 )
              .set_duration( duration );
        
        String target_event = null;
        switch (target_state) {
            case PAUSED:
                target_event = AudioEvent.PAUSE;
                break;
            case IDLE:
                target_event = AudioEvent.DESTROY;
                break;
        }
        
        if (!share_on_end) { share_( target_state, target_event ); }
        
        final String         final_event = target_event; // clone target as final to be accessible on animation end
        final ValueAnimation fader       = fader_.build();
        fader.execute( new ValueAnimation.AnimateListener()
        {
            @Override
            public void on_animate_update( float value ) {
                assert player_ != null;
                player_.setVolume( value, value );
            }
            
            @Override
            public void on_animate_repeat() { /* pass */ }
            
            @Override
            public void on_animate_end() {
                switch (target_state) {
                    case PAUSED:
                        assert player_ != null;
                        player_.pause();
                        break;
                    case IDLE:
                        // fading_ is used to track the event of an early cancel so we prevent messing the mediaplayer around.
                        if (fading_) { clear_( IDLE, false ); }
                        break;
                }
                
                if (Objects.equals( SharedBuffer.audio_type(),
                                    AudioType.READING )) { visualize_( false ); }
                fading_ = false;
                if (share_on_end) { share_( target_state, final_event ); }
            }
        } );
    }
    
    /**
     * Sets shared audio type & state and eventually shares changes on the {@link SharedBuffer#dispatcher()}.
     *
     * @param state The state to share
     * @param event The event that triggered the state's change
     */
    void share_( @NonNull final AppState state,
                 @Nullable final String event ) {
        SharedBuffer.set_audio_type( audio_type_ );
        SharedBuffer.set_audio_state( state );
        
        if (event == null) { return; }
        
        final AudioEvent ae = new AudioEvent( event, audio_type_ );
        SharedBuffer.dispatcher().dispatch_event( ae );
    }
    
    /**
     * Shortcut to remove listeners and clears the player according some {@link AppState} parameter.
     *
     * @param target_state      The state the {@link #fsm_} should reach after clearing resources
     * @param take_shared_focus The {@code target_state} will also become the {@link SharedBuffer#audio_state_}
     */
    void clear_( final AppState target_state, final boolean take_shared_focus ) {
        assert player_ != null;
        visualize_( false );
        
        player_.setOnPreparedListener( null );
        player_.setOnCompletionListener( null );
        player_.setOnBufferingUpdateListener( null );
        
        prepared_ = false;
        track_ = null;
        
        switch (target_state) {
            case COMPLETED:
                player_.reset();
                break;
            case IDLE:
                player_.release();
                player_ = null;
                break;
        }
        
        if (take_shared_focus) { SharedBuffer.set_audio_state( target_state ); }
    }
    
    /**
     * {@code MediaPlayer.onBufferingUpdate} implementation -> updates {@link #buffer_progress_}
     *
     * @param media_player The {@code MediaPlayer} receiving updates
     * @param percent      The amount of buffered audio in percent
     */
    @Override
    public void onBufferingUpdate( MediaPlayer media_player, int percent ) {
        buffer_progress_ = percent * .01f;
        
        if (buffer_progress_ == 1) {
            assert player_ != null;
            player_.setOnBufferingUpdateListener( null );
        }
    }
    
    /**
     * {@code MediaPlayer.OnCompletionListener} implementation -> {@link SharedBuffer#set_audio_state(AppState)}
     * to {@link AppState#COMPLETED} and notify {@link SharedBuffer#dispatcher()}.
     *
     * @param media_player {@inheritDoc}
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public void onCompletion( MediaPlayer media_player ) {
        if (fading_) {
            clear_( COMPLETED, false );
        } else {
            if (fsm_.canFire( AppTrigger.complete )) { fsm_.fire( AppTrigger.complete );}
            share_( COMPLETED, AudioEvent.COMPLETE );
        }
    }
    
    /**
     * {@code MediaPlayer.OnPreparedListener} implementation -> triggers {@link AppTrigger#play}.
     *
     * @param media_player {@inheritDoc}
     */
    @SuppressWarnings("ConstantConditions")
    @Override
    public void onPrepared( MediaPlayer media_player ) {
        prepared_ = true;
        
        // @dev
//        player_.seekTo( player_.getDuration() - 10000 );
        fsm_.fire( AppTrigger.play );
    }
    
    /**
     * Returns a new track, should be overridden in children classes.
     *
     * @return {@code Uri} track.
     */
    @Nullable
    Uri select_track() {
        return null;
    }
    
    /**
     * Returns the current state of the {@link #fsm_}.
     *
     * @return An {@link AppState} variable.
     */
    public AppState state() { return fsm_.getState(); }
    
    public boolean has_queue() { return queue_; }
    
    /**
     * {@link AppTrigger#pause} trigger -> will either:
     * <ul>
     * <li>drives the machine to {@link AppState#IDLE} if the sound is not playing;</li>
     * <li>drives the machine to {@link AppState#PAUSED} otherwise.</li>
     * </ul>
     */
    public void pause() {
        if (fsm_.getState().equals( PLAYING )) {
            fsm_.fire( AppTrigger.pause );
        } else {
            fsm_.fire( AppTrigger.destroy );
        }
    }
    
    /** {@link AppTrigger#destroy} trigger. */
    public void destroy() {
        if (fsm_.canFire( AppTrigger.destroy )) { fsm_.fire( AppTrigger.destroy ); }
    }
    
    /**
     * Configures a media player with init values.
     *
     * @return A {@code MediaPlayer}.
     */
    private MediaPlayer player() {
        MediaPlayer player = new MediaPlayer();
        player.setAudioStreamType( AudioManager.STREAM_MUSIC );
        player.setVolume( Constants.VOLUME_GAIN, Constants.VOLUME_GAIN );
        return player;
    }
    
    /** An enum listing types' refinements of audio states. */
    public enum AudioType
    {
        READING, SOUNDSCAPE /* TODO: NEARBY */
    }
    
    /** States enum. */
    public enum AppState
    {
        IDLE, PENDING, PREPARING, BUFFERING, PLAYING, PAUSED, COMPLETED
    }
    
    /** Triggers enum. */
    public enum AppTrigger
    {
        await, play, buffer, pause, complete, destroy
    }
    
    /**
     * Action taken when the state machine enters {@link AppState#PREPARING} -> {@link #select_track()}
     * and prepare the {@link #player_}.
     */
    class PrepareEntry implements Action
    {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void doIt() {
            try {
                track_ = select_track();
                buffer_progress_ = 0;
                
                if (player_ == null) {
                    player_ = player();
                }
                
                player_.setOnPreparedListener( AudioPlayer.this );
                player_.setDataSource( context_, track_ );
                player_.prepareAsync();
                
                share_( PREPARING, AudioEvent.PREPARE );
            } catch (IOException io) {
                io.printStackTrace();
            }
        }
    }
    
    /** Action taken before the machine enters {@link AppState#BUFFERING} -> watch for updates. */
    class BufferEntry implements Action
    {
        @Override
        public void doIt() {
            if (buffer_progress_ == 1f) { return; }
            
            if (!SharedBuffer.audio_state().equals( BUFFERING )) {
                share_( BUFFERING, AudioEvent.BUFFER );
            }
            
            assert player_ != null;
            player_.setOnBufferingUpdateListener( AudioPlayer.this );
            buffer_handler_.postDelayed( buffer_runnable_, Constants.INTERVAL_FAST );
        }
    }
    
    /**
     * Action taken before the state machine enters {@link AppState#PLAYING}, one we have a prepared
     * {@link #player_} -> starts buffering and sets this {@link #audio_type_} to take focus over
     * the shared {@link SharedBuffer#audio_type()}.
     */
    class PlayEntry implements Action
    {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void doIt() {
            if (buffer_progress_ == 0) {
                buffer_();
                return;
            }
            
            fade_in( Constants.DURATION_FADING_FAST );
        }
    }
    
    /** Action taken when the state machine enters {@link AppState#COMPLETED} -> {@link #clear_(AppState, boolean)}. */
    class CompleteEntry implements Action
    {
        @Override
        public void doIt() { clear_( AppState.COMPLETED, true ); }
    }
    
    /** Action taken when the state machine enters {@link AppState#PAUSED} -> pauses the {@link #player_}. */
    class PauseEntry implements Action
    {
        @SuppressWarnings("ConstantConditions")
        @Override
        public void doIt() {
            if (player_ != null) {
                fade_out_( AppState.PAUSED, Constants.DURATION_FADING_FAST, true );
            }
        }
    }
    
    /**
     * Guard that prevents the machine to enters {@link AppState#PLAYING} is the {@link #player_} isn't prepared.
     */
    class PlayGuard implements FuncBoolean
    {
        @Override
        public boolean call() {
            return prepared_;
        }
    }
}
