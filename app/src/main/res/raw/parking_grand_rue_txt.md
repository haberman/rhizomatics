En fait, si j’étais toi, je suivrais les chiques. Par terre, les chewing-gums. 
Parce que tu pourrais manquer l’entrée. On dirait un passage dérobé. À ta gauche ou à ta droite, selon d’où tu viens. 
« Parking Grand-Rue », c’est bien ça. 
« GranDE-Rue », non ? Le « de » pour le « rue », non ?
Et pourquoi du vert sur le panneau ? Et pourquoi pas…
La grille. Une grande grille métallique.
Passage vers le dehors ou le dedans ? Je rentre pour sortir ou je sors pour rentrer ? Une ville délimitée par des grilles. Pas des remparts d’un autre âge, des grilles.

Des grilles, et du pipi. Ça sent le pipi. En même temps, avec ces auréoles douteuses... 
Ce qui est bien, c’est qu’ensuite, en avançant, ils ont préféré le carrelage. Arrêtons-nous. 
Je crois que l’entretien, ici, est plus aisé. D’ailleurs, on s’aperçoit que la quantité de tâches diminue. Ils auraient pu mettre des carreaux bleus partout, ç’aurait été moins salissant que le blanc. Cela dit, qui met sa main sur le mur ? Déjà sur la rampe… alors sur le mur…
Tu hésites à descendre, attends encore un peu. 
« AVEZ-VOUS PAYÉ ? » te demande-t-on.
Et ce vert dans les interstices. Partout on te rappelle que tu es dans le bon, dans le vert. 
Et la musique. Tu as remarqué ? Au moment où tu as passé la grille, tu es entré dans un 1er sas. Un sas de transition, en quelque sorte. Déjà, les bruits de la rue s’atténuent alors que de nouveaux apparaissent. Des sons qui, dans la ville piétonne, sont absents : portières, moteurs, radios.


1 2 3 4 5 6 7 8 9 10 – Première volée de marches. 
2ème sas.
1 2 3 4 5 6 7 8 – Deuxième volée. 
Les voix du dehors se sont éteintes. 
1 2 3 4 5 6 7 8 – Troisième volée.
Désormais, ton seul repère est le mouvement. Des gens montent, d’autres descendent. Toi-même tu avances. En arrivée, en partance. 
Tu passes la porte. Verte, encore.

3ème sas. Tu t’arrêtes.
Le son de la radio est désormais plus lointain. Il laisse place au vrombissement de l’air conditionné et des moteurs. Ponctuellement, on entend des portières, des pneus qui crissent.
Sous ce revêtement gris, sous ce plafond gris, entre ces murs blancs. Tout n’est désormais que passages. Sauf toi. Qui écoutes. Et qui vois. 
Combien de voitures sont-elles garées ? Peut-être as-tu laissé la tienne dans un parking similaire. Comment te retrouver ? As-tu pris note mentalement de tes coordonnées ? Parking Grand-Rue, niveau -1, lettre ? F / E / D / C ? 
Empire de points de repères.
Quelle suspension, quels tuyaux suivras-tu ? Arrivée d’air, d’eau ? 
Empire des lumières.

Tu as remarqué ? Des phares, des néons et, au-dessus de chaque emplacement vide, une petite lampe verte. 
Vas-y, choisis-en une, place-toi dessous et regarde-la. 
Elle t’a remarqué. Elle est passée au rouge.
Maintenant que tu détiens ton espace, tu te sens légitime ; comme observateur, piéton immobile, dans ce lieu transitoire.
Perçois-tu l’impact de ton être ? Au moment du passage du vert au rouge, tous les panneaux disséminés ont été bouleversés : le nombre de places disponibles a changé.

On sait que TU es là.
Même sous une grosse dalle de béton, entre les racines géométriques d’une ville surélevée. Même au milieu des auréoles, des odeurs que l’on fuit. Même là où, habituellement, personne ne reste. Là où  l’on pense à ce que l’on vient de quitter ou à ce que l’on va rejoindre. 
On sait que TU es là. 

Trace éphémère qui, dès que tu la quitteras, se rendra disponible à l’autre. À un autre que toi.
Habité par ce constat, marqué par l’air vicié dans lequel tu es resté, tu auras peut-être la gorge sèche. Piéton ou conducteur, tu comprendras sans doute pourquoi tant de personnes ont désiré une chique. Pourquoi tous ces chewing-gums, en haut, sur le sol.
L’air et puis la trace. Faire glisser de la salive aromatisée dans un conduit irrité et puis laisser une empreinte durable. Subrepticement, marquer son passage dans une ville si changeante. En évolution permanente. 
D’ici, tu peux choisir, tu vas la rejoindre ou la quitter. La maculer ou l’épargner. Remonter, ou descendre. Sortir, sortir.
