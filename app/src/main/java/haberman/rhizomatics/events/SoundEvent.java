package haberman.rhizomatics.events;

import android.support.annotation.NonNull;

import haberman.rhizomatics.data.SharedBuffer;

public class SoundEvent implements IEvent
{
    public static final String VOLUME = "event_sound_volume";
    
    /** The type of event being fired. */
    @NonNull
    private final String event_type_;
    
    private final float[] spectrum_;
    
    /**
     * Constructor.
     * Will also update {@link SharedBuffer#audio_type_} if given.
     *
     * @param event_type The event type
     * @param spectrum   An array of frequency's amplitude..
     */
    public SoundEvent( @NonNull final String event_type, final float[] spectrum ) {
        event_type_ = event_type;
        spectrum_ = spectrum;
    }
    
    @NonNull
    @Override
    public final String event_type() {
        return event_type_;
    }
    
    public final float[] spectrum() { return spectrum_; }
    
}
