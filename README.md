# RHIZOMATICS
## code v12 / data v5

### 0.8.2
- **nouveautés :**
    - Utilisation du volume et de la fréquence sonore de lecture comme facteur d'opacité des zones mêmes.
    - Ajout d'une page faisant mention des crédits.
    - Emploi d'un fond de carte dédié, mise à jour des couleurs de l'interface.

### 0.8.1
- **correctifs :**
    - Mise à jour des dépendances logicielles (Android et Mapbox).
    - Corrections de quelques glitches d'affichage.

### 0.8.0
- **nouveautés :**
    - changement du fragment d'interface ouvrant les interactions avec l'espace de la carte
    -> l'affichage ou non des marqueurs est désormais séparé de l'utilisation ou non du GPS
    (dont les différents modes sont préservés).
    - sauvegarde / restauration de l'état de la carte lorsque l'application passe en arrière-plan ;
    - optimisation générale de l'empreinte de l'application sur les ressources (CPU / RAM).
   
### 0.7.1
- **nouveautés :**
    - ajout d'une icône d'erreur pour la notification de problèmes ;
    - utilisation de nouveaux marqueurs pour la carte, un peu plus gros que les précédents ;
    - ajout d'un fade-out pour les sorties du paysage sonore et dans tous les cas, lorsque l'application passe en arrière-plan;
- **correctifs :**
    - correction du glitch permettant qu'un paysage soit joué en même temps qu'une lecture
    (lorsque la première localisation se trouve dans une zone de lecture);
    - meilleure réactivité du chevron permettant de déployer le mini pager
    (ajout d'un cercle invisible deux fois plus large que l'icône).
    
### 0.7.0 
- **nouveautés :**
    - ajout d'un composant d'interface permettant de consulter le texte des lectures audio sous forme de paragraphes ;
    - changement des icônes du header ;
    - ajout du point d'écoute "Place des sciences / Musée L".
- **correctifs :**
    - corrections de nombreux glitchs, cette version est supposée _bugfree_
    (mais il faudrait quand même la tester plus en profondeur) ;

### 0.6.2
- **correctifs :**
    - corrections de la `0.6.1` ;
    - glitch d'affichage faisant croire qu'un son est en cours de chargement tampon lorsque l'on clique sur un autre marqueur que le son en écoute en mode découverte ;
    - la recherche d'une localisation initiale s'auto-termine si le GPS n'est pas _accroché_ au bout de 20 secondes.
- **nouveautés :**
    - ajout du relief des bâtiments en mode parcours ;
    - internationalisation en langue anglaise ;
    - ajout d'un mode de visualisation employant le compas afin d'orienter la caméra de la carte en mode parcours ;
    - `dev` -> ajout d'un simulateur de trajets.

### 0.6.1
- **nouveautés :**
    - récupération du marqueur précédemment actif en revenant dans l'application ou lors d'une relecture en mode découverte ;
    - ajout d'un bouton skip/next pour enchaîner les pistes du paysage sonore ;
    - ajout d'une instruction STAY_AWAKE afin que l'écran n'entre pas en veille lorsque l'application reçoit le focus utilisateur.
- **correctifs :**
    - bug ne mettant parfois pas en pause le paysage sonore lors de la sortie de l'application ;
    - bug ne lançant pas la lecture du paysage sonore après être sorti d'une zone d'écoute lors de sa lecture ;
    - bug ne faisant pas apparaître le player lors du lancement du paysage sonore ;
    - bug maintenant le player visible et l'écoute du paysage actif en passant du mode parcours au mode découverte ;
    - glitch laissant apparaître d'éventuels polygones actifs en passant du mode parcours au mode découverte ;
    - glitch ne faisant pas s'enchaîner les pistes du paysage sonore en mode parcours ;
    - glitch ne lançant pas la lecture en entrant dans un zone d'écoute alors que le paysage sonore est audible ;
    - glitch rendant les zones d'écoute invisibles lors du passage en mode parcours
    (il était nécessaire de se déplacer pour obtenir au moins un point de localisation supplémentaire, la visibilité est désormais immédiate).

### TODO's
- [ ] prendre en compte l'audio focus du téléphone (appels, enregistrements) ;
- [ ] ré-intégrer un système de notification lors du passage de l'application en arrière-plan afin d'informer d'une lecture en cours.
- [ ] vue tête-haute pour la surimpression de plus amples effet visuels ;
- [ ] implémenter le calcul de la stéréo (déjà codé) en intégrant les nouvelles boucles de composition de Philippe ;
- [ ] ajouter des vibrations dont l'intensité serait liée à la présence 360º de zones d'écoute à proximité
