package haberman.rhizomatics.utils;

import android.graphics.PointF;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.Projection;

import java.util.ArrayList;

import haberman.rhizomatics.data.SharedBuffer;
import haberman.rhizomatics.data.Vector2;


/**
 * Exposes static calculation utilities for basic computational geo(metry/graphy).
 *
 * @author haberman
 */

public class GeoUtils
{
    private static final double RAD_TO_DEG = 180 / Math.PI;
    private static final double DEG_TO_RAD = Math.PI / 180;
    
    /**
     * Computes the centroid of the given locations array.
     *
     * @param locations An ArrayList describing the polygon on which to find the centroid
     *
     * @return The computed LatLng centroid.
     */
    public static LatLng compute_centroid( ArrayList<LatLng> locations ) {
        final int s = locations.size();
    
        double a = 0, f;
        double x = 0, y = 0;
    
        for (int i = 0, j = s - 1; i < s; j = i++) {
            final LatLng p1 = locations.get( i );
            final LatLng p2 = locations.get( j );
        
            f = p1.getLongitude() * p2.getLatitude() - p2.getLongitude() * p1.getLatitude();
            a += f;
        
            x += (p1.getLongitude() + p2.getLongitude()) * f;
            y += (p1.getLatitude() + p2.getLatitude()) * f;
        }
    
        a *= 3f;
    
        return new LatLng( y / a, x / a );
    }
    
    /**
     * Simply create a new {@code ArrayList} that closes the provided one by inserting a clone of its
     * first member at the end.
     *
     * @param polygon The {@code ArrayList} of points to close.
     *
     * @return The polygon as a closed list of locations.
     */
    public static ArrayList<LatLng> close_poly( final ArrayList<LatLng> polygon ) {
        @SuppressWarnings("unchecked") ArrayList<LatLng> points = (ArrayList<LatLng>) polygon
                .clone();
        points.add( new LatLng( polygon.get( 0 ).getLatitude(), polygon.get( 0 ).getLongitude() ) );
    
        return points;
    }
    
    /**
     * Projects a given location to a certain radius along a provided angle.
     *
     * @param location The LatLng point to project
     * @param radius   The amount of projection (expressed as degrees equivalent distance of meters)
     * @param angle    The angle_to of projection
     *
     * @return The projected LatLng instance.
     */
    public static LatLng project_on_circle( LatLng location, double radius, double angle ) {
        final double lat = location.getLatitude() * DEG_TO_RAD;
        final double lng = location.getLongitude() * DEG_TO_RAD;
    
        final double point_lat = Math
                .asin( Math.sin( lat ) * Math.cos( radius ) + Math.cos( lat ) * Math.sin(
                        radius ) * Math.cos( angle ) );
        final double point_lng = lng + Math
                .atan2( Math.sin( angle ) * Math.sin( radius ) * Math.cos( lat ),
                        Math.cos( radius ) - Math.sin( lat ) * Math.sin( lat ) );
    
        return new LatLng( point_lat * RAD_TO_DEG, point_lng * RAD_TO_DEG );
    }
    
    /**
     * Computes an interpolated location given a progress value within a list of other {@code LatLnt}.
     *
     * @param locations_list Locations to interpolate
     * @param progress       Factor of progress (between 0 & 1)
     *
     * @return A new {@code LatLng} for the interpolated projection.
     */
    @Nullable
    public static LatLng interpolate_list( final LatLng[] locations_list, final float progress ) {
        if (locations_list.length == 0 || progress == 1f) {
            return null;
        }
        
        final int list_length = locations_list.length;
        
        final int index_start = (short) MathUtils.map_number( progress, 0, 1f, 0, list_length - 1 );
        final int index_end   = index_start + 1;
        
        final float step_slope = MathUtils.map_number( progress * list_length,
                                                       index_start,
                                                       index_end,
                                                       0,
                                                       1f );
        
        return interpolate( locations_list[ index_start ], locations_list[ index_end ],
                            step_slope );
    }
    
    /**
     * Computes the linear interpolation between 2 {@code LatLng}, by a specified extent.
     *
     * @param start    The first location to anchor the interpolation
     * @param end      The second location used as the projection axis
     * @param progress The factor of interpolation between the 2 locations
     *
     * @return A new {@code LatLng} for the interpolated projection.
     */
    public static LatLng interpolate( final LatLng start, final LatLng end, final float progress ) {
        final Vector2 anchor_v      = latlng_to_vector2_( start ); // circle_v
        final Vector2 destination_v = latlng_to_vector2_( end ); //polygon_v
        final Vector2 point_v = anchor_v.add( Vector2.diff( destination_v, anchor_v ).mult_scalar(
                progress ) );
        
        return new LatLng( point_v.y, point_v.x );
    }
    
    /**
     * Converts a {@code LatLng} to its {@link Vector2} representation.
     *
     * @param location {@code LatLng} instance to convert
     *
     * @return A new corresponding {@link Vector2} point.
     */
    private static Vector2 latlng_to_vector2_( @NonNull final LatLng location ) {
        return new Vector2( location.getLongitude(), location.getLatitude() );
    }
    
    /**
     * Returns the minimum distance between a reference point and a list of other locations.
     *
     * @param reference The reference point to measure the distance from
     * @param locations A list of coordinates
     *
     * @return The maximum distance in meters.
     */
    public static float min_distance( LatLng reference, ArrayList<LatLng> locations ) {
        float distance = 0;
    
        for (LatLng location : locations) {
            distance = (float) Math.min( distance, location.distanceTo( reference ) );
        }
    
        return distance;
    }
    
    /**
     * Returns the distance from the Earth's center to a point on its spheroid surface at the given latitude.
     *
     * @param latitude The latitude where to calculate the Earth's radius from
     *
     * @return The Earth's radius.
     */
    public static double earth_radius( double latitude ) {
        final int equatorial_radius = 6378137;
        final int polar_radius      = 6356752;
        
        final double a = Math.pow( Math.pow( equatorial_radius, 2 ) * Math.cos( latitude ), 2 ) +
                         Math.pow( Math.pow( polar_radius, 2 ) * Math.sin( latitude ), 2 );
        
        final double b = Math.pow( equatorial_radius * Math.cos( latitude ), 2 ) +
                         Math.pow( polar_radius * Math.sin( latitude ), 2 );
        
        return Math.sqrt( a / b );
    }
    
    /**
     * Tests if a given location is within a polygon.
     *
     * @param location The {@code LatLng} to test
     * @param polygon  An array of {@code LatLng} describing the contour of the polygon
     *
     * @return The boolean result.
     */
    public static boolean in_polygon( final LatLng location, final ArrayList<LatLng> polygon ) {
        int     i, j;
        boolean in = false;
    
        for (i = 0, j = polygon.size() - 1; i < polygon.size(); j = i++) {
            if ((polygon.get( i ).getLatitude() > location.getLatitude()) != (polygon.get( j )
                                                                                     .getLatitude() >
                                                                              location
                                                                                      .getLatitude()) &&
                (location.getLongitude() < (polygon.get( j ).getLongitude() - polygon.get( i )
                                                                                     .getLongitude()) *
                                           (location
                                                    .getLatitude() - polygon.get(
                                                   i ).getLatitude()) /
                                           (polygon.get( j ).getLatitude() - polygon.get( i )
                                                                                    .getLatitude()) +
                                           polygon
                                                   .get(
                                                           i ).getLongitude())) {
                in = !in;
            }
        }
        return in;
    }
    
    /**
     * Shortcuts to transcribe a location into its corresponding screen coordinates.
     *
     * @return {@code PointF} screen's location.
     */
    public static LatLng location_at_screen_center() {
        final Projection projection = SharedBuffer.map().getProjection();
        return projection.fromScreenLocation( new PointF( SharedBuffer.screen_width() * .5f,
                                                          SharedBuffer.screen_height() * .5f ) );
    }
}
