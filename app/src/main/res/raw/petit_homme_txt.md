Lalalalalaaaa… Tu entends ma voix ? Oui, à présent tu l’entends. Suis-la, tourne-toi si nécessaire, afin de trouver le meilleur angle d’écoute.. Lalalalalalalalalalalaaaaaaa, très bien.

Si tu as l’entrée des auditoires Thomas More dans ton dos et le mur latéral du bâtiment Leclercq devant, c’est que nous sommes sur la même longueur d’onde. Lève la tête et place-toi exactement en face de la troisième fente de la balustrade. Ca y est ? Tu vois le balcon surmonté de baies vitrées ? Tu discernes bien la fente, l’intérieur de la fente ? Rien ne te frappe ? Observe, observe bien.

Oui ! Oui, il y a là, posée sur le balcon, une toute, toute petite vie ; un petit homme, appuyé contre la paroi de gauche. Il te regarde, lui aussi. Dans la ville, d’autres frères sont disséminés. Peut-être identifieras-tu leurs cachettes. Eux, n’ont pas attendu pour te guetter.
