Pour moi, c’est ici que tout commence : on vient aux Halles s’inscrire à l’université
Alors c’est un éternel recommencement : parce que moi j’y passe toutes les semaines avec ma valise, pour aller et revenir de la gare
C’est une nappe géante, on y mange assis parterre
On s’y donne rendez-vous à la fontaine
Celle de Roméo et Juliette ?
Non ! celle de Léon et Valérie
C’est Wall Street, moi je vais y chercher de l’argent
Et moi je vais y donner mon sang

On y passe aussi pour aller faire nos emplettes
C’est le lieu névralgique de notre ville…
Alors pourquoi on l’a pas appelée la Grand Place ?
Ben parce qu’il y en a déjà une.
(bruit de sabots)
Tiens ? Une calèche et des chevaux ? pour les touristes ?
Mais non ! c’est une petite citerne pour arroser les fleurs de LLN.
C’est une fameuse plaque tournante quand même !
Et on va y chercher notre diplôme, comme l’indique la main de bronze dans le mur des Halles. C’est donc ici que tout finit…
