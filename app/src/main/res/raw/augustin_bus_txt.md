Hé ! bonjour ! bonjour ! Hé, vous là-bas ! Je suis par ici. Ici.

Allez venez approchez n’ayez pas peur. Oui, c’est moi, le bonhomme en bronze qui fait impassiblement de l’auto-stop. Où allez-vous donc ? Vers Bruxelles ? Namur ? Ou même un peu plus loin ? Je ne suis pas difficile.

Je me présente. Je m’appelle Augustin. Peut-être avez-vous déjà entendu parler de moi. Ou aperçu ma photo sur la vitrine d’un café de la Grand Place. Vous savez, Chez Augustin justement ! Mon Gépéto à moi s’appelle Gigi Warny. Il m’a conçu et installé ici-même il y a quinze ans, en 2002. 

Depuis tout ce temps, je veille sur la gare des bus. Les gens sont allés et venus. J’ai au fil des années découvert de nouvelles têtes, qui se sont ici bien remplies. J’ai connu les joies et les déceptions, les amours naissantes et les séparations. Pour moi, bien sûr, rien n’a changé.

Mais, vous savez, sous mes airs d’envie de voyage, je me dis qu’on n’est pas si mal par ici.