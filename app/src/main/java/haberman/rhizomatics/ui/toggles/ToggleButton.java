package haberman.rhizomatics.ui.toggles;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

import java.util.ArrayList;

/**
 * Simple class extending Android button with a toggling capability.
 *
 * @author haberman
 */

public class ToggleButton extends android.support.v7.widget.AppCompatButton implements
                                                                            View.OnClickListener
{
    /** Possible states this toggle can hold. */
    ArrayList<String> states_;
    
    /** Current state, defaulted as `OFF`. */
    @NonNull
    String state_ = "OFF";
    
    /** Background drawable resource representing the {@link #state_}. */
    @DrawableRes
    int drawable_ = -1;
    
    public ToggleButton( Context context ) {
        super( context );
        init();
    }
    
    /** Initializes button with an empty list of states and a click listener. */
    void init() {
        states_ = new ArrayList<>();
        setOnClickListener( this );
    }
    
    public ToggleButton( Context context, AttributeSet attrs ) {
        super( context, attrs );
        init();
    }
    
    @DrawableRes
    public int drawable() { return drawable_; }
    
    
    /**
     * Button click callback.
     *
     * @param view The view receiving the event
     */
    @Override
    public void onClick( View view ) {
        final int state_index = states_.indexOf( state_ );
        final int next_index  = state_index >= states_.size() - 1 ? 0 : state_index + 1;
        
        set_state( states_.get( next_index ), true );
    }
    
    /**
     * Sets a new state.
     * This package-private method should be overridden in children class to do actual UI changes
     * upon the {@link #drawable_} resource.
     *
     * @param state           The new string state
     * @param dispatch_change Whether to dispatch change or not
     */
    void set_state( @Nullable final String state, final boolean dispatch_change ) {
        if (state != null) {
            state_ = state;
        }
    }
}
